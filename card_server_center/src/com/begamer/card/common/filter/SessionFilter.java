package com.begamer.card.common.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.begamer.card.common.Constant;
import com.begamer.card.model.pojo.Manager;

/**
 * @ClassName: SessionFilter
 * @Description: session过滤器
 * @author gs
 * @date Nov 10, 2011 4:08:12 PM
 */
public class SessionFilter implements Filter {

	private String[] except = {"portal.htm","gift.htm","centre.htm"};

	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest req = (HttpServletRequest) request;
		for (int i = 0; i < this.except.length; i++)
		{
			if (req.getRequestURI().indexOf(this.except[i], 0) >= 0)
			{
				chain.doFilter(request, response);
				return;
			}
		}
		HttpSession session = req.getSession();
		Object obj = session.getAttribute(Constant.LOGIN_USER);
		if(null != obj)
		{
			Manager userInfo = (Manager) obj;
			if(userInfo!=null)
			{
				request.setAttribute(Constant.LOGIN_USER, userInfo);
			} 
			else
			{
				// res.sendRedirect(this.indexPage);
				java.io.PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<script>");
				out.println("window.open ('portal.htm?action=index', 'father')");
				out.println("</script>");
				out.println("</html>");
				return;
			}
		}
		else
		{
			java.io.PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<script>");
			out.println("window.open ('portal.htm?action=index', 'father')");
			out.println("</script>");
			out.println("</html>");
			return;
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig arg0) throws ServletException {}
	public void destroy() {}
}
