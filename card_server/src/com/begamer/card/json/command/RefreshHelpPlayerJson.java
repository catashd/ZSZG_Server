package com.begamer.card.json.command;

import com.begamer.card.json.BasicJson;

public class RefreshHelpPlayerJson extends BasicJson
{
	/**1需要删除水晶**/
	public int needCrystal;

	public int getNeedCrystal()
	{
		return needCrystal;
	}

	public void setNeedCrystal(int needCrystal)
	{
		this.needCrystal = needCrystal;
	}
	
}
