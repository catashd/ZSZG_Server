package com.begamer.card.json.element;

public class KOawardElement
{
	public int id;//excel表中的id
	public int rewardtype;
	public String reward;
	public int state;//(0,可以兑换,1,已经兑换过)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRewardtype() {
		return rewardtype;
	}
	public void setRewardtype(int rewardtype) {
		this.rewardtype = rewardtype;
	}
	public String getReward() {
		return reward;
	}
	public void setReward(String reward) {
		this.reward = reward;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	
	
}
