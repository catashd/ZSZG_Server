package com.begamer.card.json;

import com.begamer.card.common.Constant;
import com.begamer.card.model.pojo.Player;

public class PlayerElement
{
	public int id;
	public String name;
	public String head;
	public int level;
	public int curExp;
	public int extendCardNum;
	public int gold;
	public int crystal;
	public int diamond;
	public int iconId;/**援护技Id**/
	public int power;/**人物体力**/
	public int sPower;/**总体力值**/
	public String runeId;//当前符文信息:x-xx-xx-xx-xx-xx-xx,遍数-第1图点亮个数-第2图点亮个数-第3图点亮个数-第4图点亮个数-第5图点亮个数-第6图点亮个数
	public int battlePower;//当前战力值//
	public int newPlayerType;//新手标识
	public int maxEnergy;//怒气上限//
	public int vipLevel;//vip等级//
	public int missionId;
	public int missionId2;
	public String owncards;//获得过的卡id&id&id
	public void setData(Player p)
	{
		id=p.getId();
		name=p.getName();
		head=p.getHead();
		level=p.getLevel();
		curExp=p.getCurExp();
		extendCardNum=p.getExtendCardNum();
		gold=p.getGold();
		crystal=p.getTotalCrystal();
		diamond=p.getDiamond();
		iconId=p.getIconId();
		power =p.getPower();
		sPower =Constant.MaxPower;
		runeId=p.getRuneId();
		newPlayerType =p.getNewPlayerType();
		battlePower=p.getBattlePower();
		maxEnergy=p.getMaxEnergy();
		vipLevel=p.getVipLevel();
		missionId =p.getMissionId();
		missionId2 =p.getMissionId2();
		owncards =p.getOwncards();
	}

	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public int getLevel()
	{
		return level;
	}
	public void setLevel(int level)
	{
		this.level = level;
	}
	public int getCurExp()
	{
		return curExp;
	}
	public void setCurExp(int curExp)
	{
		this.curExp = curExp;
	}
	public int getExtendCardNum()
	{
		return extendCardNum;
	}
	public void setExtendCardNum(int extendCardNum)
	{
		this.extendCardNum = extendCardNum;
	}
	public int getGold()
	{
		return gold;
	}
	public void setGold(int gold)
	{
		this.gold = gold;
	}
	public int getCrystal()
	{
		return crystal;
	}
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	public int getIconId()
	{
		return iconId;
	}
	public void setIconId(int iconId)
	{
		this.iconId = iconId;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getSPower()
	{
		return sPower;
	}
	public void setSPower(int sPower)
	{
		this.sPower = sPower;
	}
	public String getRuneId()
	{
		return runeId;
	}
	public void setRuneId(String runeId)
	{
		this.runeId = runeId;
	}
	public int getNewPlayerType() {
		return newPlayerType;
	}
	public void setNewPlayerType(int newPlayerType) {
		this.newPlayerType = newPlayerType;
	}
	public int getBattlePower()
	{
		return battlePower;
	}
	public void setBattlePower(int battlePower)
	{
		this.battlePower = battlePower;
	}
	public int getMaxEnergy()
	{
		return maxEnergy;
	}
	public void setMaxEnergy(int maxEnergy)
	{
		this.maxEnergy = maxEnergy;
	}
	public int getVipLevel()
	{
		return vipLevel;
	}
	public void setVipLevel(int vipLevel)
	{
		this.vipLevel = vipLevel;
	}

	public int getMissionId() {
		return missionId;
	}

	public void setMissionId(int missionId) {
		this.missionId = missionId;
	}

	public int getMissionId2() {
		return missionId2;
	}

	public void setMissionId2(int missionId2) {
		this.missionId2 = missionId2;
	}

	public String getOwncards() {
		return owncards;
	}

	public void setOwncards(String owncards) {
		this.owncards = owncards;
	}

	public int getDiamond()
	{
		return diamond;
	}

	public void setDiamond(int diamond)
	{
		this.diamond = diamond;
	}
}
