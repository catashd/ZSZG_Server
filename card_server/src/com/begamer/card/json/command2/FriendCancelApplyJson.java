package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class FriendCancelApplyJson extends BasicJson
{
	public int i;

	public int getI()
	{
		return i;
	}

	public void setI(int i)
	{
		this.i = i;
	}
	
}
