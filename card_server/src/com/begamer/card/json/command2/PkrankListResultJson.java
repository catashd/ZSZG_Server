package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class PkrankListResultJson extends ErrorJson
{
	public List<String> pks;///**pk对象列表** 玩家id-玩家名称-头像-排名-战力**/
	public List<String> cardIds;//顺序对应pks中的玩家顺序 cardid-id-id-id-id-id
	public List<String> getPks() {
		return pks;
	}

	public void setPks(List<String> pks) {
		this.pks = pks;
	}

	public List<String> getCardIds() {
		return cardIds;
	}

	public void setCardIds(List<String> cardIds) {
		this.cardIds = cardIds;
	}
	
}
