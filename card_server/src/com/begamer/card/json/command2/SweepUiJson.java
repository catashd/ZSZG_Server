package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class SweepUiJson extends BasicJson
{
	/**关卡id**/
	public int md;

	public int getMd() {
		return md;
	}

	public void setMd(int md) {
		this.md = md;
	}
}
