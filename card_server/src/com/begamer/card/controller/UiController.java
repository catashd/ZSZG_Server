package com.begamer.card.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.begamer.card.cache.Cache;
import com.begamer.card.cache.MailThread;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.Constant;
import com.begamer.card.common.util.Random;
import com.begamer.card.common.util.RequestUtil;
import com.begamer.card.common.util.Statics;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.AchievementData;
import com.begamer.card.common.util.binRead.BlackMarketData;
import com.begamer.card.common.util.binRead.BlackRefreshData;
import com.begamer.card.common.util.binRead.BlackShopBoxData;
import com.begamer.card.common.util.binRead.BloodBuffData;
import com.begamer.card.common.util.binRead.CardData;
import com.begamer.card.common.util.binRead.CardGetData;
import com.begamer.card.common.util.binRead.CardkuData;
import com.begamer.card.common.util.binRead.ChangeNameCostData;
import com.begamer.card.common.util.binRead.ComposeData;
import com.begamer.card.common.util.binRead.CornucopiaData;
import com.begamer.card.common.util.binRead.EquipData;
import com.begamer.card.common.util.binRead.EventData;
import com.begamer.card.common.util.binRead.EvolutionData;
import com.begamer.card.common.util.binRead.FBeventData;
import com.begamer.card.common.util.binRead.GiftData;
import com.begamer.card.common.util.binRead.IconUnlockData;
import com.begamer.card.common.util.binRead.ItemsData;
import com.begamer.card.common.util.binRead.KoAwardData;
import com.begamer.card.common.util.binRead.LevelGiftData;
import com.begamer.card.common.util.binRead.MazeBattleData;
import com.begamer.card.common.util.binRead.MazeCostData;
import com.begamer.card.common.util.binRead.MazeData;
import com.begamer.card.common.util.binRead.MazeRewardData;
import com.begamer.card.common.util.binRead.MeditationData;
import com.begamer.card.common.util.binRead.MissionData;
import com.begamer.card.common.util.binRead.PassiveSkillData;
import com.begamer.card.common.util.binRead.RechargeData;
import com.begamer.card.common.util.binRead.RunData;
import com.begamer.card.common.util.binRead.RunnerData;
import com.begamer.card.common.util.binRead.SensitivewordsData;
import com.begamer.card.common.util.binRead.SevenDaysData;
import com.begamer.card.common.util.binRead.ShopData;
import com.begamer.card.common.util.binRead.ShoppvpData;
import com.begamer.card.common.util.binRead.SkillData;
import com.begamer.card.common.util.binRead.TakeCardTimeData;
import com.begamer.card.common.util.binRead.UnLockData;
import com.begamer.card.common.util.binRead.VipData;
import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.FriendElement;
import com.begamer.card.json.PackElement;
import com.begamer.card.json.PlayerElement;
import com.begamer.card.json.command2.AchieveResultJson;
import com.begamer.card.json.command2.BloodResultJson;
import com.begamer.card.json.command2.BreakResultJson;
import com.begamer.card.json.command2.CGResultJson;
import com.begamer.card.json.command2.CardInfoResultJson;
import com.begamer.card.json.command2.ComposeResultJson;
import com.begamer.card.json.command2.CornucopiaResultJson;
import com.begamer.card.json.command2.DropGuideResultJson;
import com.begamer.card.json.command2.EventResultJson;
import com.begamer.card.json.command2.FriendResultJson;
import com.begamer.card.json.command2.GiftRewardResultJson;
import com.begamer.card.json.command2.HeadSetResultJson;
import com.begamer.card.json.command2.ImaginationResultJson;
import com.begamer.card.json.command2.KOExchangeResultJson;
import com.begamer.card.json.command2.LevelGiftResultJson;
import com.begamer.card.json.command2.LockResultJson;
import com.begamer.card.json.command2.LoginDayResultJson;
import com.begamer.card.json.command2.LotEventRankResultJson;
import com.begamer.card.json.command2.LotResultJson;
import com.begamer.card.json.command2.MailUIResultJson;
import com.begamer.card.json.command2.MainResultJson;
import com.begamer.card.json.command2.MapResultJson;
import com.begamer.card.json.command2.MazeClearResultJson;
import com.begamer.card.json.command2.MazeResultJson;
import com.begamer.card.json.command2.MazeRewardResultJson;
import com.begamer.card.json.command2.NewUnitskillResultJson;
import com.begamer.card.json.command2.PackElementJson;
import com.begamer.card.json.command2.PackResultJson;
import com.begamer.card.json.command2.PkrankListResultJson;
import com.begamer.card.json.command2.PlayerResultJson;
import com.begamer.card.json.command2.PowerResultJson;
import com.begamer.card.json.command2.RechargeUiResultJson;
import com.begamer.card.json.command2.RunRewardResultJson;
import com.begamer.card.json.command2.RunnerResultJson;
import com.begamer.card.json.command2.RunnerUiResultJson;
import com.begamer.card.json.command2.SelectCardResultJson;
import com.begamer.card.json.command2.ShopUiResultJson;
import com.begamer.card.json.command2.SignResultJson;
import com.begamer.card.json.command2.UIJson;
import com.begamer.card.json.command2.WishResultJson;
import com.begamer.card.json.element.KOawardElement;
import com.begamer.card.json.element.MailUIResultElement;
import com.begamer.card.json.element.ShopElement;
import com.begamer.card.log.ErrorLogger;
import com.begamer.card.log.PlayerLogger;
import com.begamer.card.model.pojo.Activity;
import com.begamer.card.model.pojo.ActivityInfo;
import com.begamer.card.model.pojo.Announce;
import com.begamer.card.model.pojo.Card;
import com.begamer.card.model.pojo.CardGroup;
import com.begamer.card.model.pojo.Equip;
import com.begamer.card.model.pojo.Event;
import com.begamer.card.model.pojo.Item;
import com.begamer.card.model.pojo.LogBuy;
import com.begamer.card.model.pojo.LotRank;
import com.begamer.card.model.pojo.Mail;
import com.begamer.card.model.pojo.Maze;
import com.begamer.card.model.pojo.PassiveSkill;
import com.begamer.card.model.pojo.Player;
import com.begamer.card.model.pojo.Skill;

public class UiController extends AbstractMultiActionController {
	private static Logger errorlogger = ErrorLogger.logger;
	private static Logger playerlogger = PlayerLogger.logger;
	private static Logger logger = Logger.getLogger(UiController.class);
	private static String[] ss = { " ", "`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "+", "=", "\\", "|", "}", "{", "[", "]", ";", ":", "'", ",", ".", "/", "?", ">", "<", "\"" };
	
	public ModelAndView getui(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			// 获取参数
			UIJson uj = JSONObject.toJavaObject(jsonObject, UIJson.class);
			String msg = "";
			ErrorJson ej = null;
			if (uj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(uj.playerId, request.getSession());
				if (pi != null)
				{
					// 玩家信息
					if (uj.ui == 0)
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|玩家信息");
						ej = new PlayerResultJson();
						PlayerResultJson prj = (PlayerResultJson) ej;
						List<PlayerElement> pes = new ArrayList<PlayerElement>();
						PlayerElement pe = new PlayerElement();
						pi.calBattlePower();
						pe.setData(pi.player);
						pes.add(pe);
						prj.list = pes;
						int mission = pi.player.getMissionId();
						int lv = pi.player.getLevel();
						String[] s = UnLockData.getUnLockStrByMethod(mission, lv);
						prj.s = s;
						msg = JSON.toJSONString(prj);
					}
					else if (uj.ui == 1)// 背包物品
					{
						// if(pi.player.getId()==9669)
						// {
						// pi.player.setDiamond(20000);
						// System.out.println("..................");
						// }
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|背包");
						ej = new PackResultJson();
						PackResultJson prj = (PackResultJson) ej;
						if (checkMode(2, pi))
						{
							int type = uj.getType();
							List<PackElement> pes = packElements(pi, type);
							int t = type;
							if (t == 3)// equip
							{
								t = 4;
							}
							else if (t == 4)// item
							{
								t = 5;
							}
							else if (t == 8)// passiveskill
							{
								t = 3;
							}
							pes = orderPackElement(pes, t);
							// 判断装备或者主动技能装备与哪个卡 用于背包中显示装备于XXX
							prj.pejs = getPackElementJsons(pes, pi, uj.type);
							prj.buyTimes = pi.player.getBuyBagTimes();
							
							// 查看背包改变新卡标识
							switch (type)
							{
								case 1:// card
									List<Card> cards = pi.getCards();
									for (Card c : cards)
									{
										if (c.getNewType() == 0)
										{
											c.setNewType(1);
										}
									}
									break;
								case 2:// skill
									List<Skill> skills = pi.getSkills();
									for (Skill s : skills)
									{
										if (s.getNewType() == 0)
										{
											s.setNewType(1);
										}
									}
									break;
								case 3:// equip
									List<Equip> equips = pi.getEquips();
									for (Equip e : equips)
									{
										if (e.getNewType() == 0)
										{
											e.setNewType(1);
										}
									}
									break;
								case 4:// item
									List<Item> items = pi.getItems();
									for (Item item : items)
									{
										if (item.getNewType() == 0)
										{
											item.setNewType(1);
										}
									}
									break;
								case 5:// paaaiveskill
									List<PassiveSkill> pskills = pi.getPassiveSkillls();
									for (PassiveSkill ps : pskills)
									{
										if (ps.getNewType() == 0)
										{
											ps.setNewType(1);
										}
									}
									break;
							}
						}
						else
						{
							prj.errorCode = 56;
						}
						msg = JSON.toJSONString(prj);
					}
					else if (uj.ui == 2)// 背包中可出售物品
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|出售物品列表");
						ej = new PackResultJson();
						PackResultJson prj = (PackResultJson) ej;
						int type = uj.getType();
						List<PackElement> pes = packSales(pi, type);
						
						pes = orderPackElement3(type, pes, pi);
						prj.list = pes;
						prj.buyTimes = pi.player.getBuyBagTimes();
						String allSelect = pi.player.getAllSelectId();
						String[] temp = null;
						if (allSelect != null && allSelect.length() > 0)
						{
							temp = allSelect.split("&");
							prj.allSelect = temp;
						}
						else
						{
							temp = new String[1];
							temp[0] = "";
							prj.allSelect = temp;
						}
						msg = JSON.toJSONString(prj);
					}
					else if (uj.ui == 3)// 主界面--当前战斗卡组中角色卡信息
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|主界面");
						ej = new MainResultJson();
						MainResultJson mrj = (MainResultJson) ej;
						List<String> cs = getCardInfo(pi);
						mrj.cs = cs;
						// 提示信息功能
						List<String> s = new ArrayList<String>();
						// 好友
						if (checkMode(21, pi))
						{
							if (Cache.getInstance().getApplyMes(pi.player.getId()).size() > 0)
							{
								s.add(21 + "-" + 1);
							}
							else if (Cache.getInstance().getApplyMes(pi.player.getId()).size() == 0)
							{
								s.add(21 + "-" + 0);
							}
						}
						else
						{
							s.add(21 + "-" + 0);
						}
						// 邮件
						if (checkMode(22, pi))
						{
							s.add(22 + "-" + getNewMail(pi));
						}
						else
						{
							s.add(22 + "-" + 0);
						}
						// 抽卡
						CardGetData cgd = CardGetData.getData(1);
						if (checkMode(16, pi))
						{
							if (pi.player.getFriend() >= cgd.cost || pi.getTargetItemsNum(40000) > 0)
							{
								s.add(16 + "-" + 1);
							}
							else
							{
								s.add(16 + "-" + 0);
							}
						}
						else
						{
							s.add(16 + "-" + 0);
						}
						s.add(1 + "-" + getNewTypeFormation(pi));
						// // 突破
						// if (checkMode(17, pi))
						// {
						// s.add(17 + "-" + getBreakCard(pi));
						// }
						// else
						// {
						// s.add(17 + "-" + 0);
						// }
						// 成就
						boolean a = true;
						if (checkMode(20, pi))
						{
							String active = pi.player.getAchive();
							if (active != null && active.length() > 0)
							{
								String[] actives = active.split("-");
								for (int k = 0; k < actives.length; k++)
								{
									if (actives[k] != null && actives[k].length() > 0)
									{
										if (actives[k].split("%")[1].equals("0"))
										{
											AchievementData ad = AchievementData.getData(StringUtil.getInt(actives[k].split("%")[0]));
											if (ad != null)
											{
												s.add(20 + "-" + 1);
												a = false;
											}
											else
											{
												s.add(20 + "-" + 0);
												a = false;
											}
											break;
										}
									}
									
								}
								if (a)
								{
									s.add(20 + "-" + 0);
								}
							}
							else
							{
								s.add(20 + "-" + 0);
							}
						}
						else
						{
							s.add(20 + "-" + 0);
						}
						// 合成
						int compose = 0;// 无新提醒
						List<ComposeData> cDatas = ComposeData.getComposeDatas();
						// 有新的可制作卡牌
						for (ComposeData com : cDatas)
						{
							if (checkCompose(com, pi))
							{
								compose = 1;// 有新提醒
								break;
							}
						}
						if (compose == 0)// 解锁新的合成卡牌
						{
							List<ComposeData> list = ComposeData.getComposeDataUnlocked(pi);
							HashMap<String, String> comMap = pi.getComposeMap(pi);
							for (int i = 0; i < list.size(); i++)
							{
								if (!comMap.containsKey(list.get(i).composite + ""))
								{
									compose = 1;
									break;
								}
							}
						}
						s.add(4 + "-" + compose);
						// 任务 38
						int activity = 0;
						String state = pi.player.getDailyTaskState();
						if (pi.player.getVipLevel() == 0)
						{
							String v = state.substring(7, 8);
							if (StringUtil.getInt(v) == 1)
							{
								state = StringUtil.replaceStr(state, 7, 0 + "");
							}
						}
						else
						{
							String v = state.substring(7, 8);
							if (StringUtil.getInt(v) == 0)
							{
								state = StringUtil.replaceStr(state, 7, 1 + "");
							}
						}
						if (pi.player.getVipMonthType() == 1 && pi.player.getVipMonthDay() > 0)// 月卡领取钻石
						{
							String t1 = state.substring(10, 11);
							if (StringUtil.getInt(t1) == 0)
							{
								state = StringUtil.replaceStr(state, 10, 1 + "");
							}
						}
						
						// 领取体力特殊处理
						// 00:00-13:00发给客户端第一个领取体力值任务 13:00-19：00
						String date1 = "13:00:00";
						String date3 = "19:00:00";
						String date4 = "12:00:00";
						String date5 = "18:00:00";
						String today = StringUtil.getDate(System.currentTimeMillis());
						long t2 = StringUtil.getTimeStamp(today + " " + date1);// 13
						long t3 = StringUtil.getTimeStamp(today + " " + date3);// 19
						long t4 = StringUtil.getTimeStamp(today + " " + date4);// 12
						long t5 = StringUtil.getTimeStamp(today + " " + date5);// 18
						long time = System.currentTimeMillis();
						String t = "";
						if (time - t4 >= 0 && time - t2 <= 0)// 12-13
						{
							// 更改状态
							t = state.substring(11, 12);
							if (StringUtil.getInt(t) == 0)
							{
								state = StringUtil.replaceStr(state, 11, 1 + "");// 修改任务状态
							}
						}
						else if (time - t2 > 0)
						{
							// 更改状态
							t = state.substring(11, 12);
							if (StringUtil.getInt(t) == 1)// 如果活动时间结束改回未完成状态
							{
								state = StringUtil.replaceStr(state, 11, 0 + "");// 修改任务状态
							}
						}
						if (time - t5 >= 0 && time - t3 <= 0)// 18-19
						{
							t = state.substring(12, 13);
							if (StringUtil.getInt(t) == 0)
							{
								state = StringUtil.replaceStr(state, 12, 1 + "");// 修改任务状态
							}
						}
						else if (time - t3 > 0)// 如果活动时间结束改回未完成状态
						{
							// 更改状态
							t = state.substring(12, 13);
							if (StringUtil.getInt(t) == 1)
							{
								state = StringUtil.replaceStr(state, 12, 0 + "");// 修改任务状态
							}
						}
						if (state != null && state.length() > 0)
						{
							if (state.contains("1"))
							{
								activity = 1;
							}
						}
						pi.player.setDailyTaskState(state);
						s.add(38 + "-" + activity);
						// 签到
						int sign = 0;
						if (!Statics.isTodaySign(pi))
						{
							sign = 1;
						}
						s.add(23 + "-" + sign);
						
						// 活动
						List<ActivityInfo> aci = Cache.instance.getActivityInfosforType();
						if (aci != null && aci.size() > 0)
						{
							boolean is = true;
							for (ActivityInfo ai : aci)
							{
								String exnum = pi.player.getExchange();// 27-1,
								boolean is2 = true;
								if (exnum != null && exnum.trim().length() > 0)
								{
									String[] exchange = exnum.split(",");// 27-1
									for (int n = 0; n < exchange.length; n++)
									{
										String[] ex = exchange[n].split("-");// 27
										int activityId = Integer.parseInt(ex[0]);
										int num = Integer.parseInt(ex[1]);
										if (activityId == ai.getId() && ai.getSole() > 0 && num >= ai.getSole())// 30
										{
											is2 = false;
											break;
										}
									}
								}
								if (is2)
								{
									boolean n1 = true;
									boolean n2 = true;
									boolean n3 = true;
									
									if (ai.getNeedType() > 0)
									{
										n1 = Statics.check(pi, ai.getNeedType(), ai.getNeedId(), ai.getNeedNum());
									}
									if (ai.getNeedType2() > 0)
									{
										n2 = Statics.check(pi, ai.getNeedType2(), ai.getNeedId2(), ai.getNeedNum2());
									}
									if (ai.getNeedType3() > 0)
									{
										n3 = Statics.check(pi, ai.getNeedType3(), ai.getNeedId3(), ai.getNeedNum3());
									}
									if (n1 && n2 && n3)
									{
										s.add(19 + "-" + 1);
										is = false;
									}
								}
							}
							if (is)
							{
								// 判断等级
								String levelgift = pi.player.getLevelgift();
								if (levelgift != null && levelgift.length() > 0)
								{
									for (int i = 1; i <= pi.player.getLevel(); i++)
									{
										LevelGiftData lgData = LevelGiftData.getLevelGiftData(i);
										if (lgData != null)
										{
											if (!levelgift.contains(lgData.id + ""))
											{
												s.add(19 + "-" + 1);
												is = false;
												break;
											}
										}
									}
								}
								else
								{
									for (int i = 1; i <= pi.player.getLevel(); i++)
									{
										LevelGiftData lgData = LevelGiftData.getLevelGiftData(i);
										if (lgData != null)
										{
											s.add(19 + "-" + 1);
											is = false;
											break;
										}
									}
								}
								// 判断登陆
								if (is)
								{
									String loginDayAward = pi.player.getLoginDayAward();
									int day = 0;
									if (pi.player.getLoginDayNum() <= 7)
									{
										day = pi.player.getLoginDayNum();
									}
									else
									{
										day = 7;
									}
									if (loginDayAward != null && loginDayAward.length() > 0)
									{
										HashMap<Integer, String> map = new HashMap<Integer, String>();
										String[] temp = loginDayAward.split("&");
										for (int i = 0; i < temp.length; i++)
										{
											if (temp[i] != null && temp[i].length() > 0)
											{
												map.put(StringUtil.getInt(temp[i]), temp[i]);
											}
										}
										for (int j = 1; j <= day; j++)
										{
											ShopElement se = new ShopElement();
											se.setId(j);
											if (!map.containsKey(j))
											{
												s.add(19 + "-" + 1);
												is = false;
												break;
											}
										}
									}
									else
									{
										for (int j = 0; j <= day; j++)
										{
											s.add(19 + "-" + 1);
											is = false;
											break;
										}
									}
								}
								// 大风车
								if (is)
								{
									Activity turnAct = Cache.getInstance().getActivityById(8);
									if (turnAct != null && turnAct.getSell() == 0)
									{
										if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(turnAct.getEndtime()) < 0 && StringUtil.getDateTime(System.currentTimeMillis()).compareTo(turnAct.getSttime()) > 0)
										{
											String turnTimes = pi.player.getLottoTimes();
											boolean turnIn = false;
											if (turnTimes != null && turnTimes.length() > 0)// 存储次数字段不为空
											{
												String[] turn = turnTimes.split("&");
												for (int e = 0; e < turn.length; e++)
												{
													String[] temp = turn[e].split("-");
													if (StringUtil.getInt(temp[1]) > 0)// 次数大于0，可抽奖，t=1
													{
														s.add(19 + "-" + 1);
														is = false;
														break;
													}
												}
											}
										}
									}
								}
								// 限时神将
								if (is)
								{
									Activity turnAct = Cache.getInstance().getActivityById(9);
									if (turnAct != null && turnAct.getSell() == 0)
									{
										if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(turnAct.getEndtime()) < 0 && StringUtil.getDateTime(System.currentTimeMillis()).compareTo(turnAct.getSttime()) > 0)
										{
											TakeCardTimeData tctd = TakeCardTimeData.getData(1);
											LotRank lr = Cache.getInstance().getLotRankById(pi.player.getId());
											if (lr != null)
											{
												if ((System.currentTimeMillis() - (lr.getLastLotTime() + tctd.time * 3600 * 1000)) >= 0)
												{
													s.add(19 + "-" + 1);
													is = false;
												}
											}
											else
											{
												s.add(19 + "-" + 1);
												is = false;
											}
										}
									}
								}
								// 否则，最终
								if (is)
								{
									s.add(19 + "-" + 0);
								}
							}
							
						}
						else
						{
							boolean is = true;
							// 判断等级
							String levelgift = pi.player.getLevelgift();
							if (levelgift != null && levelgift.length() > 0)
							{
								for (int i = 1; i <= pi.player.getLevel(); i++)
								{
									LevelGiftData lgData = LevelGiftData.getLevelGiftData(i);
									if (lgData != null)
									{
										if (!levelgift.contains(lgData.id + ""))
										{
											s.add(19 + "-" + 1);
											is = false;
											break;
										}
									}
								}
							}
							else
							{
								for (int i = 1; i <= pi.player.getLevel(); i++)
								{
									LevelGiftData lgData = LevelGiftData.getLevelGiftData(i);
									if (lgData != null)
									{
										s.add(19 + "-" + 1);
										is = false;
										break;
									}
								}
							}
							
							// 判断登陆
							if (is)
							{
								String loginDayAward = pi.player.getLoginDayAward();
								int day = 0;
								if (pi.player.getLoginDayNum() <= 7)
								{
									day = pi.player.getLoginDayNum();
								}
								else
								{
									day = 7;
								}
								if (loginDayAward != null && loginDayAward.length() > 0)
								{
									HashMap<Integer, String> map = new HashMap<Integer, String>();
									String[] temp = loginDayAward.split("&");
									for (int i = 0; i < temp.length; i++)
									{
										if (temp[i] != null && temp[i].length() > 0)
										{
											map.put(StringUtil.getInt(temp[i]), temp[i]);
										}
									}
									for (int j = 1; j <= day; j++)
									{
										ShopElement se = new ShopElement();
										se.setId(j);
										if (!map.containsKey(j))
										{
											s.add(19 + "-" + 1);
											is = false;
											break;
										}
									}
								}
								else
								{
									for (int j = 0; j <= day; j++)
									{
										s.add(19 + "-" + 1);
										is = false;
										break;
									}
								}
							}
							// 大风车
							if (is)
							{
								Activity turnAct = Cache.getInstance().getActivityById(8);
								if (turnAct != null && turnAct.getSell() == 0)
								{
									if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(turnAct.getEndtime()) < 0 && StringUtil.getDateTime(System.currentTimeMillis()).compareTo(turnAct.getSttime()) > 0)
									{
										String turnTimes = pi.player.getLottoTimes();
										boolean turnIn = false;
										if (turnTimes != null && turnTimes.length() > 0)// 存储次数字段不为空
										{
											String[] turn = turnTimes.split("&");
											for (int e = 0; e < turn.length; e++)
											{
												String[] temp = turn[e].split("-");
												if (StringUtil.getInt(temp[1]) > 0)// 次数大于0，可抽奖，t=1
												{
													s.add(19 + "-" + 1);
													is = false;
													break;
												}
											}
										}
									}
								}
							}
							// 限时神将
							if (is)
							{
								Activity turnAct = Cache.getInstance().getActivityById(9);
								if (turnAct != null && turnAct.getSell() == 0)
								{
									if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(turnAct.getEndtime()) < 0 && StringUtil.getDateTime(System.currentTimeMillis()).compareTo(turnAct.getSttime()) > 0)
									{
										TakeCardTimeData tctd = TakeCardTimeData.getData(1);
										LotRank lr = Cache.getInstance().getLotRankById(pi.player.getId());
										if (lr != null)
										{
											if ((System.currentTimeMillis() - (lr.getLastLotTime() + tctd.time * 3600 * 1000)) >= 0)
											{
												s.add(19 + "-" + 1);
												is = false;
											}
										}
										else
										{
											s.add(19 + "-" + 1);
											is = false;
										}
									}
								}
							}
							// 否则，最终
							if (is)
							{
								s.add(19 + "-" + 0);
							}
						}
						if (checkMode(25, pi))
						{
							// 在线时间
							int online = 0;
							long lineGiftTime = StringUtil.getTimeStamp(pi.player.getLineGiftTime());
							online = (int) (System.currentTimeMillis() - lineGiftTime) / 1000;
							String gift = pi.player.getLineGift();
							int giftId = 0;
							if (gift != null && gift.length() > 0)
							{
								String[] temp = gift.split("-");
								if (StringUtil.getInt(temp[1]) == 1)// 领取过
								{
									giftId = StringUtil.getInt(temp[0]) + 1;
									if (giftId <= GiftData.getDataList().size())
									{
										GiftData gdnext = GiftData.getGiftData(giftId);
										if (gdnext != null)
										{
											if (online >= gdnext.time * 60)
											{
												online = 0;
												pi.player.setLineGift(giftId + "-" + 0);
											}
											else
											{
												online = gdnext.time * 60 - online;
											}
										}
									}
									else
									{
										mrj.errorCode = 99;
									}
								}
								else
								{
									giftId = StringUtil.getInt(temp[0]);
									online = 0;
								}
							}
							else
							{
								GiftData gd = GiftData.getGiftData(1);
								if (online >= gd.time * 60)
								{
									giftId = gd.id;
									pi.player.setLineGift(giftId + "-" + 0);
									online = 0;
								}
								else
								{
									giftId = gd.id;
									online = gd.time * 60 - online;
								}
							}
							if (mrj.errorCode == 0)
							{
								mrj.giftId = giftId;
								mrj.onlineT = online;
							}
							
						}
						List<Activity> list = Cache.getInstance().getActivitys();
						if (list != null && list.size() > 0)
						{
							for (Activity ac : list)
							{
								if (ac.getActivityId() == 6)
								{
									if (ac.getEndtime() != null && ac.getEndtime().length() > 0)
									{
										int cTime = (int) (StringUtil.getTimeStamp(ac.getEndtime() + " 24:00:00") - System.currentTimeMillis()) / 1000;
										if (cTime <= 0)
										{
											mrj.type = 0;
										}
										else if (StringUtil.getTimeStamp(ac.getSttime() + " 00:00:00") - System.currentTimeMillis() > 0)
										{
											mrj.type = 0;
										}
										else
										{
											mrj.type = 1;
										}
									}
									break;
								}
							}
						}
						
						mrj.s = s;
						mrj.cost = pi.player.getVipCost();
						msg = JSON.toJSONString(mrj);
					}
					else if (uj.ui == 4)// 卡组
					{
						int playerID = uj.type;// 竞技场中对手玩家的id
						if (playerID != pi.player.getId() && playerID != 0)
						{
							pi = Cache.getInstance().getPlayerInfo(playerID);
						}
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|阵容");
						ej = new CGResultJson();
						CGResultJson frj = (CGResultJson) ej;
						if (checkMode(1, pi))
						{
							CardGroup cg = pi.getCardGroup();
							frj.transform(cg, pi);
							// 新合体技提醒功能
							frj.unit = 0;
						}
						else
						{
							frj.errorCode = 56;
						}
						msg = JSON.toJSONString(frj);
					}
					else if (uj.ui == 5)// 获取一个类型的所有数据
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|一个类型的所有数据");
						ej = new PackResultJson();
						PackResultJson prj = (PackResultJson) ej;
						int type = uj.getType();
						List<PackElement> pes = new ArrayList<PackElement>();
						if (type == 1)
						{
							pes = getElements2(pi, 1);
						}
						else
						{
							pes = getElements(pi, type);
						}
						if (type == 1 || type == 4)// card and equip
						// number从小到大排序
						{
							pes = orderPackElement(pes, type);
						}
						else if (type == 2 || type == 3)// skill and
						// passiveskill
						// 星级高的在前，同星级的序号小的在前
						{
							pes = orderPackElement2(pes, type);
						}
						// 5.30级解锁6.7.
						prj.list = pes;
						prj.buyTimes = pi.player.getBuyBagTimes();
						msg = JSON.toJSONString(prj);
					}
					else if (uj.ui == 6)// 设置战斗卡组
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|设置战斗卡组");
						ej = new CGResultJson();
						CGResultJson frj = (CGResultJson) ej;
						if (uj.type == 1)
						{
							CardGroup cg = pi.getCardGroup();
							if (cg.canFight())
							{
								frj.transform(cg, pi);
							}
							else
							{
								frj.errorCode = 23;
							}
							msg = JSON.toJSONString(frj);
						}
						msg = JSON.toJSONString(ej);
					}
					else if (uj.ui == 7)// 可强化的物品
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|可强化物品列表");
						ej = new PackResultJson();
						PackResultJson prj = (PackResultJson) ej;
						if (checkMode(3, pi))
						{
							int type = uj.getType();
							List<PackElement> pes = new ArrayList<PackElement>();
							if (type == 1)
							{
								pes = getElements2(pi, 1);
							}
							else
							{
								pes = getElements(pi, type);
							}
							if (type == 1)// card
							{
								for (int i = pes.size() - 1; i >= 0; i--)
								{
									CardData cData = CardData.getData(pes.get(i).dataId);
									if (cData != null)
									{
										// 经验卡，金币卡，突破卡
										if (cData.element == 5 || cData.race == 6 || cData.race == 7 || pes.get(i).getUse() == 1)
										{
											pes.remove(i);
										}
									}
								}
							}
							else if (type == 2)// skill
							{
								for (int i = pes.size() - 1; i >= 0; i--)
								{
									SkillData sData = SkillData.getData(pes.get(i).dataId);
									if (sData != null)
									{
										// 等级满级的卡或者不能强化升级的卡
										// new
										if (pes.get(i).lv >= pi.player.getLevel() || sData.exptype == 2)
										{
											pes.remove(i);
										}
									}
								}
							}
							if (type == 3)// passiveskill
							{
								for (int i = pes.size() - 1; i >= 0; i--)
								{
									PassiveSkillData psData = PassiveSkillData.getData(pes.get(i).getDataId());
									if (psData != null)
									{
										// 等级满级的卡或者不能强化升级的卡
										if (psData.level == 10)
										{
											pes.remove(i);
										}
									}
								}
							}
							if (type == 4)// equip
							{
								for (int i = pes.size() - 1; i >= 0; i--)
								{
									// 装备等级大于等于任务本身等级*3，不能强化
									if (pes.get(i).getLv() >= pi.player.getLevel() * 3)
									{
										pes.remove(i);
									}
								}
							}
							if (type == 1)// card
							{
								pes = orderIntensify(pi, pes, 1);
								if (pi.player.getNewPlayerType() == 8)// 新手强化，孙悟空的位置信息固定
								{
									List<PackElement> pList = new ArrayList<PackElement>();
									if (pes.get(0).getDataId() == 14001)
									{
										PackElement p1 = pes.get(0);
										pes.set(0, pes.get(1));
										pes.set(1, p1);
									}
									else if (pes.get(1).getDataId() != 14001)
									{
										pList.add(pes.get(0));
										for (int m = 1; m < pes.size(); m++)
										{
											if (pes.get(m).getDataId() == 14001)
											{
												pList.add(pes.get(m));
											}
										}
										for (int m = 1; m < pes.size(); m++)
										{
											if (pes.get(m).getDataId() != 14001)
											{
												pList.add(pes.get(m));
											}
										}
										pes = pList;
									}
								}
							}
							else
							{
								pes = orderPackElement2(pes, type);
							}
							
							int t = type;
							if (t == 3)
							{
								t = 8;
							}
							else if (t == 4)
							{
								t = 3;
							}
							prj.pejs = getPackElementJsons(pes, pi, t);
						}
						else
						{
							prj.errorCode = 56;
						}
						msg = JSON.toJSONString(prj);
					}
					// 可以被强化的物品
					else if (uj.ui == 8)
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|被吞噬列表");
						ej = new SelectCardResultJson();
						SelectCardResultJson scrj = (SelectCardResultJson) ej;
						int type = uj.getType();
						List<PackElement> pes = getElements(pi, type);
						int show = uj.show;
						CardData cardData = CardData.getData(pes.get(uj.i).getDataId());
						if (type == 1)
						{
							PackElement p = getPe(pi, type, uj.i);
							// 在卡组中，获取指定技能
							if (p.getUse() == 1)
							{
								List<PackElement> list = getCardGroupPEs(pi, uj.i);
								for (PackElement pe : list)
								{
									if (pe.geteType() == 2)
									{
										scrj.i = pe.getDataId();
										break;
									}
								}
							}
							// 卡牌本身自带的技能
							else
							{
								scrj.i = cardData.basicskill;
							}
						}
						for (int i = pes.size() - 1; i >= 0; i--)
						{
							if (type == 1)
							{
								CardData cData = CardData.getData(pes.get(i).getDataId());
								// 如果在卡组中或者已经被选为强化卡则不能被强化
								if (pes.get(i).use == 1 || pes.get(i).getI() == uj.i)
								{
									pes.remove(i);
								}
								
								// 同族显示
								else if (show == 1 && cData.race != cardData.race)
								{
									pes.remove(i);
								}
								// 突破卡时，如果突破卡的星级与主卡不同时，不能被强化
								// new 突破卡不能被强化
								else if (cData.race == 6)
								{
									pes.remove(i);
								}
								else if (cData.race == 7)// 金币卡
								{
									pes.remove(i);
								}
							}
							else
							{
								if (pes.get(i).use == 1 || pes.get(i).getI() == uj.i)
								{
									pes.remove(i);
								}
							}
						}
						pes = intensifyOrder(pi, pes, uj.i, 0, type);
						scrj.pes = pes;
						msg = JSON.toJSONString(scrj);
					}
					else if (uj.ui == 9)
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|");
						ej = new PackResultJson();
						PackResultJson prj = (PackResultJson) ej;
						List<PackElement> pes = getCardGroupPEs(pi, uj.i);
						List<PackElement> pes2 = getCardGroupPEs2(pi, uj.i);
						prj.list = pes;
						prj.list2 = pes2;
						msg = JSON.toJSONString(prj);
					}
					else if (uj.ui == 11)// 刷新援护玩家
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|刷新援护玩家");
						ej = new FriendResultJson();
						FriendResultJson frj = (FriendResultJson) ej;
						List<String> helpIds = pi.getHelpers();
						boolean fixRobot = false;// 在第一个位置固定刷一个人
						if (pi.player.getNewPlayerType() == 12)
						{
							fixRobot = true;
						}
						frj.list = createFriendElements(helpIds, fixRobot);
						frj.times = pi.player.getInviteFriendTimes();
						frj.cost = Statics.getNextInviteFriendCost(pi.player.getLevel(), pi.player.getInviteFriendTimes());
						msg = JSON.toJSONString(frj);
					}
					else if (uj.ui == 12)// 合成物品
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|合成物品列表");
						ej = new ComposeResultJson();
						ComposeResultJson crj = (ComposeResultJson) ej;
						if (checkMode(4, pi))// 是否解锁
						{
							int equip = 0;
							int card = 0;
							int item = 0;
							int skill = 0;
							List<String> list = new ArrayList<String>();
							for (int j = 1; j < 5; j++)
							{
								List<ComposeData> comDataList = ComposeData.getComposeDatasBytype(j);
								HashMap<Integer, ComposeData> comMap = ComposeData.getComposeDataMap(j, pi);
								
								if (comDataList != null && comDataList.size() > 0)
								{
									for (ComposeData comData : comDataList)
									{
										// 可制作为1，否则为0
										int i = 0;
										int k = 0;
										if (comData != null)
										{
											if (comMap.containsKey(comData.composite))// 解锁
											{
												if (checkCompose(comData, pi))// 判断是否可以制作
												{
													i = 1;
													switch (comData.type)
													{
														case 1:// card
															if (card == 0)
															{
																card = 1;
															}
															break;
														case 2:// equip
															if (equip == 0)
															{
																equip = 1;
															}
															break;
														case 3:// item
															if (item == 0)
															{
																item = 1;
															}
															break;
														case 4:// skill
															if (skill == 0)
															{
																skill = 1;
															}
															break;
														default:
															break;
													}
												}
												k = 1;
											}
											if (k == 0)
											{
												continue;
											}
											else
											{
												if (j == uj.type)
												{
													list.add(comData.composite + "-" + i + "-" + k);
												}
											}
										}
									}
								}
							}
							
							List<String> list1 = compose(list, uj.type);
							crj.cs = list1;
							for (int j = 1; j < 5; j++)
							{
								List<ComposeData> coms = ComposeData.getComposeDataLocked(pi, j);
								for (ComposeData com : coms)
								{
									if (com.style > 0)
									{
										if (!pi.getComposeMap(pi).containsKey(com.composite + ""))
										{
											pi.player.setCompose(pi.player.getCompose() + "," + com.composite);
											switch (com.type)
											{
												case 1:// card
													if (card == 0)
													{
														card = 1;
													}
													break;
												case 2:// equip
													if (equip == 0)
													{
														equip = 1;
													}
													break;
												case 3:// item
													if (item == 0)
													{
														item = 1;
													}
													break;
												case 4:// skill
													if (skill == 0)
													{
														skill = 1;
													}
													break;
											}
										}
									}
								}
							}
							
							List<String> ss = new ArrayList<String>();
							ss.add(1 + "-" + card);
							ss.add(2 + "-" + equip);
							ss.add(3 + "-" + item);
							ss.add(4 + "-" + skill);
							crj.mark = ss;
						}
						else
						{
							crj.errorCode = 56;
						}
						msg = JSON.toJSONString(crj);
					}
					else if (uj.ui == 14)// 合成所需材料列表
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|合成所需材料列表");
						ej = new ComposeResultJson();
						ComposeResultJson crj = (ComposeResultJson) ej;
						List<PackElement> pes = composeMaterial(uj.i, uj.type, pi);
						crj.pes = pes;
						
						msg = JSON.toJSONString(crj);
					}
					else if (uj.ui == 18)// 所有迷宫列表
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|迷宫列表");
						ej = new MazeResultJson();
						MazeResultJson mrj = (MazeResultJson) ej;
						if (checkMode(11, pi))
						{
							List<String> s = new ArrayList<String>();
							s = MazeData.getMazes(pi);
							if (s != null && s.size() > 0)
							{
								String m = s.get(s.size() - 1);
								if (m != null && m.length() > 0)
								{
									mrj.mId = StringUtil.getInt(m.split("-")[0]);
								}
							}
							List<String> list = MazeData.getMazeDatas(pi);
							mrj.s = list;
							mrj.i = list.size();
							if (pi.player.getMaze() == null)
							{
								mrj.maze = "";
							}
							else
							{
								mrj.maze = pi.player.getMaze();
							}
							int cdtime = 0;
							if (pi.mazecdtime != 0)
							{
								cdtime = (int) (System.currentTimeMillis() - pi.mazecdtime) / 1000;
								if (cdtime >= 10 * 60)
								{
									mrj.cdtime = 0;
								}
								else
								{
									mrj.cdtime = 10 * 60 - cdtime;
								}
							}
							mrj.mazeBossDrop = pi.player.getMazeBossDrop();
							mrj.mazeWish = pi.player.getMazeWish();
						}
						else
						{
							mrj.errorCode = 56;
						}
						msg = JSON.toJSONString(mrj);
					}
					
					else if (uj.ui == 19)// 进入迷宫
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|进入迷宫");
						ej = new MazeResultJson();
						MazeResultJson mrj = (MazeResultJson) ej;
						String maze = pi.player.getMaze();
						// 校验阶段
						boolean aa = true;
						MazeData mData = MazeData.getMazeData(uj.i);
						// 校验当前进入的迷宫是否解锁
						int errorcode = 0;
						if (errorcode == 0)// 校验冷却时间
						{
							if (pi.mazecdtime != 0)
							{
								int cdtime = (int) (System.currentTimeMillis() - pi.mazecdtime) / 1000;
								if (cdtime < 10 * 60)
								{
									errorcode = 86;
								}
							}
						}
						if (errorcode == 0)
						{
							if (mData.condition0 == 1)// 等级解锁
							{
								if (mData.condition > pi.player.getLevel())
								{
									aa = false;
									mrj.errorCode = 25;
								}
							}
							else if (mData.condition0 == 2)// 关卡解锁
							{
								if (mData.condition > pi.player.getMissionId())
								{
									aa = false;
									mrj.errorCode = 25;
								}
							}
							if (pi.player.getNewPlayerType() > 17)// 过了新手后的迷宫。校验体力
							{
								if (pi.player.getPower() < mData.energy)
								{
									mrj.errorCode = 27;
									aa = false;
								}
							}
							// 进入迷宫校验背包-skill，item
							if (pi.getSkills().size() >= Statics.getPackageNum(pi) || pi.getItems().size() >= Statics.getPackageNum(pi))
							{
								mrj.errorCode = 53;
							}
							// 校验进入次数
							if (aa)
							{
								if (maze != null && maze.length() > 0)
								{
									String[] mazestr = maze.split(",");
									// 第一次进入此迷宫
									for (int i = 0; i < mazestr.length; i++)
									{
										String[] mazes = mazestr[i].split("-");
										if (mazes[0] != null && mazes[0].length() > 0 && mazes[2] != null && mazes[2].length() > 0)
										{
											if (Integer.parseInt(mazes[0]) == uj.i)
											{
												// 校验进入两次之后，金钱是否足够付费重置
												if (Integer.parseInt(mazes[2]) >= mData.freeentry)
												{
													if (mazes[3] != null && mazes[3].length() > 0)
													{
														VipData vData = VipData.getVipData(pi.player.getVipLevel());
														if (vData.maze > 0)
														{
															if (Integer.parseInt(mazes[3]) < vData.maze)// 可以付费重置
															{
																MazeCostData mCostData = MazeCostData.getMazeCostData(StringUtil.getInt(mazes[3]) + 1);
																if (mCostData != null)
																{
																	if (pi.player.getTotalCrystal() < mCostData.cost)// 水晶数不足
																	{
																		aa = false;
																		mrj.errorCode = 19;
																	}
																}
																else
																{
																	aa = false;
																}
															}
															else
															{
																aa = false;
																mrj.errorCode = 60;
															}
														}
														else
														{
															aa = false;
															mrj.errorCode = 70;
														}
													}
												}
												break;
											}
										}
									}
								}
							}
						}
						// 功能阶段
						if (aa && errorcode == 0)
						{
							if (maze != null && maze.length() > 0)
							{
								String[] mazestr = maze.split(",");
								// 第一次进入此迷宫
								boolean a = true;
								for (int i = 0; i < mazestr.length; i++)
								{
									String[] mazes = mazestr[i].split("-");
									if (mazes[0] != null && mazes[0].length() > 0)
									{
										if (Integer.parseInt(mazes[0]) == uj.i)
										{
											if (mazes[1] != null && mazes[1].length() > 0)
											{
												mrj.t = 0;
												// mazes[1] = 0 + "";//
												// 每次进入迷宫清空进度
												// mazes[2] =
												// StringUtil.getInt(mazes[2]) +
												// 1 + "";
												mrj.state = Integer.parseInt(mazes[1]);
												mrj.num = Integer.parseInt(mazes[2]);
											}
											mazestr[i] = mazes[0] + "-" + mazes[1] + "-" + mazes[2] + "-" + mazes[3];
											a = false;
											break;
										}
									}
								}
								if (!a)
								{
									String newStr = "";
									for (int j = 0; j < mazestr.length; j++)
									{
										if (mazestr[j] != null && mazestr[j].length() > 0)
										{
											if (newStr == null || newStr.length() == 0)
											{
												newStr = mazestr[j];
											}
											else
											{
												newStr = newStr + "," + mazestr[j];
											}
										}
										else
										{
											continue;
										}
									}
									pi.player.setMaze(newStr);
								}
								if (a)
								{
									mrj.state = 0;
									mrj.num = 0;
								}
								// 第一次进入迷宫，将迷宫信息直接追加到原有字符串,并且扣除体力
								if (a)
								{
									String str = "";
									str = uj.i + "-" + "0" + "-" + "0" + "-" + "0";
									mrj.t = 0;
									pi.player.setMaze(pi.player.getMaze() + "," + str);
								}
							}
							else
							{
								pi.player.setMaze(uj.i + "-" + "0" + "-" + "0" + "-" + "0");
								mrj.t = 0;
							}
							// // 进入迷宫扣除体力
							// if (pi.player.getNewPlayerType() != 17)
							// {
							// pi.player.removePower(mData.energy);
							// }
							// // 每次进入迷宫清空血瓶购买次数
							// if (pi.player.getBloodBuffNum() > 0)
							// {
							// pi.player.setBloodBuffNum(0);
							// }
							mrj.td = uj.i;
//							List<PackElement> pes = new ArrayList<PackElement>();
//							for (int i = 0; i < 6; i++)
//							{
//								Card card = pi.getCardGroup().getCard(i);
//								PackElement pe = null;
//								if (card != null)
//								{
//									pe = Statics.createPackElement(pi.getCards().indexOf(card), card, pi);
//								}
//								pes.add(pe);
//							}
							//mrj.pes = pes;
							// 接着上次的进度
							if (mrj.state >= 0)
							{
								boolean is = true;
								String s1 = "";
								String s2 = "";
								if (pi.mazes != null && pi.mazes.size() > 0)
								{
									for (Maze maze2 : pi.mazes)
									{
										if (maze2.getMazeId() == uj.i)
										{
											is = false;
											String[] cards = maze2.getCardInfo().split(",");
											String[] bloods = maze2.getBloodInfo().split("-");
											String[] maxBloods = maze2.getMaxbloodInfo().split("-");
											for (int i = 0; i < cards.length; i++)
											{
												String cardId = cards[i].split("-")[0];
												String blood = bloods[i];
												String maxBlood = maxBloods[i];
												if (i == 0)
												{
													s1 = s1 + cardId + "-" + blood;
													s2 = s2 + cardId + "-" + maxBlood;
												}
												else
												{
													s1 = s1 + "&" + cardId + "-" + blood;
													s2 = s2 + "&" + cardId + "-" + maxBlood;
												}
											}
										}
									}
									
								}
								if (is)
								{
									pi.mazes.add(Maze.createMaze(pi, uj.i));
									if (pi.mazes != null && pi.mazes.size() > 0)
									{
										for (Maze maze2 : pi.mazes)
										{
											if (maze2.getMazeId() == uj.i)
											{
												String[] cards = maze2.getCardInfo().split(",");
												String[] bloods = maze2.getBloodInfo().split("-");
												String[] maxBloods = maze2.getMaxbloodInfo().split("-");
												for (int i = 0; i < cards.length; i++)
												{
													String cardId = cards[i].split("-")[0];
													String blood = bloods[i];
													String maxBlood = maxBloods[i];
													if (i == 0)
													{
														s1 = s1 + cardId + "-" + blood;
														s2 = s2 + cardId + "-" + maxBlood;
													}
													else
													{
														s1 = s1 + "&" + cardId + "-" + blood;
														s2 = s2 + "&" + cardId + "-" + maxBlood;
													}
												}
											}
										}
									}
									// 玩家添加血瓶
									if (pi.player.getNewPlayerType() == 17)// 新手送两个
									{
										if (pi.getTargetItemsNum(130001) < 2)
										{
											pi.addItem(130001, 2);
										}
									}
									else
									{
										int number = pi.getTargetItemsNum(130001);
										if (number < 1)
										{
											pi.addItem(130001, 1);
										}
									}
								}
								mrj.cb = s1;
								mrj.mcb = s2;
							}
							else
							{
								if (pi.mazes == null && pi.mazes.size() > 0)
								{
									pi.mazes.add(Maze.createMaze(pi, uj.i));
								}
								else
								{
									pi.mazes.add(Maze.createMaze(pi, uj.i));
								}
								String s1 = "";
								String s2 = "";
								if (pi.mazes != null && pi.mazes.size() > 0)
								{
									for (Maze maze2 : pi.mazes)
									{
										if (maze2.getMazeId() == uj.i)
										{
											
											String[] cards = maze2.getCardInfo().split(",");
											String[] bloods = maze2.getBloodInfo().split("-");
											String[] maxBloods = maze2.getMaxbloodInfo().split("-");
											for (int i = 0; i < cards.length; i++)
											{
												String cardId = cards[i].split("-")[0];
												String blood = bloods[i];
												String maxBlood = maxBloods[i];
												if (i == 0)
												{
													s1 = s1 + cardId + "-" + blood;
													s2 = s2 + cardId + "-" + maxBlood;
												}
												else
												{
													s1 = s1 + "&" + cardId + "-" + blood;
													s2 = s2 + "&" + cardId + "-" + maxBlood;
												}
											}
										}
									}
								}
								mrj.cb = s1;
								mrj.mcb = s2;
								// 玩家添加血瓶
								if (pi.player.getNewPlayerType() == 17)// 新手送两个
								{
									if (pi.getTargetItemsNum(130001) < 2)
									{
										pi.addItem(130001, 2);
									}
								}
								else
								{
									int number = pi.getTargetItemsNum(130001);
									if (number < 1)
									{
										pi.addItem(130001, 1);
									}
								}
							}
							// 血瓶数量
							mrj.number = pi.getTargetItemsNum(130001);
						}
						if (errorcode != 0)
						{
							mrj.errorCode = errorcode;
						}
						msg = JSON.toJSONString(mrj);
					}
					else if (uj.ui == 20)// 迷宫随机获得物品
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|迷宫随机获得物品");
						ej = new MazeResultJson();
						MazeResultJson mrj = (MazeResultJson) ej;
						int randnum = Random.getNumber(0, 1000);
						// randnum =560;
						MazeData mData = MazeData.getMazeData(uj.i);
						boolean mazeGetThings = true;
						if (mData != null)
						{
							// 校验进度
							String str = pi.player.getMaze();
							if (str != null && str.length() > 0)
							{
								String[] maze = str.split(",");
								for (int j = 0; j < maze.length; j++)
								{
									String[] mazes = maze[j].split("-");
									if (mazes[0] != null && mazes[0].length() > 0)
									{
										if (Integer.parseInt(mazes[0]) == uj.i)
										{
											if (uj.show <= StringUtil.getInt(mazes[1]))
											{
												mazeGetThings = false;// 客户端传的进度小于等于服务器的进度，则不用随机物品，亦不用改变服务器的进度信息
											}
//											else
//											{
//												maze[j] = uj.i + "-" + uj.show + "-" + mazes[2] + "-" + mazes[3];
//											}
											break;
										}
									}
									else
									{
										continue;
									}
								}
//								if (mazeGetThings)// 需要随机物品时，修改进度信息
//								{
//									String newStr = "";
//									// 改变进度
//									for (int i = maze.length - 1; i >= 0; i--)
//									{
//										if (maze[i] != null && maze[i].length() > 0)
//										{
//											if (newStr == null || newStr.length() == 0)
//											{
//												newStr = maze[i];
//											}
//											else
//											{
//												newStr = newStr + "," + maze[i];
//											}
//										}
//										else
//										{
//											continue;
//										}
//									}
//									pi.player.setMaze(newStr);
//								}
							}
							if (mazeGetThings)// 随机物品 type:1治疗 2号角 3炸弹 4金币 5精英战斗
							// 6普通战斗 7boss
							{
								
								if (uj.show > mData.step || uj.show <= 0)
								{
									mrj.errorCode = 121;
								}
								else
								{
									// List<String> expPro =mData.exp_pro;
									int start = 0;
									boolean a = true;
									// 精英战斗5
									if (uj.show % 5 == 0)// 五的倍数，则为精英战斗
									{
										mrj.type = 5;
										mrj.i = 0;
										mrj.state = uj.show;
									}
									else
									{
										// 金币4
										if (a)
										{
											List<String> goldPro = mData.gold_pro;
											for (int i = 0; i < goldPro.size(); i++)
											{
												String[] s = goldPro.get(i).split("-");
												if (s[1] != null && s[1].length() > 0 && s[0] != null && s[0].length() > 0)
												{
													int end = Integer.parseInt(s[1]) + start;
													if (randnum > start && randnum <= end)
													{
														mrj.i = Integer.parseInt(s[0]);
														mrj.type = 4;
														mrj.state = uj.show;
														pi.player.addGold((int) Cache.getGoldDropMul() * Integer.parseInt(s[0]));
														a = false;
														break;
													}
													start = end;
												}
											}
										}
										// 道具2
										if (a)
										{
											List<String> itemPro = mData.item_pro;
											for (int i = 0; i < itemPro.size(); i++)
											{
												String[] s = itemPro.get(i).split("-");
												if (s[1] != null && s[1].length() > 0 && s[0] != null && s[0].length() > 0)
												{
													int end = Integer.parseInt(s[1]) + start;
													if (randnum > start && randnum <= end)
													{
														mrj.i = Integer.parseInt(s[0]);
														mrj.type = 2;
														mrj.state = uj.show;
														pi.addItem(Integer.parseInt(s[0]), 1);
														a = false;
														break;
													}
													start = end;
												}
											}
										}
										// 治疗1
										if (a)
										{
											List<String> addPro = mData.add_pro;
											for (int i = 0; i < addPro.size(); i++)
											{
												String[] s = addPro.get(i).split("-");
												if (s[1] != null && s[1].length() > 0 && s[0] != null && s[0].length() > 0)
												{
													int end = Integer.parseInt(s[1]) + start;
													if (randnum > start && randnum <= end)
													{
														int rand = Random.getNumber(0, 1000);
														List<BloodBuffData> bds = BloodBuffData.getBloodBuffDatas(StringUtil.getInt(s[0]));
														int en = 0;
														int st = 0;
														for (int k = 0; k < bds.size(); k++)
														{
															if (bds.get(k) != null)
															{
																en = bds.get(k).probability + st;
																if (rand > st && rand <= en)
																{
																	mrj.i = bds.get(k).num;
																	mrj.type = 1;
																	mrj.state = uj.show;
																	a = false;
																	break;
																}
																st = en;
															}
														}
														break;
													}
													start = end;
												}
											}
											// 修改玩家进入迷宫卡牌血量
											List<Maze> mazes = pi.mazes;
											if (mazes != null && mazes.size() > 0)
											{
												for (Maze maze : mazes)
												{
													BloodBuffData bfd = BloodBuffData.getBloodBuffData(mrj.i);
													if (maze.getMazeId() == uj.i && bfd != null)
													{
														String s = "";
														String[] blood = maze.getBloodInfo().split("-");
														String[] maxblood = maze.getMaxbloodInfo().split("-");
														for (int i = 0; i < blood.length; i++)
														{
															int num = (int) (StringUtil.getInt(blood[i]) * (1 + bfd.effect));
															int maxnum = StringUtil.getInt(maxblood[i]);
															if (num > maxnum)
															{
																num = maxnum;
															}
															if ("".equals(s))
															{
																s = s + num;
															}
															else
															{
																s = s + "-" + num;
															}
														}
														maze.setBloodInfo(s);
														pi.mazes.remove(maze);
														pi.mazes.add(maze);
													}
												}
											}
										}
										// 炸弹3
										if (a)
										{
											List<String> cutPro = mData.cut_pro;
											for (int i = 0; i < cutPro.size(); i++)
											{
												String[] s = cutPro.get(i).split("-");
												if (s[1] != null && s[1].length() > 0 && s[0] != null && s[0].length() > 0)
												{
													int end = Integer.parseInt(s[1]) + start;
													if (randnum > start && randnum <= end)
													{
														int rand = Random.getNumber(0, 1000);
														List<BloodBuffData> bds = BloodBuffData.getBloodBuffDatas(StringUtil.getInt(s[0]));
														int en = 0;
														int st = 0;
														for (int k = 0; k < bds.size(); k++)
														{
															
															if (bds.get(k) != null)
															{
																en = bds.get(k).probability + st;
																if (rand > st && rand <= en)
																{
																	mrj.i = bds.get(k).num;
																	mrj.type = 3;
																	mrj.state = uj.show;
																	a = false;
																	break;
																}
																st = en;
															}
														}
														break;
													}
													start = end;
												}
											}
											// 修改玩家进入迷宫卡牌血量
											List<Maze> mazes = pi.mazes;
											if (mazes != null && mazes.size() > 0)
											{
												for (Maze maze : mazes)
												{
													BloodBuffData bfd = BloodBuffData.getBloodBuffData(mrj.i);
													if (maze.getMazeId() == uj.i && bfd != null)
													{
														String s = "";
														String[] blood = maze.getBloodInfo().split("-");
														for (int i = 0; i < blood.length; i++)
														{
															int num = (int) (StringUtil.getInt(blood[i]) * (1 - bfd.effect));
															if (num < 1)
															{
																num = 1;
															}
															if (i == 0)
															{
																s = s + num;
															}
															else
															{
																s = s + "-" + num;
															}
														}
														maze.setBloodInfo(s);
														pi.mazes.remove(maze);
														pi.mazes.add(maze);
													}
												}
											}
										}
										
										// 普通战斗6
										if (a)
										{
											if (pi.player.getNewPlayerType() == 17)// 新手迷宫
											{
												mrj.type = 4;
												mrj.state = uj.show;
												mrj.i = 2000;
												pi.player.addGold(2000);
											}
											else
											{
												mrj.type = 6;
												mrj.i = 0;
												mrj.state = uj.show;
											}
										}
									}
								}
								mrj.td = uj.i;
								mrj.lv = pi.player.getLevel();
								pi.mrj = mrj;
								// cardid-blood&cardid-blood
								List<Maze> mazes = pi.mazes;
								String s = "";
								boolean is = false;
								if (mazes != null && mazes.size() > 0)
								{
									for (Maze maze : mazes)
									{
										if (maze.getMazeId() == uj.i)
										{
											is = true;
											String[] cards = maze.getCardInfo().split(",");
											String[] bloods = maze.getBloodInfo().split("-");
											for (int i = 0; i < cards.length; i++)
											{
												String[] card = cards[i].split("-");
												String cardId = card[0];
												String blood = bloods[i];
												if (i == 0)
												{
													s = s + cardId + "-" + blood;
												}
												else
												{
													s = s + "&" + cardId + "-" + blood;
												}
											}
										}
									}
									if (is)
									{
										mrj.cb = s;
									}
									else
									{
										logger.info("迷宫进度cardid-blood数据出错!");
										mrj.cb = "";
									}
								}
								else
								{
									logger.info("玩家迷宫进度数据为空");
									mrj.cb = "";
								}
								if (mazeGetThings)
								{
									String str1 = pi.player.getMaze();
									if (str1 != null && str1.length() > 0)
									{
										String[] maze = str1.split(",");
										for (int j = 0; j < maze.length; j++)
										{
											String[] mazes1 = maze[j].split("-");
											if (mazes1[0] != null && mazes1[0].length() > 0)
											{
												if (Integer.parseInt(mazes1[0]) == uj.i)
												{
													if (uj.show <= StringUtil.getInt(mazes1[1]))
													{
														mazeGetThings = false;// 客户端传的进度小于等于服务器的进度，则不用随机物品，亦不用改变服务器的进度信息
													}
													else
													{
														maze[j] = uj.i + "-" + uj.show + "-" + mazes1[2] + "-" + mazes1[3];
													}
													break;
												}
											}
											else
											{
												continue;
											}
										}
										if (mazeGetThings && (mrj.type == 1 || mrj.type == 2 || mrj.type == 3 || mrj.type == 4))// 需要随机物品时，修改进度信息
										{
											String newStr = "";
											// 改变进度
											for (int i = maze.length - 1; i >= 0; i--)
											{
												if (maze[i] != null && maze[i].length() > 0)
												{
													if (newStr == null || newStr.length() == 0)
													{
														newStr = maze[i];
													}
													else
													{
														newStr = newStr + "," + maze[i];
													}
												}
												else
												{
													continue;
												}
											}
											pi.player.setMaze(newStr);
										}
									}
								}
							}
							else
							// 无须随机时
							{
								mrj = pi.mrj;
							}
						}
						msg = JSON.toJSONString(mrj);
					}
					else if (uj.ui == 21)// 进入冥想界面
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|冥想界面");
						ej = new ImaginationResultJson();
						ImaginationResultJson irj = (ImaginationResultJson) ej;
						if (checkMode(10, pi))
						{
							int packageNum = Statics.getPackageNum(pi);
							if (pi.getPassiveSkillls().size() >= packageNum)
							{
								irj.i = 0;
							}
							else
							{
								irj.i = packageNum - pi.getPassiveSkillls().size();// 背包容量
							}
							if (pi.player.getNewPlayerType() == 24)// 新手灵界(冥想)
							{
								pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
							}
							irj.id = pi.player.getImagination();
							irj.g = pi.player.getGold();
							if (pi.player.getMid() == 0 && pi.player.getMnum() == 0)
							{
								int mrandom = pi.player.getMrandom();
								if (mrandom>0)
								{
									MeditationData mData = MeditationData.getData(mrandom);
									pi.player.setMid(mrandom);
									pi.player.setMnum(0);
									pi.player.setMrandom(mData.random);
								}
								if (mrandom<0)
								{
									MeditationData mData = MeditationData.getMeditationDataByRandom(mrandom);
									int mid = Random.getNumber((mData.id + 1), MeditationData.getDatas().size());
									pi.player.setMid(mid);
									pi.player.setMnum(0);
								}
							}
							irj.mid = pi.player.getMid();
							irj.mnum = pi.player.getMnum();
						}
						else
						{
							irj.errorCode = 56;
						}
						msg = JSON.toJSONString(irj);
					}
					else if (uj.ui == 22) // 自动回复体力
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|自动回复体力");
						ej = new PowerResultJson();
						PowerResultJson prj = (PowerResultJson) ej;
						if (pi.player.getPower() >= Constant.MaxPower)
						{
							prj.errorCode = 51;
						}
						else
						{
							if (System.currentTimeMillis() - pi.player.getLastRestorePowerTime() < Constant.AutoRestorePowerTime)
							{
								prj.errorCode = 52;
							}
							else
							{
								pi.player.setLastRestorePowerTime(System.currentTimeMillis());
								pi.player.addPower(1, true);
								prj.p = pi.player.getPower();
							}
						}
						msg = JSON.toJSONString(prj);
					}
					else if (uj.ui == 23)// 地图选择
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|大地图");
						ej = new MapResultJson();
						MapResultJson mrj = (MapResultJson) ej;
						mrj.m1 = pi.player.getMissionId();
						mrj.m2 = pi.player.getMissionId2();
						mrj.star1 = pi.player.getCompleteStar1();
						mrj.star2 = pi.player.getCompleteStar2();
						mrj.b1 = pi.player.getBouns1();
						mrj.b2 = pi.player.getBouns2();
						mrj.t1 = pi.player.getTimes1();
						mrj.t2 = pi.player.getTimes2();
						mrj.c = pi.player.getTotalCrystal();
						mrj.d = pi.player.getGold();
						mrj.kopoint = pi.player.getKopoint();
						String koExchange = pi.player.getKoExchange();
						HashMap<Integer, String> koMap = new HashMap<Integer, String>();
						if (koExchange != null && koExchange.length() > 0)
						{
							String[] temp = koExchange.split(",");
							for (int i = 0; i < temp.length; i++)
							{
								if (!koMap.containsKey(StringUtil.getInt(temp[i])))
								{
									koMap.put(StringUtil.getInt(temp[i]), temp[i]);
								}
							}
						}
						List<KoAwardData> dataList = KoAwardData.getAwardDatas();
						int t = 0;
						for (KoAwardData kd : dataList)
						{
							if (kd.number <= pi.player.getKopoint())
							{
								if (!koMap.containsKey(kd.id))
								{
									t = 1;
									break;
								}
							}
						}
						mrj.koType = t;
						mrj.fType = getNewTypeFormation(pi);
						msg = JSON.toJSONString(mrj);
					}
					else if (uj.ui == 24)// 进入活动副本主界面
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|活动副本主界面");
						ej = new EventResultJson();
						EventResultJson erj = (EventResultJson) ej;
						if (checkMode(8, pi))
						{
							int week = StringUtil.getWeekDay(System.currentTimeMillis());
							List<String> s = new ArrayList<String>();
							List<EventData> eDatas = EventData.getEvents();
							HashMap<Integer, Event> eMap = Cache.getInstance().getEventMap();
							int hour = StringUtil.getHour(System.currentTimeMillis());
							int num = 0;
							for (int k = 0; k < eDatas.size(); k++)// 全部开启的副本list
							{
								EventData eData = eDatas.get(k);
								if (eData.positionid == 1)// 金币副本
								{
									s.add(eData.ID + "-" + 1 + "-" + hour);
								}
								if (eData.positionid == 2 && eData.timestyle == week)// 经验副本
								{
									s.add(eData.ID + "-" + 1 + "-" + hour);
								}
								if (eData.positionid == 3)// 特殊副本
								{
									if (eMap != null && eMap.containsKey(eData.ID))
									{
										Event e = eMap.get(eData.ID);
										if (e.getSell() == 1)
										{
											s.add(eData.ID + "-" + 1 + "-" + StringUtil.getdays(e.getEnd()));
										}
									}
								}
								if (eData.nametype == 1)
								{
									String str = pi.getFBnumMap1().get(801);
									if (str != null)
									{
										String[] sn = str.split("-");
										num = StringUtil.getInt(sn[1]);
									}
								}
							}
							erj.num = num;
							if (s.size() < 3)// 若s.size<3,则增加一个未开启的特殊副本
							{
								for (int k = 0; k < eDatas.size(); k++)
								{
									EventData eData = eDatas.get(k);
									if (s.size() == 2 && eDatas.get(k).positionid == 3)
									{
										if (eMap != null && eMap.containsKey(eData.ID))
										{
											s.add(eData.ID + "-" + 0 + "-" + 0);
											break;
										}
									}
								}
							}
							if (pi.player.getNewPlayerType() == 23)// 新手活动副本(异世界)
							{
								pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
							}
							if (s.size() == 3)
							{
								erj.s = s;
							}
							else
							{
								erj.errorCode = 82;
							}
							
						}
						else
						{
							erj.errorCode = 56;
						}
						msg = JSON.toJSONString(erj);
					}
					
					else if (uj.ui == 25)// 副本选择关卡界面
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|副本选择关卡界面");
						ej = new EventResultJson();
						EventResultJson erj = (EventResultJson) ej;
						int id = uj.type;// 副本id
						EventData eData = EventData.getEventData(id);
						List<String> s = new ArrayList<String>();
						HashMap<Integer, String> fbMap1 = pi.getFBnumMap1();
						HashMap<Integer, String> fbMap2 = pi.getFBnumMap2();
						HashMap<Integer, String> fbMap = null;
						if (eData.fbid != null && eData.fbid.size() > 0)
						{
							if (eData.positionid <= 2)
							{
								fbMap = fbMap1;
							}
							else
							{
								fbMap = fbMap2;
							}
							for (int k = 0; k < eData.fbid.size(); k++)
							{
								FBeventData fbd = FBeventData.getFBeventData(eData.fbid.get(k));
								if (fbMap != null)
								{
									if (fbMap.containsKey(eData.fbid.get(k)))
									{
										if (pi.player.getLevel() < fbd.unlocklevel)
										{
											s.add(fbMap.get(eData.fbid.get(k)) + "-" + "0");
										}
										else
										{
											s.add(fbMap.get(eData.fbid.get(k)) + "-" + "1");
										}
									}
									else
									{
										if (pi.player.getLevel() < fbd.unlocklevel)
										{
											s.add(eData.fbid.get(k) + "-" + fbd.unlocklevel + "-" + "0");
										}
										else
										{
											s.add(eData.fbid.get(k) + "-" + 0 + "-" + 1);
										}
									}
								}
								else
								{
									if (pi.player.getLevel() < fbd.unlocklevel)
									{
										s.add(eData.fbid.get(k) + "-" + fbd.unlocklevel + "-" + 0);
									}
									else
									{
										s.add(eData.fbid.get(k) + "-" + 0 + "-" + 1);
									}
								}
							}
						}
						erj.s = s;
						erj.id = id;
						long cdtime = 0;
						if (eData.positionid == 1)
						{
							cdtime = pi.eventcdtime1;
						}
						else if (eData.positionid == 2)
						{
							cdtime = pi.eventcdtime2;
						}
						else if (eData.positionid == 3)
						{
							cdtime = pi.eventcdtime3;
						}
						else
						{
							errorlogger.info("活动副本位置编号出错");
						}
						if (cdtime != 0)
						{
							int sTime = (int) (System.currentTimeMillis() - cdtime) / 1000;
							if (sTime >= 10 * 60)
							{
								erj.cdtime = 0;
							}
							else
							{
								erj.cdtime = 10 * 60 - sTime;
							}
						}
						pi.erj = erj;
						msg = JSON.toJSONString(erj);
					}
					else if (uj.ui == 26)// 查看成就列表
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|成就界面");
						ej = new AchieveResultJson();
						AchieveResultJson arj = (AchieveResultJson) ej;
						if (checkMode(20, pi))// 校验模块解锁
						{
							pi.player.updateAchieve();
							arj.ac = pi.player.getAchive();
							if (pi.player.getNewPlayerType() == 9)// 新手成就
							{
								pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);// 改变新手标识
							}
						}
						else
						{
							arj.errorCode = 56;
						}
						msg = JSON.toJSONString(arj);
					}
					else if (uj.ui == 27)// 迷宫付费重置
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|迷宫进入次数重置");
						ej = new MazeClearResultJson();
						MazeClearResultJson mcrj = (MazeClearResultJson) ej;
						int errorcode = 0;
						String[] maze = pi.player.getMaze().split(",");
						MazeData mData = MazeData.getMazeData(uj.type);
						VipData vData = VipData.getVipData(pi.player.getVipLevel());
						if (maze != null && maze.length > 0)
						{
							// 校验
							if (errorcode == 0)// 校验vip等级
							{
								if (vData != null)
								{
									if (vData.maze == 0)
									{
										errorcode = 70;
									}
								}
							}
							if (errorcode == 0)
							{
								for (int k = 0; k < maze.length; k++)// 校验重置次数
								{
									if (maze[k] != null && maze[k].length() > 0)
									{
										String[] temp = maze[k].split("-");
										if (temp[0] != null && temp[0].length() > 0)
										{
											if (StringUtil.getInt(temp[0]) == uj.type)
											{
												if (temp[3] != null && temp[3].length() > 0)
												{
													if (StringUtil.getInt(temp[3]) >= vData.maze)
													{
														errorcode = 60;
														break;
													}
													else if (StringUtil.getInt(temp[2]) < mData.freeentry)
													{
														errorcode = 61;
														break;
													}
												}
											}
										}
									}
								}
								if (errorcode == 0)// 校验水晶数
								{
									int clear = 0;
									for (int k = 0; k < maze.length; k++)
									{
										if (maze[k] != null && maze[k].length() > 0)
										{
											String[] temp = maze[k].split("-");
											if (temp[0] != null && temp[0].length() > 0)
											{
												if (uj.type == StringUtil.getInt(temp[0]))
												{
													clear = StringUtil.getInt(temp[3]) + 1;
													break;
												}
											}
										}
									}
									MazeCostData mcData1 = MazeCostData.getMazeCostData(clear);
									if (pi.player.getTotalCrystal() < mcData1.cost)
									{
										errorcode = 19;
									}
								}
							}
							// 功能
							if (errorcode == 0)
							{
								int num = 0;
								// 修改数据库
								for (int k = 0; k < maze.length; k++)
								{
									if (maze[k] != null && maze[k].length() > 0)
									{
										String[] temp = maze[k].split("-");
										if (temp[0] != null && temp[0].length() > 0)
										{
											if (uj.type == StringUtil.getInt(temp[0]))
											{
												temp[3] = StringUtil.getInt(temp[3]) + 1 + "";
												num = StringUtil.getInt(temp[3]);
												maze[k] = temp[0] + "-" + temp[1] + "-" + "0" + "-" + temp[3];
												mcrj.maze = maze[k];
												break;
											}
										}
									}
								}
								String str = "";
								for (int k = 0; k < maze.length; k++)
								{
									if (maze[k] != null && maze[k].length() > 0)
									{
										if (str == null || str.length() == 0)
										{
											str = maze[k];
										}
										else
										{
											str = str + "," + maze[k];
										}
									}
								}
								// 扣除水晶
								MazeCostData mcData = MazeCostData.getMazeCostData(num);
								if (mcData != null)
								{
									int[] crystals = pi.player.removeCrystal(mcData.cost);
									MazeData md = MazeData.getMazeData(mcData.mazeid);
									if (md != null)
									{
										MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买迷宫次数|" + md.name, crystals));
									}
								}
								else
								{
									errorlogger.error("mazecostdata is null");
								}
								pi.player.setMaze(str);
								mcrj.crystal = pi.player.getTotalCrystal();
							}
							else
							{
								mcrj.errorCode = errorcode;
							}
						}
						msg = JSON.toJSONString(mcrj);
					}
					else if (uj.ui == 28)// 抽卡
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|抽卡界面");
						ej = new LotResultJson();
						LotResultJson lrj = (LotResultJson) ej;
						lrj.d = pi.getTargetItemsNum(Constant.DiaosItemId);
						lrj.f = pi.player.getFriend();
						lrj.n = pi.player.getLotPort();
						lrj.c = pi.player.getTotalCrystal();
						if (pi.player.getLastFreeDrawTime() == 0)
						{
							lrj.t = 0;
						}
						else
						{
							int time = (int) (24 * 3600 * 1000 - (System.currentTimeMillis() - pi.player.getLastFreeDrawTime())) / 1000;
							if (time < 0)
							{
								time = 0;
							}
							lrj.t = time;
						}
						lrj.list = null;
						msg = JSON.toJSONString(lrj);
					}
					else if (uj.ui == 29)// 签到
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|签到界面");
						ej = new SignResultJson();
						SignResultJson srj = (SignResultJson) ej;
						srj.times = pi.player.getSignTimes();
						srj.mark = Statics.isTodaySign(pi) ? 1 : 0;
						srj.days = StringUtil.getDayNumsOfThisMonth();
						msg = JSON.toJSONString(srj);
					}
					else if (uj.ui == 30)// 突破-可以突破的卡
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|可以突破界面");
						ej = new BreakResultJson();
						BreakResultJson brj = (BreakResultJson) ej;
						if (checkMode(17, pi))
						{
							List<PackElement> pes = new ArrayList<PackElement>();
							pes = getElements(pi, 1);
							pes = breakOrder(pes, pi.player);
							if (pi.player.getNewPlayerType() == 20)// 如果新手突破
							{
								for (int k = 0; k < pes.size(); k++)
								{
									if (k != 0)
									{
										if (pes.get(k).getDataId() == 14001)
										{
											PackElement pe = pes.get(0);
											pes.set(0, pes.get(k));
											pes.set(k, pe);
											break;
										}
									}
									else if (k == 0 && pes.get(k).getDataId() == 14001)
									{
										break;
									}
								}
							}
							brj.pes = pes;
						}
						else
						{
							brj.errorCode = 56;
						}
						msg = JSON.toJSONString(brj);
					}
					else if (uj.ui == 31)// 突破-可以被突破的卡
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|可以被突破界面");
						ej = new BreakResultJson();
						BreakResultJson brj = (BreakResultJson) ej;
						Card card = pi.getCards().get(uj.i);
						CardData cardData = CardData.getData(card.getCardId());
						List<PackElement> pes = new ArrayList<PackElement>();
						List<PackElement> list = new ArrayList<PackElement>();
						pes = getElements(pi, 1);
						for (int i = pes.size() - 1; i >= 0; i--)
						{
							if (pes.get(i) != null)
							{
								CardData cData = CardData.getData(pes.get(i).getDataId());
								if (pes.get(i).getUse() == 0)
								{// 不在卡组中的卡
									if (pes.get(i).i != uj.i && cData != null && cData.race == 6 && cData.star == cardData.star)// 同星级神格卡
									{
										list.add(pes.get(i));
									}
									else if (pes.get(i).i != uj.i && cData != null && cData.id == cardData.id)// 同卡
									{
										list.add(pes.get(i));
									}
								}
							}
						}
						brj.pes = list;
						msg = JSON.toJSONString(brj);
					}
					else if (uj.ui == 32)// 客户端请求是否有解锁模块以及模块id
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|解锁模块界面");
						ej = new LockResultJson();
						LockResultJson lrj = (LockResultJson) ej;
						if (pi.player.getNewPlayerType() == 10)// 强化
						{
							if (checkMode(3, pi))
							{
								lrj.type = 1;
								lrj.modeId = 3;
							}
						}
						else if (pi.player.getNewPlayerType() == 8)// 成就
						{
							if (checkMode(20, pi))
							{
								lrj.type = 1;
								lrj.modeId = 20;
							}
						}
						else if (pi.player.getNewPlayerType() == 15)// 请求合成解锁并发放铁矿石10101*3
						{
							if (checkMode(4, pi))
							{
								lrj.type = 1;
								lrj.modeId = 4;
								pi.addItem(10101, 3);
								pi.addItem(10301, 1);
								pi.player.addGold(500);
								pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
							}
						}
						else if (pi.player.getNewPlayerType() == 23)// 活动副本(异世界)
						{
							if (checkMode(8, pi))
							{
								lrj.type = 1;
								lrj.modeId = 8;
							}
						}
						else if (pi.player.getNewPlayerType() == 17)// 扭曲空间
						{
							if (checkMode(11, pi))
							{
								lrj.type = 1;
								lrj.modeId = 11;
							}
						}
						else if (pi.player.getNewPlayerType() == 21)// 竞技场
						{
							if (checkMode(9, pi))
							{
								lrj.type = 1;
								lrj.modeId = 9;
							}
						}
						else if (pi.player.getNewPlayerType() == 22)// 符文
						{
							if (checkMode(5, pi))
							{
								lrj.type = 1;
								lrj.modeId = 5;
							}
						}
						else if (pi.player.getNewPlayerType() == 19)// 发放突破物品和金币(孙悟空,20000)
						{
							if (checkMode(17, pi))
							{
								lrj.type = 1;
								lrj.modeId = 17;
								// pi.addCard(14001, 1);
								// pi.addCard(14001, 1);
								// List<Integer> cardIds = new
								// ArrayList<Integer>();
								// cardIds.add(14001);
								// pi.getNewUnitSkill(cardIds);
								// pi.player.addGold(20000);
								pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
							}
						}
						else if (pi.player.getNewPlayerType() == 24)// 灵界
						{
							if (checkMode(10, pi))
							{
								lrj.type = 1;
								lrj.modeId = 10;
							}
						}
						msg = JSON.toJSONString(lrj);
					}
					else if (uj.ui == 33)// 邮箱
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|邮箱界面");
						ej = new MailUIResultJson();
						MailUIResultJson mrj = (MailUIResultJson) ej;
						List<Mail> mails = pi.getMails();
						List<MailUIResultElement> ms = new ArrayList<MailUIResultElement>();
						int unreadNum = 0;
						for (Mail mail : mails)
						{
							if (mail.getState() == 2)
							{
								unreadNum++;
							}
							ms.add(mail.getJson());
						}
						mrj.ms = ms;
						mrj.ur = unreadNum;
						
						msg = JSON.toJSONString(mrj);
					}
					else if (uj.ui == 34)// 头像设置区域
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|头像设置界面");
						ej = new HeadSetResultJson();
						HeadSetResultJson hrj = (HeadSetResultJson) ej;
						if (pi.player.getChangeNameTime() <= 9)
						{
							ChangeNameCostData cncd = ChangeNameCostData.getChangeNameCostData(pi.player.getChangeNameTime() + 1);
							if (cncd != null)
							{
								hrj.crystal = cncd.cost;
							}
						}
						else
						{
							ChangeNameCostData cncd = ChangeNameCostData.getChangeNameCostData(10);
							hrj.crystal = cncd.cost;
						}
						
						hrj.f = pi.player.getFriend();
						int iconId = IconUnlockData.getIconUnlockDataByUnlock(pi.player.getBattlePower(), pi.player.getHeadIconUnlock());
						if (iconId != pi.player.getHeadIconUnlock() && iconId > pi.player.getHeadIconUnlock())
						{
							pi.player.setHeadIconUnlock(iconId);
						}
						else
						{
							iconId = pi.player.getHeadIconUnlock();
						}
						hrj.iconId = iconId;
						hrj.playerid = 0x0f000000 | pi.player.getId();
						msg = JSON.toJSONString(hrj);
					}
					else if (uj.ui == 35)// 修改玩家头像
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|头像修改");
						ej = new HeadSetResultJson();
						HeadSetResultJson hrj = (HeadSetResultJson) ej;
						String s = uj.str;
						
						if (s == null || s.length() == 0)
						{
							hrj.errorCode = 75;
						}
						else
						{
							if (!s.equals(pi.player.getHead()))
							{
								pi.player.setHead(s);
							}
						}
						msg = JSON.toJSONString(hrj);
					}
					else if (uj.ui == 36)// 修改军团名称
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|军团名称设置");
						ej = new HeadSetResultJson();
						HeadSetResultJson hrj = (HeadSetResultJson) ej;
						String s = uj.str;
						int errorcode = 0;
						// 校验水晶
						ChangeNameCostData cncd = null;
						if (pi.player.getChangeNameTime() <= 9)
						{
							cncd = ChangeNameCostData.getChangeNameCostData(pi.player.getChangeNameTime() + 1);
						}
						else
						{
							cncd = ChangeNameCostData.getChangeNameCostData(10);
						}
						if (pi.player.getTotalCrystal() < cncd.cost)
						{
							errorcode = 19;
						}
						if (errorcode == 0)
						{
							if (s == null || s.length() == 0)
							{
								errorcode = 75;
							}
							else if (s.length() > 6)
							{
								errorcode = 87;
							}
						}
						// 校验名字
						if (errorcode == 0)
						{
							if (StringUtil.match(s))
							{
								errorcode = 85;
							}
						}
						if (errorcode == 0)
						{
							for (int k = 0; k < ss.length; k++)
							{
								if (s.contains(ss[k]))
								{
									errorcode = 85;
									break;
								}
							}
						}
						if (errorcode == 0)
						{
							List<SensitivewordsData> senstiveList = SensitivewordsData.getSensitivewordsDatas();
							for (SensitivewordsData sData : senstiveList)
							{
								boolean sensive = true;
								String newnameString = s;
								if (sData.chinese != null && sData.chinese.length() > 0)
								{
									for (int i = 0; i < sData.chinese.length(); i++)
									{
										if (!newnameString.contains(sData.chinese.subSequence(i, i + 1)))
										{
											sensive = false;
											break;
										}
										else
										{
											newnameString = newnameString.substring(newnameString.indexOf(sData.chinese.substring(i, i + 1)), newnameString.length());
										}
									}
									if (sensive)
									{
										errorcode = 85;
										break;
									}
								}
							}
						}
						if (errorcode == 0)
						{
							if (s.length() == 0)
							{
								errorcode = 85;
							}
						}
						if (errorcode == 0)
						{
							if (!s.equals(pi.player.getName()))
							{
								playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|军团名称改为:" + s);
								pi.player.setName(s);
							}
							int[] crystals = pi.player.removeCrystal(cncd.cost);
							MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "修改名字", crystals));
							pi.player.setChangeNameTime(pi.player.getChangeNameTime() + 1);
							hrj.crystals = pi.player.getTotalCrystal();
							hrj.name = pi.player.getName();
							ChangeNameCostData changeNameCostData = null;
							if (pi.player.getChangeNameTime() <= 9)
							{
								changeNameCostData = ChangeNameCostData.getChangeNameCostData(pi.player.getChangeNameTime());
							}
							else
							{
								changeNameCostData = ChangeNameCostData.getChangeNameCostData(10);
							}
							hrj.crystal = changeNameCostData.cost;
							// 新手引导改名
							if (pi.player.getNewPlayerType() == 11)
							{
								pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
							}
						}
						else
						{
							hrj.errorCode = errorcode;
						}
						msg = JSON.toJSONString(hrj);
					}
					else if (uj.ui == 37)// 掉落指引
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|掉落指引");
						int itemId = uj.type;
						List<String> ds = getDropGuides(pi, itemId);
						ej = new DropGuideResultJson();
						DropGuideResultJson drj = (DropGuideResultJson) ej;
						drj.id = itemId;
						drj.ds = ds;
						msg = JSON.toJSONString(drj);
					}
					
					else if (uj.ui == 38)// 卡牌详细信息，是否有新装备提醒或者提升
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|卡牌详细信息-新提醒");
						ej = new CardInfoResultJson();
						CardInfoResultJson cirj = (CardInfoResultJson) ej;
						cirj.pd = pi.player.getDiamond();
						int position = uj.i;// 卡牌的位置信息
						Card card = pi.getCardGroup().getCard(position);
						if (card == null)
						{
							card = pi.getCards().get(uj.i);
						}
						if (card != null)
						{
							CardGroup cardGroup = pi.getCardGroup();
							List<PassiveSkill> ps = cardGroup.getPassiveSkills(position);
							List<PackElement> pes1 = getElements(pi, 3);
							List<PackElement> pes2 = getElements(pi, 4);
							List<PackElement> pskills = new ArrayList<PackElement>();
							List<PackElement> equips = new ArrayList<PackElement>();
							// 判断被动技能是否有新提醒
							if (pes1 != null && pes1.size() > 0)
							{
								for (PackElement pe : pes1)
								{
									if (pe != null)
									{
										if (pe.getUse() == 0)
										{
											pskills.add(pe);
										}
									}
								}
							}
							if (ps != null)
							{
								for (int i = 0; i < 3; i++)
								{
									PassiveSkill p = ps.get(i);
									if (p != null)
									{
										if (pskills != null && pskills.size() > 0)
										{
											if (i == 0)
											{
												cirj.ps1 = 0;
											}
											if (i == 1)
											{
												cirj.ps2 = 0;
											}
											if (i == 2)
											{
												cirj.ps3 = 0;
											}
										}
									}
									else
									{
										if (pskills != null && pskills.size() > 0)
										{
											boolean is = true;
											int num = 0;
											for (int j = 0; j < 3; j++)
											{
												PassiveSkill p2 = ps.get(j);
												if (p2 != null)
												{
													PassiveSkillData psd1 = PassiveSkillData.getData(p2.getPassiveSkillId());
													for (int k = 0; k < pskills.size(); k++)
													{
														PassiveSkillData psd2 = PassiveSkillData.getData(pskills.get(k).dataId);
														if (psd1.type == psd2.type)
														{
															num++;
															if (pskills.size() <= num)
															{
																is = false;
																break;
															}
														}
													}
												}
											}
											if (i == 0 && UiController.checkMode(34, pi) && is)
											{
												cirj.ps1 = 1;
											}
											if (i == 1 && UiController.checkMode(35, pi) && is)
											{
												cirj.ps2 = 1;
											}
											if (i == 2 && UiController.checkMode(36, pi) && is)
											{
												cirj.ps3 = 1;
											}
										}
									}
								}
							}
							else
							{
								for (int m = 0; m < 3; m++)
								{
									if (m == 0 && UiController.checkMode(34, pi) && pskills.size() > 0)
									{
										cirj.ps1 = 1;
									}
									if (m == 1 && UiController.checkMode(35, pi) && pskills.size() > 0)
									{
										cirj.ps2 = 1;
									}
									if (m == 2 && UiController.checkMode(36, pi) && pskills.size() > 0)
									{
										cirj.ps3 = 1;
									}
								}
							}
							
							/** 判断装备是否有新提醒或者提升 **/
							List<Equip> eList = cardGroup.getEquips(position);
							if (pes2 != null && pes2.size() > 0)
							{
								for (PackElement pe : pes2)
								{
									if (pe != null)
									{
										if (pe.getUse() == 0)
										{
											equips.add(pe);
										}
									}
								}
							}
							
							if (eList != null && eList.size() > 0)//
							{
								HashMap<Integer, Equip> equipMap = new HashMap<Integer, Equip>();
								
								for (int j = 0; j < eList.size(); j++)
								{
									if (eList.get(j) != null)
									{
										EquipData equipData = EquipData.getData(eList.get(j).getEquipId());
										if (equipData != null)
										{
											if (!equipMap.containsKey(equipData.type))
											{
												equipMap.put(equipData.type, eList.get(j));
											}
										}
									}
								}
								cirj.equip1 = checkEquipStar(equips, eList, equipMap, 1);
								cirj.equip2 = checkEquipStar(equips, eList, equipMap, 2);
								cirj.equip3 = checkEquipStar(equips, eList, equipMap, 3);
							}
							else
							{
								if (equips != null && equips.size() > 0)
								{
									for (PackElement pe : equips)
									{
										if (pe != null)
										{
											EquipData eData = EquipData.getData(pe.getDataId());
											if (eData != null)
											{
												if (cirj.equip1 == 0 || cirj.equip2 == 0 || cirj.equip3 == 0)
												{
													if (eData.type == 1)
													{
														if (cirj.equip1 == 0)
														{
															cirj.equip1 = 1;
														}
													}
													else if (eData.type == 2)
													{
														if (cirj.equip2 == 0)
														{
															cirj.equip2 = 1;
														}
													}
													else if (eData.type == 3)
													{
														if (cirj.equip3 == 0)
														{
															cirj.equip3 = 1;
														}
													}
												}
												
											}
										}
									}
								}
							}
							// 突破需要金罡心
							CardData cd = CardData.getData(card.getCardId());
							EvolutionData evD = null;
							if (card.getBreakNum() < 7)
							{
								evD = EvolutionData.getData(cd.star, card.getBreakNum() + 1);
								cirj.diamond = evD.moneypercard;
								// 突破需要卡牌
								cirj.cardN = evD.cards;
							}
							else
							{
								cirj.diamond = -1;
								cirj.cardN = -1;
							}
							// 玩家拥有此卡牌
							int pCardN = 0;
							// 玩家拥有万能突破卡牌数量
							int multCard = 0;
							List<PackElement> pes = new ArrayList<PackElement>();
							List<PackElement> list = new ArrayList<PackElement>();
							pes = getElements(pi, 1);
							for (int i = pes.size() - 1; i >= 0; i--)
							{
								if (pes.get(i) != null)
								{
									CardData cData = CardData.getData(pes.get(i).getDataId());
									if (pes.get(i).getUse() == 0)
									{// 不在卡组中的卡
										if (pes.get(i).i != uj.i && cData != null && cData.race == 6 && cData.star == cd.star)// 同星级神格卡
										{
											multCard++;
											list.add(pes.get(i));
										}
										else if (pes.get(i).i != uj.i && cData != null && cData.id == cd.id)// 同卡
										{
											pCardN++;
											list.add(pes.get(i));
										}
									}
								}
							}
							cirj.pCardN = pCardN;
							cirj.multCard = multCard;
							cirj.pes = list;
						}
						msg = JSON.toJSONString(cirj);
					}
					else if (uj.ui == 39)// vip特权界面
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|充值界面或者特权界面");
						ej = new RechargeUiResultJson();
						RechargeUiResultJson rurj = (RechargeUiResultJson) ej;
						VipData vd = VipData.getVipData(pi.player.getVipLevel());
						VipData vd2 = VipData.getVipData(pi.player.getVipLevel() + 1);
						if (vd2 == null)
						{
							if (Constant.OS_ANDROID.equals(pi.player.getPlatform()))
							{
								rurj.vipCost = vd.costandroid;
								rurj.sCost = vd.costandroid;
							}
							else if (Constant.OS_IOS.equals(pi.player.getPlatform()))
							{
								rurj.vipCost = vd.costios;
								rurj.sCost = vd.costios;
							}
							else
							{
								rurj.vipCost = vd.costios;
								rurj.sCost = vd.costios;
							}
						}
						else
						{
							if (Constant.OS_ANDROID.equals(pi.player.getPlatform()))
							{
								rurj.vipCost = pi.player.getVipCost();// -vd.costandroid;
								rurj.sCost = vd2.costandroid;
							}
							else if (Constant.OS_IOS.equals(pi.player.getPlatform()))
							{
								rurj.vipCost = pi.player.getVipCost();// -vd.costios;
								rurj.sCost = vd2.costios;
							}
							else
							{
								rurj.vipCost = pi.player.getVipCost() - vd.costios;
								rurj.sCost = vd2.costios;
							}
						}
						// 10倍显示
						rurj.vipCost *= 10;
						rurj.sCost *= 10;
						rurj.vipLv = pi.player.getVipLevel();
						rurj.vipMonthType = pi.player.getVipMonthType();
						rurj.vipMonthDay = pi.player.getVipMonthDay();
						
						List<String> ids = RechargeData.getIds();
						String rechargeInfo = pi.player.getRechargeInfo();
						if (rechargeInfo != null && rechargeInfo.length() > 0)
						{
							String[] temp = rechargeInfo.split("-");
							HashMap<String, String> map = new HashMap<String, String>();
							for (int j = 0; j < temp.length; j++)
							{
								if (temp[j] != null && temp[j].length() > 0)
								{
									map.put(temp[j], temp[j]);
								}
							}
							for (int i = ids.size() - 1; i >= 0; i--)
							{
								if (map.containsKey(ids.get(i)))
								{
									ids.remove(i);
								}
							}
						}
						rurj.ids = ids;
						String buyVipGift = pi.player.getBuyVipGift();
						List<String> giftIds = new ArrayList<String>();
						if (buyVipGift != null && buyVipGift.length() > 0)
						{
							String[] temp = buyVipGift.split("-");
							HashMap<Integer, String> map = new HashMap<Integer, String>();
							for (int i = 0; i < temp.length; i++)
							{
								if (temp[i] != null && temp[i].length() > 0)
								{
									if (!map.containsKey(StringUtil.getInt(temp[i])))
									{
										map.put(StringUtil.getInt(temp[i]), temp[i]);
									}
								}
							}
							for (int i = 1; i <= pi.player.getVipLevel(); i++)
							{
								if (!map.containsKey(i))
								{
									giftIds.add(i + "");
								}
							}
						}
						else
						{
							for (int i = 1; i <= pi.player.getVipLevel(); i++)
							{
								giftIds.add(i + "");
							}
						}
						rurj.giftIds = giftIds;
						msg = JSONObject.toJSONString(rurj);
					}
					else if (uj.ui == 41)// 在线礼包
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|领取在线礼包");
						ej = new GiftRewardResultJson();
						GiftRewardResultJson grj = (GiftRewardResultJson) ej;
						int errorcode = 0;
						int giftId = uj.i;
						long lineGiftTime = StringUtil.getTimeStamp(pi.player.getLineGiftTime());
						GiftData gd = GiftData.getGiftData(giftId);
						int linetime = 0;
						if (errorcode == 0)
						{
							if (!checkMode(25, pi))
							{
								errorcode = 56;
							}
						}
						if (errorcode == 0)
						{
							
							if (giftId > 0)
							{
								// 校验阶段
								String gift = pi.player.getLineGift();
								if (gift != null && gift.length() > 0)
								{
									String[] temp = gift.split("-");
									if (StringUtil.getInt(temp[0]) == giftId && StringUtil.getInt(temp[1]) == 0)
									{
										linetime = 0;
									}
									else
									{
										int t = (int) (System.currentTimeMillis() - lineGiftTime) / 1000;
										if (t < gd.time * 60)
										{
											linetime = gd.time * 60 - t;
										}
									}
								}
								else
								{
									GiftData giftData = GiftData.getGiftData(1);
									int t = (int) (System.currentTimeMillis() - lineGiftTime) / 1000;
									if (t < giftData.time * 60)
									{
										linetime = giftData.time * 60 - t;
									}
								}
							}
							// 功能阶段
							if (linetime == 0)
							{
								List<Integer> cardIds = new ArrayList<Integer>();
								// 发放在线礼包奖励
								for (int i = 0; i < gd.reward.size(); i++)
								{
									String s = gd.reward.get(i);
									if (s != null && s.length() > 0)
									{
										String[] temp = s.split("-");
										if (StringUtil.getInt(temp[0]) == 3)
										{
											String[] stri = temp[1].split(",");
											cardIds.add(StringUtil.getInt(stri[0]));
										}
										Statics.getReward(StringUtil.getInt(temp[0]), temp[1], pi);
									}
								}
								if (cardIds.size() > 0)
								{
									pi.getNewUnitSkill(cardIds);
								}
								pi.player.setLineGift(giftId + "-" + 1);
								// 下一个礼包
								GiftData gdNext = GiftData.getGiftData(giftId + 1);
								if (giftId + 1 > GiftData.getDataList().size())
								{
									grj.errorCode = 99;
									grj.nextGift = 0;
									grj.nextOnline = 0;
								}
								else
								{
									if (gdNext != null)
									{
										grj.nextGift = gdNext.id;
										grj.nextOnline = gdNext.time * 60;
									}
									else
									{
										errorlogger.info("giftdata is null");
									}
								}
								pi.player.setLineGiftTime(StringUtil.getDateTime(System.currentTimeMillis()));
							}
							else
							{
								grj.linetime = linetime;
							}
						}
						else
						{
							grj.errorCode = errorcode;
						}
						
						msg = JSONObject.toJSONString(grj);
					}
					else if (uj.ui == 42)// ko兑换界面1
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|ko兑换界面1");
						ej = new KOExchangeResultJson();
						KOExchangeResultJson krj = (KOExchangeResultJson) ej;
						krj.point = pi.player.getKopoint();
						
						List<KoAwardData> kds = new ArrayList<KoAwardData>();
						kds = KoAwardData.getAwardDatas();
						String exchange = pi.player.getKoExchange();
						String[] temp = exchange.split(",");
						HashMap<Integer, String> map = new HashMap<Integer, String>();
						for (int j = 0; j < temp.length; j++)
						{
							if (temp[j] != null && temp[j].length() > 0)
							{
								map.put(StringUtil.getInt(temp[j]), temp[j]);
							}
						}
						List<KOawardElement> list = new ArrayList<KOawardElement>();
						if (kds != null && kds.size() > 0)
						{
							for (int k = 0; k < kds.size(); k++)
							{
								KOawardElement ke = new KOawardElement();
								if (kds.get(k) != null)
								{
									ke.setId(kds.get(k).id);
									ke.setReward(kds.get(k).reward1);
									ke.setRewardtype(kds.get(k).rewardtyp1);
									if (map.containsKey(kds.get(k).id))
									{
										ke.setState(1);
									}
									else
									{
										ke.setState(0);
									}
									list.add(ke);
								}
								else
								{
									errorlogger.info("koaward is null");
								}
							}
							krj.awards = list;
						}
						else
						{
							krj.errorCode = 21;
							errorlogger.info("koawarddatas is null");
						}
						msg = JSONObject.toJSONString(krj);
					}
					else if (uj.ui == 43)// ko兑换界面2
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|ko兑换界面2");
						ej = new KOExchangeResultJson();
						KOExchangeResultJson krj = (KOExchangeResultJson) ej;
						int numName = uj.i;
						List<KoAwardData> kds = new ArrayList<KoAwardData>();
						kds = KoAwardData.getAwardDatasByName(numName);
						String exchange = pi.player.getKoExchange();
						String[] temp = exchange.split(",");
						HashMap<Integer, String> map = new HashMap<Integer, String>();
						for (int j = 0; j < temp.length; j++)
						{
							if (temp[j] != null && temp[j].length() > 0)
							{
								map.put(StringUtil.getInt(temp[j]), temp[j]);
							}
						}
						List<KOawardElement> list = new ArrayList<KOawardElement>();
						if (kds != null && kds.size() > 0)
						{
							for (int k = 0; k < kds.size(); k++)
							{
								KOawardElement ke = new KOawardElement();
								if (kds.get(k) != null)
								{
									ke.setId(kds.get(k).id);
									ke.setReward(kds.get(k).reward1);
									ke.setRewardtype(kds.get(k).rewardtyp1);
									if (map.containsKey(kds.get(k).id))
									{
										ke.setState(1);
									}
									else
									{
										ke.setState(0);
									}
									list.add(ke);
								}
								else
								{
									errorlogger.info("koaward is null");
								}
							}
							krj.awards = list;
							krj.point = pi.player.getKopoint();
						}
						else
						{
							krj.errorCode = 21;
							errorlogger.info("koawarddatas is null");
						}
						msg = JSONObject.toJSONString(krj);
					}
					else if (uj.ui == 44)// ko兑换功能
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|ko兑换");
						ej = new KOExchangeResultJson();
						KOExchangeResultJson krj = (KOExchangeResultJson) ej;
						int koId = uj.i;
						KoAwardData kad = KoAwardData.getKoAwardData(koId);
						// 校验阶段
						if (kad != null)
						{
							if (pi.player.getKopoint() < kad.number)// 校验积分是否可以兑换
							{
								krj.errorCode = 20;
							}
							if (krj.errorCode == 0)// 校验是否兑换过
							{
								String exchange = pi.player.getKoExchange();
								if (exchange != null && exchange.length() > 0)
								{
									HashMap<Integer, String> map = new HashMap<Integer, String>();
									String[] temp = exchange.split(",");
									for (int i = 0; i < temp.length; i++)
									{
										if (temp[i] != null && temp[i].length() > 0)
										{
											if (!map.containsKey(StringUtil.getInt(temp[i])))
											{
												map.put(StringUtil.getInt(temp[i]), temp[i]);
											}
										}
									}
									if (map.containsKey(koId))
									{
										krj.errorCode = 101;
									}
								}
							}
							if (krj.errorCode == 0)
							{
								List<Integer> cardIds = new ArrayList<Integer>();
								// 发放物品
								if (kad.rewardtyp1 == 3)
								{
									String[] temp = kad.reward1.split(",");
									cardIds.add(StringUtil.getInt(temp[0]));
								}
								Statics.getReward(kad.rewardtyp1, kad.reward1, pi);
								krj.point = pi.player.getKopoint();
								if (cardIds.size() > 0)
								{
									pi.getNewUnitSkill(cardIds);
								}
								// 修改数据库中兑换信息
								String str = pi.player.getKoExchange();
								if (str != null && str.length() > 0)
								{
									pi.player.setKoExchange(pi.player.getKoExchange() + "," + kad.id);
								}
								else
								{
									pi.player.setKoExchange(kad.id + "");
								}
								// 新手ko兑换
								if (pi.player.getNewPlayerType() == 6)
								{
									pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
								}
							}
							msg = JSONObject.toJSONString(krj);
						}
						else
						{
							errorlogger.info("koawarddata is null");
						}
					}
					else if (uj.ui == 45)// 商城界面//黑市界面
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|商城黑市荣誉商城界面");
						ej = new ShopUiResultJson();
						ShopUiResultJson surj = (ShopUiResultJson) ej;
						List<ShopElement> ses1 = new ArrayList<ShopElement>();
						List<ShopElement> ses2 = new ArrayList<ShopElement>();
						if (uj.type == 1)// 商城
						{
							String buyShopNum = pi.player.getBuyShopNum();
							String buyShopNums = pi.player.getBuyShopNums();
							List<ShopData> list1 = ShopData.getShopDatas1();
							List<ShopData> list2 = ShopData.getShopDatas2();
							HashMap<Integer, Integer> shopMap1 = new HashMap<Integer, Integer>();
							if (buyShopNum != null && buyShopNum.length() > 0)
							{
								String[] ss1 = buyShopNum.split("-");
								for (int i = 0; i < ss1.length; i++)
								{
									if (ss1[i] != null && ss1[i].length() > 0)
									{
										String[] temp1 = ss1[i].split("&");
										shopMap1.put(StringUtil.getInt(temp1[0]), StringUtil.getInt(temp1[1]));
									}
								}
							}
							for (int i = 0; i < list1.size(); i++)
							{
								ShopElement se = new ShopElement();
								se.setId(list1.get(i).id);
								if (shopMap1.containsKey(list1.get(i).id))
								{
									se.setNum(shopMap1.get(list1.get(i).id));
								}
								else
								{
									se.setNum(0);
								}
								ses1.add(se);
							}
							HashMap<Integer, Integer> shopMap2 = new HashMap<Integer, Integer>();
							if (buyShopNums != null && buyShopNums.length() > 0)
							{
								String[] ss2 = buyShopNums.split("-");
								for (int i = 0; i < ss2.length; i++)
								{
									if (ss2[i] != null && ss2[i].length() > 0)
									{
										String[] temp2 = ss2[i].split("&");
										shopMap2.put(StringUtil.getInt(temp2[0]), StringUtil.getInt(temp2[1]));
									}
								}
							}
							for (int i = 0; i < list2.size(); i++)
							{
								ShopElement se = new ShopElement();
								se.setId(list2.get(i).id);
								if (shopMap2.containsKey(list2.get(i).id))
								{
									se.setNum(shopMap2.get(list2.get(i).id));
								}
								else
								{
									se.setNum(0);
								}
								ses2.add(se);
							}
							surj.ses1 = ses1;
							surj.ses2 = ses2;
						}
						if (uj.type == 2 || uj.type == 3)// 黑市
						{
							boolean refresh = false;
							boolean refreshType = false;
							int refreshTime = 0;
							String date1 = "06:00:00";
							String date2 = "12:00:00";
							String date3 = "18:00:00";
							String date4 = "23:59:59";
							String today = StringUtil.getDate(System.currentTimeMillis());
							long t1 = StringUtil.getTimeStamp(today + " " + date1);// 06
							long t2 = StringUtil.getTimeStamp(today + " " + date2);// 12
							long t3 = StringUtil.getTimeStamp(today + " " + date3);// 18
							long t4 = StringUtil.getTimeStamp(today + " " + date4) - 24 * 60 * 60 * 1000;// 20
							long t5 = StringUtil.getTimeStamp(today + " " + date4);
							long time = System.currentTimeMillis();
							int hour = StringUtil.getInt(StringUtil.getHours(time));
							if (hour >= 0 && hour < 6)
							{
								refreshTime = (int) (t1 - time) / 1000;
							}
							else if (hour >= 6 && hour < 12)
							{
								refreshTime = (int) (t2 - time) / 1000;
							}
							else if (hour >= 12 && hour < 18)
							{
								refreshTime = (int) (t3 - time) / 1000;
							}
							else
							{
								refreshTime = (int) (t5 - time) / 1000;
							}
							if (uj.type == 3)
							{
								BlackRefreshData brData = null;// 如果刷新次数超过上限。则消耗以最后一次刷新为准。否则每次刷新的消耗根据次数读表
								if (pi.player.getBuyRefreshTimes() >= BlackRefreshData.getDataSize())
								{
									brData = BlackRefreshData.getBlackRefreshData(BlackRefreshData.getDataSize());
								}
								else
								{
									brData = BlackRefreshData.getBlackRefreshData(pi.player.getBuyRefreshTimes() + 1);
								}
								if (brData != null)
								{
									if (surj.errorCode == 0)// 校验金币
									{
										if (brData.costtype == 2)
										{
											if (pi.player.getGold() < brData.costnumber)
											{
												surj.errorCode = 19;
											}
										}
										else if (brData.costtype == 1)
										{
											if (pi.player.getTotalCrystal() < brData.costnumber)
											{
												surj.errorCode = 19;
											}
										}
									}
								}
								// 功能
								if (surj.errorCode == 0)// 立即刷新
								{
									refresh = true;
									if (brData.costtype == 2)
									{
										pi.player.removeGold(brData.costnumber);
									}
									else if (brData.costtype == 1)
									{
										int[] crystals = pi.player.removeCrystal(brData.costnumber);
										MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "黑市刷新", crystals));
									}
									pi.player.setBuyRefreshTimes(pi.player.getBuyRefreshTimes() + 1);
									refreshType = true;
								}
							}
							else
							// 黑市界面
							{
								long blackMarketRefreshTime = StringUtil.getTimeStamp(pi.player.getBlackMarketRefreshTime());
								if (blackMarketRefreshTime == 0)// 当天第一次打开界面时间为6*60*60
								{
									refresh = true;
									if (hour >= 0 && hour < 6)
									{
										pi.player.setBlackMarketRefreshTime(StringUtil.getDateTime(t4));
									}
									else if (hour >= 6 && hour < 12)
									{
										pi.player.setBlackMarketRefreshTime(StringUtil.getDateTime(t1));
									}
									else if (hour >= 12 && hour < 18)
									{
										pi.player.setBlackMarketRefreshTime(StringUtil.getDateTime(t2));
									}
									else
									{
										pi.player.setBlackMarketRefreshTime(StringUtil.getDateTime(t3));
									}
								}
								else
								{
									int ttt = (int) ((time - blackMarketRefreshTime) / 1000);// 非第一
									// 时间为6*60*60-已经等待的时间
									if (ttt >= 60 * 60 * 6)
									{
										refresh = true;
										if (hour >= 0 && hour < 6)
										{
											pi.player.setBlackMarketRefreshTime(StringUtil.getDateTime(t4));
										}
										else if (hour >= 6 && hour < 12)
										{
											pi.player.setBlackMarketRefreshTime(StringUtil.getDateTime(t1));
										}
										else if (hour >= 12 && hour < 18)
										{
											pi.player.setBlackMarketRefreshTime(StringUtil.getDateTime(t2));
										}
										else
										{
											pi.player.setBlackMarketRefreshTime(StringUtil.getDateTime(t3));
										}
									}
								}
							}
							if (surj.errorCode == 0)
							{
								if (refresh)// 重新刷新
								{
									List<BlackMarketData> list = BlackMarketData.getDataList();
									HashMap<Integer, Integer> blackMap = new HashMap<Integer, Integer>();
									int shopBoxNum = BlackShopBoxData.getShopBoxNum() + pi.player.getBuyShopBoxNum();
									int j = 0;
									pi.player.setBlackMarkets("");
									for (int i = 0; i < shopBoxNum + j; i++)
									{
										int randNum = Random.getNumber(0, Constant.TotalDropPro);
										int st = 0;
										int end = 0;
										boolean a = true;
										for (BlackMarketData bmData : list)
										{
											if (pi.player.getLevel() >= bmData.level)
											{
												if (refreshType)
												{
													if (pi.player.getBuyRefreshTimes() > 0 && pi.player.getBuyRefreshTimes() <= 5)
													{
														end = bmData.probability1 + st;
													}
													else if (pi.player.getBuyRefreshTimes() > 5 && pi.player.getBuyRefreshTimes() <= 10)
													{
														end = bmData.probability2 + st;
													}
													if (pi.player.getBuyRefreshTimes() > 10)
													{
														end = bmData.probability3 + st;
													}
												}
												else
												{
													end = bmData.probability1 + st;
												}
												if (randNum >= st && randNum <= end)
												{
													ShopElement se = new ShopElement();
													se.id = bmData.id;
													se.num = 0;
													if (!blackMap.containsKey(se.id))// 随机出不同的物品
													{
														ses1.add(se);
														if (pi.player.getBlackMarkets() != null && pi.player.getBlackMarkets().length() > 0)
														{
															pi.player.setBlackMarkets(pi.player.getBlackMarkets() + "&" + bmData.id + "-" + 0);
														}
														else
														{
															pi.player.setBlackMarkets(bmData.id + "-" + 0);
														}
														
														blackMap.put(se.id, se.id);
													}
													else
													{
														j++;
													}
													a = false;
													break;
												}
												else
												{
													st = end;
												}
											}
										}
										if (a)
										{
											ShopElement se = new ShopElement();
											se.id = list.get(0).id;
											se.num = 0;
											if (!blackMap.containsKey(se.id))// 随机出不同的物品
											{
												ses1.add(se);
												if (pi.player.getBlackMarkets() != null && pi.player.getBlackMarkets().length() > 0)
												{
													pi.player.setBlackMarkets(pi.player.getBlackMarkets() + "&" + list.get(0).id + "-" + 0);
												}
												else
												{
													pi.player.setBlackMarkets(list.get(0).id + "-" + 0);
												}
												blackMap.put(se.id, se.id);
											}
											else
											{
												j++;
											}
										}
									}
									if (refreshTime == 0)
									{
										refreshTime = 6 * 60 * 60;
									}
									surj.refreshTime = refreshTime;
								}
								else
								// 不重新刷新
								{
									String[] shopList = null;
									if (pi.player.getBlackMarkets() != null && pi.player.getBlackMarkets().length() > 0)
									{
										shopList = pi.player.getBlackMarkets().split("&");
										for (int i = 0; i < shopList.length; i++)
										{
											String str = shopList[i];
											if (str != null && str.length() > 0)
											{
												String[] temp = str.split("-");
												ShopElement se = new ShopElement();
												se.id = StringUtil.getInt(temp[0]);
												se.num = StringUtil.getInt(temp[1]);
												ses1.add(se);
											}
										}
										surj.refreshTime = refreshTime;// 不重新刷新时间为60*60*6-等待时间
									}
								}
								surj.ses1 = ses1;
								surj.refresh = pi.player.getBuyRefreshTimes();
							}
						}
						if (uj.type == 4 || uj.type == 5)
						{
							if (uj.type == 5)
							{
								BlackRefreshData brData = null;// 如果刷新次数超过上限。则消耗以最后一次刷新为准。否则每次刷新的消耗根据次数读表
								if (pi.player.getPvpShopRefreshTimes() >= BlackRefreshData.getDataSize())
								{
									brData = BlackRefreshData.getBlackRefreshData(BlackRefreshData.getDataSize());
								}
								else
								{
									brData = BlackRefreshData.getBlackRefreshData(pi.player.getPvpShopRefreshTimes() + 1);
								}
								if (null != brData)
								{
									if (surj.errorCode == 0)// 校验金币{
										if (brData.costtype == 2)
										{
											if (pi.player.getGold() < brData.costnumber)
											{
												surj.errorCode = 19;
											}
										}
										else if (brData.costtype == 1)
										{
											if (pi.player.getTotalCrystal() < brData.costnumber)
											{
												surj.errorCode = 19;
											}
										}
										else if (brData.costtype == 3)
										{
											if (pi.player.getPvpHonor() < brData.costnumber)
											{
												surj.errorCode = 125;
											}
										}
								}
								// 功能
								if (surj.errorCode == 0)
								{
									if (brData.costtype == 2)
									{
										pi.player.removeGold(brData.costnumber);
									}
									else if (brData.costtype == 1)
									{
										int[] crystals = pi.player.removeCrystal(brData.costnumber);
										MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "竞技场商城刷新", crystals));
									}
									else if (brData.costtype == 3)
									{
										pi.player.addPvpHonor(-brData.costnumber);
									}
									String pvpShopItems = null;
									if (pi.player.getPvpShopRefreshTimes() <= 5)
									{
										pvpShopItems = ShoppvpData.getRandomDatas(0);
									}
									else if (pi.player.getPvpShopRefreshTimes() <= 10)
									{
										pvpShopItems = ShoppvpData.getRandomDatas(1);
									}
									else
									{
										pvpShopItems = ShoppvpData.getRandomDatas(2);
									}
									pi.player.addPvpShopRefreshTimes(1);
									pi.player.setPvpShop(pvpShopItems);
									String[] ss = pvpShopItems.split(",");
									List<ShopElement> elments = new ArrayList<ShopElement>();
									for (int i = 0; i < ss.length; i++)
									{
										String[] s = ss[i].split("-");
										ShopElement se = new ShopElement();
										se.setId(Integer.valueOf(s[0]));
										se.setNum(Integer.valueOf(s[1]));
										elments.add(se);
									}
									surj.ses1 = elments;
								}
							}
							else
							{
								String pvpShopItems = pi.player.getPvpShop();
								if (null == pvpShopItems || "".equals(pvpShopItems))
								{
									pvpShopItems = Cache.getInstance().getPvpShopItems();
									if (null == pvpShopItems || "".equals(pvpShopItems))
									{
										pvpShopItems = ShoppvpData.getRandomDatas(0);
									}
								}
								else
								{
									if (Cache.getInstance().getLastPvpRefreshTime() != 0 && pi.player.getLastPvpShopRefreshTime() != Cache.getInstance().getLastPvpRefreshTime())
									{
										pvpShopItems = Cache.getInstance().getPvpShopItems();
										pi.player.setLastPvpShopRefreshTime(Cache.getInstance().getLastPvpRefreshTime());
									}
								}
								pi.player.setPvpShop(pvpShopItems);
								String[] ss = pvpShopItems.split(",");
								List<ShopElement> elments = new ArrayList<ShopElement>();
								for (int i = 0; i < ss.length; i++)
								{
									String[] s = ss[i].split("-");
									ShopElement se = new ShopElement();
									se.setId(Integer.valueOf(s[0]));
									se.setNum(Integer.valueOf(s[1]));
									elments.add(se);
								}
								surj.ses1 = elments;
							}
							surj.refresh = pi.player.getPvpShopRefreshTimes();
							surj.pvpHonor = pi.player.getPvpHonor();
						}
						msg = JSONObject.toJSONString(surj);
					}
					else if (uj.ui == 46)// 背包出售全选设定
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|背包出售全选设定");
						ej = new PackResultJson();
						PackResultJson prj = (PackResultJson) ej;
						String str = uj.str;
						// 校验
						if (str != null && str.length() > 0)
						{
							String[] temp = str.split("&");
							for (int i = 0; i < temp.length; i++)
							{
								if (StringUtil.getInt(temp[i]) < 0 || StringUtil.getInt(temp[i]) > 6)
								{
									prj.errorCode = 4;
								}
							}
						}
						else
						{
							prj.errorCode = 110;
						}
						// 功能阶段
						if (prj.errorCode == 0)
						{
							pi.player.setAllSelectId(str);
						}
						msg = JSONObject.toJSONString(prj);
					}
					else if (uj.ui == 47)// 七天登陆奖励
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|七天有礼");
						ej = new LoginDayResultJson();
						LoginDayResultJson ldrj = (LoginDayResultJson) ej;
						String loginDayAward = pi.player.getLoginDayAward();
						List<ShopElement> s = new ArrayList<ShopElement>();
						int day = 0;
						if (pi.player.getLoginDayNum() <= 7)
						{
							day = pi.player.getLoginDayNum();
						}
						else
						{
							day = 7;
						}
						if (loginDayAward != null && loginDayAward.length() > 0)
						{
							HashMap<Integer, String> map = new HashMap<Integer, String>();
							String[] temp = loginDayAward.split("&");
							for (int i = 0; i < temp.length; i++)
							{
								if (temp[i] != null && temp[i].length() > 0)
								{
									map.put(StringUtil.getInt(temp[i]), temp[i]);
								}
							}
							for (int j = 1; j <= day; j++)
							{
								ShopElement se = new ShopElement();
								se.setId(j);
								if (map.containsKey(j))
								{
									se.setNum(1);
								}
								else
								{
									se.setNum(0);
								}
								s.add(se);
							}
						}
						else
						{
							for (int j = 0; j <= day; j++)
							{
								ShopElement se = new ShopElement();
								se.setId(j);
								se.setNum(0);
								s.add(se);
							}
						}
						ldrj.s = s;
						msg = JSONObject.toJSONString(ldrj);
					}
					else if (uj.ui == 48)// 七天有礼领取奖励
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|七天有礼领取奖励");
						ej = new LoginDayResultJson();
						LoginDayResultJson ldrj = (LoginDayResultJson) ej;
						SevenDaysData sData = SevenDaysData.getSevenDaysData(uj.type);
						List<Activity> list = Cache.getInstance().getActivitys();
						for (Activity a : list)
						{
							if (a.getActivityId() == uj.i)
							{
								if (a.getSttime() != null && a.getSttime().length() > 0)
								{
									if (StringUtil.getTimeStamp(a.getSttime() + " 00:00:00") - System.currentTimeMillis() > 0)
									{
										ldrj.errorCode = 114;
									}
								}
								if (ldrj.errorCode == 0)
								{
									if (a.getEndtime() != null && a.getEndtime().length() > 0)
									{
										if (StringUtil.getTimeStamp(a.getEndtime() + " 00:00:00") - System.currentTimeMillis() <= 0)
										{
											ldrj.errorCode = 113;
										}
									}
								}
							}
						}
						if (ldrj.errorCode == 0)
						{
							if (sData != null)
							{
								if (pi.player.getLoginDayNum() < uj.type)
								{
									ldrj.errorCode = 95;
								}
								else
								{
									if (pi.player.getLoginDayAward() != null && pi.player.getLoginDayAward().length() > 0)
									{
										if (pi.player.getLoginDayAward().contains(uj.type + ""))
										{
											ldrj.errorCode = 94;
										}
										else
										{
											List<Integer> cardIds = new ArrayList<Integer>();
											for (int j = 0; j < sData.items.size(); j++)
											{
												String str = sData.items.get(j);
												String[] temp = str.split("-");
												if (StringUtil.getInt(temp[0]) == 3)
												{
													String[] stri = temp[1].split(",");
													cardIds.add(StringUtil.getInt(stri[0]));
												}
												Statics.getReward(StringUtil.getInt(temp[0]), temp[1], pi);
											}
											if (cardIds.size() > 0)
											{
												pi.getNewUnitSkill(cardIds);
											}
										}
									}
									else
									{
										List<Integer> cardIds = new ArrayList<Integer>();
										for (int j = 0; j < sData.items.size(); j++)
										{
											String str = sData.items.get(j);
											String[] temp = str.split("-");
											if (StringUtil.getInt(temp[0]) == 3)
											{
												String[] stri = temp[1].split(",");
												cardIds.add(StringUtil.getInt(stri[0]));
											}
											Statics.getReward(StringUtil.getInt(temp[0]), temp[1], pi);
										}
										if (cardIds.size() > 0)
										{
											pi.getNewUnitSkill(cardIds);
										}
									}
								}
								
								if (ldrj.errorCode == 0)
								{
									if (pi.player.getLoginDayAward() == null || pi.player.getLoginDayAward().length() == 0)
									{
										pi.player.setLoginDayAward(uj.type + "");
									}
									else
									{
										pi.player.setLoginDayAward(pi.player.getLoginDayAward() + "&" + uj.type);
									}
								}
							}
						}
						msg = JSONObject.toJSONString(ldrj);
					}
					else if (uj.ui == 49)// 聚宝盆界面
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|聚宝盆界面");
						ej = new CornucopiaResultJson();
						CornucopiaResultJson crj = (CornucopiaResultJson) ej;
						crj.id = pi.player.getCornucopia();
						crj.crestalPay = pi.player.getCornucopiaCrystal();
						List<Activity> list = Cache.getInstance().getActivitys();
						if (list != null && list.size() > 0)
						{
							for (Activity a : list)
							{
								if (a.getActivityId() == uj.type)
								{
									if (StringUtil.getTimeStamp(a.getSttime() + " 00:00:00") - System.currentTimeMillis() > 0)
									{
										crj.errorCode = 114;
									}
									else
									{
										if (a.getEndtime() != null && a.getEndtime().length() > 0)
										{
											int time = (int) (StringUtil.getTimeStamp(a.getEndtime() + " 24:00:00") - System.currentTimeMillis()) / 1000;
											if (time >= 0)
											{
												crj.time = time;
											}
											else
											{
												crj.time = 0;
											}
										}
										else
										{
											crj.time = -1;
										}
									}
									break;
								}
							}
						}
						msg = JSONObject.toJSONString(crj);
					}
					else if (uj.ui == 50)// 聚宝按钮，开始聚宝
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|开始聚宝");
						ej = new CornucopiaResultJson();
						CornucopiaResultJson crj = (CornucopiaResultJson) ej;
						crj.crestalPay = pi.player.getCornucopiaCrystal();
						// 校验活动时间是否结束
						List<Activity> list = Cache.getInstance().getActivitys();
						if (list != null && list.size() > 0)
						{
							for (Activity a : list)
							{
								if (a.getActivityId() == uj.type)
								{
									if (a.getEndtime() != null && a.getEndtime().length() > 0)
									{
										int time = (int) (StringUtil.getTimeStamp(a.getEndtime() + " 24:00:00") - System.currentTimeMillis()) / 1000;
										if (time <= 0)
										{
											crj.errorCode = 113;
										}
										else
										{
											crj.time = time;
										}
										if (StringUtil.getTimeStamp(a.getSttime() + " 00:00:00") - System.currentTimeMillis() > 0)
										{
											crj.errorCode = 114;
										}
									}
									break;
								}
							}
						}
						if (crj.errorCode == 0)
						{
							if (pi.player.getCornucopia() < 3)
							{
								
								CornucopiaData cData = CornucopiaData.getCornucopiaData(pi.player.getCornucopia() + 1);
								if (pi.player.getCornucopiaCrystal() < cData.cost)
								{
									crj.errorCode = 115;
								}
								else
								{
									if (pi.player.getTotalCrystal() < cData.cost)
									{
										crj.errorCode = 19;
									}
									else
									{
										int[] crystals = pi.player.removeCrystal(cData.cost);
										MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "聚宝盆|" + (pi.player.getCornucopia() + 1), crystals));
										pi.player.setCornucopia(pi.player.getCornucopia() + 1);
										crj.id = pi.player.getCornucopia();
									}
								}
							}
							else
							{
								crj.errorCode = 112;
							}
						}
						msg = JSONObject.toJSONString(crj);
					}
					
					else if (uj.ui == 51)// 请求是否有新解锁的合体技
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|请求新解锁的合体技");
						ej = new NewUnitskillResultJson();
						NewUnitskillResultJson urj = (NewUnitskillResultJson) ej;
						urj.unitskills = pi.newunitskills;
						pi.newunitskills = "";
						msg = JSONObject.toJSONString(urj);
					}
					else if (uj.ui == 52)// 请求等级奖励界面
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|请求等级奖励");
						ej = new LevelGiftResultJson();
						LevelGiftResultJson lgrj = (LevelGiftResultJson) ej;
						String levelgift = pi.player.getLevelgift();
						List<ShopElement> list = new ArrayList<ShopElement>();
						if (levelgift != null && levelgift.length() > 0)
						{
							for (int i = 1; i <= pi.player.getLevel(); i++)
							{
								LevelGiftData lgData = LevelGiftData.getLevelGiftData(i);
								if (lgData != null)
								{
									ShopElement se = new ShopElement();
									se.setId(lgData.id);
									if (levelgift.contains(lgData.id + ""))
									{
										se.setNum(1);
									}
									else
									{
										se.setNum(0);
									}
									list.add(se);
								}
							}
						}
						else
						{
							for (int i = 1; i <= pi.player.getLevel(); i++)
							{
								LevelGiftData lgData = LevelGiftData.getLevelGiftData(i);
								if (lgData != null)
								{
									ShopElement se = new ShopElement();
									se.setId(lgData.id);
									se.setNum(0);
									list.add(se);
								}
							}
						}
						lgrj.s = list;
						msg = JSON.toJSONString(lgrj);
					}
					else if (uj.ui == 53)// 领取等级奖励
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|领取等级奖励");
						ej = new LevelGiftResultJson();
						LevelGiftResultJson lgrj = (LevelGiftResultJson) ej;
						int id = uj.type;
						LevelGiftData lgData = LevelGiftData.getLevelGiftData2(id);
						int errorcode = 0;
						if (lgData != null)
						{
							// 校验
							if (pi.player.getLevelgift().contains(id + ""))
							{
								errorcode = 91;
							}
							if (errorcode == 0)
							{
								if (lgData.level > pi.player.getLevel())
								{
									errorcode = 57;
								}
							}
							// 功能阶段
							if (errorcode == 0)
							{
								List<Integer> cardIds = new ArrayList<Integer>();
								// 发放物品
								for (int i = 0; i < lgData.goods.size(); i++)
								{
									String str = lgData.goods.get(i);
									if (str != null && str.length() > 0)
									{
										String[] temp = str.split("-");
										if (StringUtil.getInt(temp[0]) == 3)
										{
											String[] s = temp[1].split(",");
											cardIds.add(StringUtil.getInt(s[0]));
										}
										Statics.getReward(StringUtil.getInt(temp[0]), temp[1], pi);
									}
								}
								if (cardIds.size() > 0)
								{
									pi.getNewUnitSkill(cardIds);
								}
								// 记录领取
								if (pi.player.getLevelgift() != null && pi.player.getLevelgift().length() > 0)
								{
									pi.player.setLevelgift(pi.player.getLevelgift() + "&" + id);
								}
								else
								{
									pi.player.setLevelgift(id + "");
								}
							}
							else
							{
								lgrj.errorCode = errorcode;
							}
							msg = JSON.toJSONString(lgrj);
						}
						else
						{
							errorlogger.info("lgdata is null");
						}
					}
					else if (uj.ui == 54)// 迷宫血瓶使用
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|使用血瓶");
						ej = new BloodResultJson();
						BloodResultJson brj = (BloodResultJson) ej;
						int itemID = uj.type;
						if (itemID == 130001)
						{
							if (pi.getTargetItemsNum(itemID) <= 0)
							{
								brj.errorCode = 120;
							}
							// 功能阶段
							if (brj.errorCode == 0)
							{
								// 修改玩家进入迷宫卡牌血量
								List<Maze> mazes = pi.mazes;
								if (mazes != null && mazes.size() > 0)
								{
									for (Maze maze : mazes)
									{
										if (maze.getMazeId() == uj.i)
										{
											String s = "";
											String[] blood = maze.getBloodInfo().split("-");
											String[] maxblood = maze.getMaxbloodInfo().split("-");
											for (int i = 0; i < blood.length; i++)
											{
												int num = StringUtil.getInt(maxblood[i]);
												if ("".equals(s))
												{
													s = s + num;
												}
												else
												{
													s = s + "-" + num;
												}
											}
											maze.setBloodInfo(s);
											pi.mazes.remove(maze);
											pi.mazes.add(maze);
										}
									}
								}
								pi.removeItem(itemID, 1);
							}
							msg = JSON.toJSONString(brj);
						}
						else
						{
							errorlogger.error("bloodId is error");
						}
					}
					else if (uj.ui == 55)// 刷新对手玩家--竞技场
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|刷新对手玩家");
						ej = new PkrankListResultJson();
						PkrankListResultJson prj = (PkrankListResultJson) ej;
						int rank = uj.type;
						if (rank > 0)
						{
							prj.pks = PkController.getPkRanks(rank);
							List<String> cardIds = new ArrayList<String>();
							for (int i = 0; i < prj.pks.size(); i++)
							{
								if (prj.pks.get(i) != null)
								{
									String[] temp = prj.pks.get(i).split("-");
									PlayerInfo pInfo = Cache.getInstance().getPlayerInfo(StringUtil.getInt(temp[0]));
									String card = null;
									Card[] cards = pInfo.getCardGroup().getCards();
									for (int m = 0; m < cards.length; m++)
									{
										if (cards[m] != null)
										{
											if (card != null && card.length() > 0)
											{
												card = card + "-" + cards[m].getCardId();
											}
											else
											{
												card = cards[m].getCardId() + "";
											}
										}
									}
									cardIds.add(card);
								}
								else
								{
									errorlogger.error("刷新。pk对象出错!!!");
								}
							}
							prj.cardIds = cardIds;
							msg = JSON.toJSONString(prj);
						}
						else
						{
							errorlogger.error("排名为负数，不能刷新对手");
						}
					}
					else if (uj.ui == 56)// 迷宫许愿
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|迷宫许愿");
						ej = new WishResultJson();
						WishResultJson wrj = (WishResultJson) ej;
						int itemId = uj.type;
						int mazeId = uj.show;
						int errorcode = 0;
						boolean drop = true;// 没有掉落过
						// 校验该碎片是否已经掉落过，可以许愿？
						String mazeBossDrop = pi.player.getMazeBossDrop();
						if (mazeBossDrop != null && mazeBossDrop.length() > 0)
						{
							String[] mazeDrop = mazeBossDrop.split("&");
							for (int i = 0; i < mazeDrop.length; i++)
							{
								if (mazeDrop != null && mazeDrop.length > 0)
								{
									String[] temp = mazeDrop[i].split("-");
									if (StringUtil.getInt(temp[0]) == mazeId)
									{
										for (int j = 1; j < temp.length; j++)
										{
											if (StringUtil.getInt(temp[j]) == itemId)
											{
												drop = false;
												break;
											}
										}
										break;
									}
								}
							}
							if (drop)
							{
								errorcode = 122;
							}
							
						}
						else
						{
							errorcode = 122;
						}
						// 功能阶段
						if (errorcode == 0)
						{
							String mazewish = pi.player.getMazeWish();
							if (mazewish != null && mazewish.length() > 0)
							{
								String[] mazeItem = mazewish.split("&");
								boolean flag = true;
								String str = "";
								for (int i = 0; i < mazeItem.length; i++)
								{
									String[] temp = mazeItem[i].split("-");
									if (StringUtil.getInt(temp[0]) == mazeId)
									{
										flag = false;
										str = mazewish.replace(mazeItem[i], mazeItem[i] + "-" + itemId);
										break;
										// temp[1] = itemId + "";
										// mazeItem[i] = temp[0] + "-" +
										// temp[1];
									}
								}
								if (flag)
								{
									str = mazewish + "&" + mazeId + "-" + itemId;
								}
								// for (int j = 0; j < mazeItem.length; j++)
								// {
								// if (str != null && str.length() > 0)
								// {
								// str = str + "&" + mazeItem[j];
								// }
								// else
								// {
								// str = mazeItem[j];
								// }
								// }
								mazewish = str;
							}
							else
							{
								mazewish = mazeId + "-" + itemId;
							}
							pi.player.setMazeWish(mazewish);
							wrj.mazeBossDrop = pi.player.getMazeBossDrop();
							wrj.mazeWish = pi.player.getMazeWish();
						}
						else
						{
							wrj.errorCode = errorcode;
						}
						msg = JSON.toJSONString(wrj);
					}
					else if (uj.ui == 57)// 迷宫通关大奖
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|迷宫通关大奖");
						ej = new MazeRewardResultJson();
						MazeRewardResultJson mrrj = (MazeRewardResultJson) ej;
						
						int td = uj.type;// 关卡id
						int r = uj.show;// 战斗结果
						MazeBattleData mData = MazeBattleData.getMazeBattleData(td);
						int errorcode = 0;
						// 校验阶段
						if (errorcode == 0)
						{
							if (mData.type != 2 || r != 1)
							{
								errorcode = 124;
							}
						}
						// 功能阶段
						if (errorcode == 0)
						{
							int random = Random.getNumber(0, 1000);
							MazeRewardData mrData = MazeRewardData.getMazeRewardData(mData.map);
							if (mrData != null)
							{
								List<String> mazereward = mrData.reward;
								int st = 0;
								int end = 0;
								for (int i = 0; i < mazereward.size(); i++)
								{
									String str = mazereward.get(i);
									if (str != null && str.length() > 0)
									{
										String[] temp = str.split("-");
										end = StringUtil.getInt(temp[2]) + st;
										if (random > st && random <= end)
										{
											Statics.getReward(StringUtil.getInt(temp[0]), temp[1], pi);
											mrrj.reward = str;
											break;
										}
										else
										{
											st = end;
										}
									}
								}
							}
							else
							{
								errorlogger.error("mazerewarddata is null");
							}
						}
						else
						{
							mrrj.errorCode = errorcode;
						}
						msg = JSON.toJSONString(mrrj);
					}
					else if (uj.ui == 61)
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|抽卡排行活动界面");
						ej = new LotEventRankResultJson();
						LotEventRankResultJson lerrj = (LotEventRankResultJson) ej;
						long curTime = System.currentTimeMillis();
						TakeCardTimeData tctd = TakeCardTimeData.getData(1);
						lerrj.uc = tctd.cast;
						LotRank lr = Cache.getInstance().getLotRankById(pi.player.getId());
						HashMap<Integer, LotRank> lotRankMap = Cache.getInstance().getLotRankMap2();
						List<String> s = new ArrayList<String>();
						if (lotRankMap.values() != null)
						{
							for (LotRank lotRank : lotRankMap.values())
							{
								Player player = Cache.getInstance().getPlayer(lotRank.getPlayerId());
								if (pi.player.getId() == player.getId())
								{
									if (lotRank.getScore() % tctd.interracialcast == 0)
									{
										
										lerrj.uc = 0;
									}
									if (lotRank.getScoreLotNum() > 0)
									{
										lerrj.uc = 0;
									}
								}
								
								String str = null;
								if (lotRank.getRank() <= 5)
								{
									if (pi.player.getId() == player.getId())
									{
										str = lotRank.getRank() + "-" + player.getName() + "-" + lotRank.getScore() + "-" + 1;
									}
									else
									{
										str = lotRank.getRank() + "-" + player.getName() + "-" + lotRank.getScore() + "-" + 0;
									}
									s.add(str);
								}
							}
						}
						List<String> str = new ArrayList<String>();
						if (str != null)
						{
							for (String string : Cache.instance.lotRanksView)
							{
								String[] s1 = string.split("-");
								if (StringUtil.getInt(s1[3]) == pi.player.getId())
								{
									str.add(s1[0] + "-" + s1[1] + "-" + s1[2] + "-" + 1);
								}
								else
								{
									str.add(s1[0] + "-" + s1[1] + "-" + s1[2] + "-" + 0);
								}
							}
						}
						lerrj.sr = str;
						// List<CardkuData> list = null;
						CardkuData ck1 = CardkuData.getCardkuData2(1);
						CardkuData ck2 = CardkuData.getCardkuData2(2);
						CardkuData ck3 = CardkuData.getCardkuData2(3);
						int cs = 0;
						int n = 0;
						if (lr != null)
						{
							int num = lr.getLotNum();
							if (num > ck3.firstjump)
							{
								if (num % ck2.secondjump == 0)
								{
									cs = 2;
									// list = CardkuData.getCardkuDatas(2);
								}
								else if (num % ck3.secondjump == 0 || (num % ck3.secondjump == 0 && num % ck2.secondjump == 0))
								{
									cs = 3;
									// n = ck2.secondjump - (ck3.secondjump -
									// ck2.secondjump);
									// list = CardkuData.getCardkuDatas(3);
									n = ck2.secondjump - (num % ck2.secondjump);
								}
								else
								{
									cs = 1;
									// list = CardkuData.getCardkuDatas(1);
									n = ck2.secondjump - (num % ck2.secondjump);
								}
							}
							else
							{
								if (num == ck2.firstjump)
								{
									cs = 2;
									// list = CardkuData.getCardkuDatas(2);
								}
								else if (num == ck3.firstjump)
								{
									cs = 3;
									// list = CardkuData.getCardkuDatas(3);
									n = ck2.secondjump;
								}
								else
								{
									cs = 1;
									// list = CardkuData.getCardkuDatas(1);
									if (num >= 0 && num < ck2.firstjump)
									{
										n = ck2.firstjump - num;
									}
									if (num > ck2.firstjump)
									{
										n = ck2.firstjump + ck2.secondjump - num;
									}
								}
							}
							lerrj.cs = cs;
							lerrj.n = n;
							// if (list != null)
							// {
							// List<Integer> ss =new ArrayList<Integer>();
							// for (int i = 0; i < list.size(); i++)
							// {
							// CardkuData ckd = list.get(i);
							// // lerrj.cs.add(ckd.card);
							// ss.add(ckd.card);
							// }
							// lerrj.cs =ss;
							// }
							long time = tctd.time * 3600 * 1000;
							if (curTime - lr.getLastLotTime() >= time)
							{
								lerrj.lt = 0;
							}
							else
							{
								lerrj.lt = (int) ((lr.getLastLotTime() + time) - curTime) / 1000;
							}
							lerrj.score = lr.getScore();
							lerrj.rank = lr.getRank();
						}
						else
						{
							lerrj.n = ck2.firstjump;
							lerrj.cs = 1;
							lerrj.lt = 0;
							lerrj.score = 0;
							lerrj.rank = 10000;
						}
						lerrj.crystal = pi.player.getCrystal() + pi.player.getCrystalPay();
						Activity activity = Cache.getInstance().getActivityById(9);
						if (activity != null)
						{
							lerrj.bt = activity.sttime;
							lerrj.et = activity.endtime;
							lerrj.ht = (int) (StringUtil.getTimeStamp(lerrj.et + " 23:59:59") - curTime) / 1000;
						}
						else
						{
							lerrj.bt = "";
							lerrj.et = "";
							lerrj.ht = 0;
						}
						msg = JSON.toJSONString(lerrj);
					}
					else if (uj.ui == 58)// 转盘主界面（档位界面）
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|转盘档位界面");
						ej = new RunnerUiResultJson();
						RunnerUiResultJson rurj = (RunnerUiResultJson) ej;
						Activity a = Cache.getInstance().getActivityById(8);
						if (a.getSttime() != null && a.getEndtime() != null && a.getSttime().length() > 0 && a.getEndtime().length() > 0)
						{
							if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getEndtime()) > 0)
							{
								rurj.errorCode = 113;
							}
							else if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getSttime()) < 0)
							{
								rurj.errorCode = 114;
							}
						}
						if (rurj.errorCode == 0)
						{
							List<RunnerData> list = RunnerData.getDatasByState(1);
							HashMap<Integer, Integer> lMap = pi.getLottoTimesMap();
							List<ShopElement> ses = new ArrayList<ShopElement>();
							for (int i = 0; i < list.size(); i++)
							{
								ShopElement se = new ShopElement();
								se.setId(list.get(i).id);
								if (lMap.containsKey(list.get(i).id))
								{
									se.setNum(lMap.get(list.get(i).id));
								}
								else
								{
									se.setNum(0);
								}
								ses.add(se);
							}
							rurj.list = ses;
						}
						msg = JSON.toJSONString(rurj);
						
					}
					else if (uj.ui == 59)// 转动转盘界面
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|转盘界面");
						ej = new RunnerResultJson();
						RunnerResultJson rrj = (RunnerResultJson) ej;
						HashMap<Integer, String> lastMap = pi.getLastLottoAwardMap();
						// 校验阶段
						int errorcode = 0;
						HashMap<Integer, Integer> lMap = pi.getLottoTimesMap();
						Activity a = Cache.getInstance().getActivityById(8);
						if (a.getSttime() != null && a.getEndtime() != null && a.getSttime().length() > 0 && a.getEndtime().length() > 0)
						{
							if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getEndtime()) > 0)
							{
								errorcode = 113;
							}
							else if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getSttime()) < 0)
							{
								errorcode = 114;
							}
						}
						if (uj.type == 2)
						{
							if (errorcode == 0)
							{
								if (lMap.containsKey(uj.i))
								{
									if (lMap.get(uj.i) <= 0)
									{
										errorcode = 127;
									}
								}
								else
								{
									errorcode = 127;
								}
							}
						}
						
						// 功能阶段
						if (errorcode == 0)
						{
							if (uj.type == 1)// 转盘界面
							{
								if (lastMap.containsKey(uj.i))
								{
									String[] temp = lastMap.get(uj.i).split("-");
									rrj.type = StringUtil.getInt(temp[0]);
									rrj.id = temp[1];
								}
								// 转动次数 - id;
								RunnerData runnerData = RunnerData.getRunnerData(uj.i);
								List<RunData> runDatas = RunData.getDatasByRun(runnerData.condition);
								String lottoturn = pi.player.getLottoTurnTimes();
								List<ShopElement> ses2 = new ArrayList<ShopElement>();
								for (RunData runData : runDatas)
								{
									boolean is = true;
									
									if (lottoturn != null && !"".equals(lottoturn))
									{
										String str[] = pi.player.getLottoTurnTimes().split("&");
										for (int i = 0; i < str.length; i++)
										{
											String[] s = str[i].split("-");
											if (runData.run == StringUtil.getInt(s[1]))
											{
												is = false;
												ShopElement se = new ShopElement();
												se.setId(runData.id);
												if (StringUtil.getInt(s[2]) < runData.truntime)
												{
													se.setNum(runData.truntime - StringUtil.getInt(s[2]));
												}
												else
												{
													se.setNum(0);
												}
												ses2.add(se);
											}
										}
									}
									if (is)
									{
										ShopElement se = new ShopElement();
										se.setId(runData.id);
										se.setNum(runData.truntime);
										ses2.add(se);
									}
								}
								rrj.view = ses2;
							}
							else if (uj.type == 2)// 转动转盘
							{
								int randnum = Random.getNumber(0, 1000);
								// 随机奖品
								boolean is = true;
								RunnerData rData = RunnerData.getRunnerData(uj.i);
								// 根据抽奖次数判断是普通概率或者必得概率
								if (lMap.containsKey(uj.ui))
								{
									if (lMap.get(uj.i) % rData.circulationtime == 0)
									{
										is = false;
									}
								}
								
								List<String> awards = RunnerData.getRunnerData(uj.i).item_pro;
								int st = 0;
								int end = 0;
								int index = 0;
								for (int i = 0; i < awards.size(); i++)
								{
									index++;
									String[] temp = awards.get(i).split("-");
									if (is)
									{
										end = st + StringUtil.getInt(temp[2]);
									}
									else
									{
										end = st + StringUtil.getInt(temp[3]);
									}
									if (randnum >= st && randnum <= end)
									{
										
										rrj.type = StringUtil.getInt(temp[0]);
										rrj.id = temp[1];
										// 奖品放进背包
										Statics.getReward(rrj.type, rrj.id, pi);
										// 加入广播
										if (StringUtil.getInt(temp[4]) == 1)
										{
											Announce e = null;
											
											switch (rrj.type)
											{
												case 1:// items
													String[] it = rrj.id.split(",");
													ItemsData iData = ItemsData.getItemsData(StringUtil.getInt(it[0]));
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + iData.star + "星道具" + iData.name + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 2:// equip
													String[] es = rrj.id.split(",");
													EquipData eData = EquipData.getData(StringUtil.getInt(es[0]));
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + eData.star + "星装备" + eData.name + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 3:// card
													String[] cs = rrj.id.split(",");
													CardData cData = CardData.getData(StringUtil.getInt(cs[0]));
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + cData.star + "星卡牌" + cData.name + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 4:// skill
													String[] ss = rrj.id.split(",");
													SkillData sData = SkillData.getData(StringUtil.getInt(ss[0]));
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + sData.star + "星主动技能" + sData.name + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 5:// ps
													String[] pss = rrj.id.split(",");
													PassiveSkillData psData = PassiveSkillData.getData(StringUtil.getInt(pss[0]));
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + psData.star + "星被动技能" + psData.name + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 6:// gold
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + "金币" + rrj.id + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 7:// exp
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + "人物经验" + rrj.id + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 8:// crystal
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + "钻石" + rrj.id + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 9:// rune
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + "符文" + rrj.id + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 10:// power
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + "体力" + rrj.id + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
												case 11:// friend
													e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + "友情值" + rrj.id + "！", 1, 1, 1);
													Cache.instance.addAnnounce(e);
													break;
											}
										}
										break;
									}
									else
									{
										st = end;
									}
								}
								rrj.index = index;
								// 记录获得的物品
								if (lastMap.containsKey(uj.i))
								{
									// 之前获得过。更新
									String lastAward = pi.player.getLastLottoAward();
									String[] str = lastAward.split("&");
									String string = "";
									for (int i = 0; i < str.length; i++)
									{
										String[] temp = str[i].split("-");
										if (StringUtil.getInt(temp[0]) == uj.i)
										{
											str[i] = temp[0] + "-" + rrj.type + "-" + rrj.id;
										}
										if (string != null && string.length() > 0)
										{
											string = string + "&" + str[i];
										}
										else
										{
											string = str[i];
										}
									}
									pi.player.setLastLottoAward(string);
								}
								else
								// 如果该档位之前没有获得过奖品,直接追加
								{
									pi.player.setLastLottoAward(pi.player.getLastLottoAward() + "&" + uj.i + "-" + rrj.type + "-" + rrj.id);
								}
								// 减少抽奖次数
								pi.player.removeLottoTimes(uj.i, 1);
								// 增加转盘转动次数
								pi.player.addLottoTurnTimes(rData.type, rData.condition, 1);
								// 更新转动n次的奖励
								pi.player.updateLottoReward(rData.type, rData.condition);
								// // 增加转盘转动次数
								// pi.player.addLottoTurnTimes(rData.type,
								// rData.condition, 1);
								// // 更新转动n次的奖励
								// pi.player.updateLottoReward(rData.type,
								// rData.condition);
								// // 增加转盘转动次数
								// pi.player.addLottoTurnTimes(rData.type,
								// rData.condition, 1);
								// // 更新转动n次的奖励
								// pi.player.updateLottoReward(rData.type,
								// rData.condition);
								// id次数
								RunnerData runnerData = RunnerData.getRunnerData(uj.i);
								List<RunData> runDatas = RunData.getDatasByRun(runnerData.condition);
								String lottoturn = pi.player.getLottoTurnTimes();
								List<ShopElement> ses2 = new ArrayList<ShopElement>();
								for (RunData runData : runDatas)
								{
									if (lottoturn != null)
									{
										String str[] = pi.player.getLottoTurnTimes().split("&");
										for (int i = 0; i < str.length; i++)
										{
											String[] s = str[i].split("-");
											if (runData.run == StringUtil.getInt(s[1]))
											{
												ShopElement se = new ShopElement();
												se.setId(runData.id);
												if (StringUtil.getInt(s[2]) < runData.truntime)
												{
													se.setNum(runData.truntime - StringUtil.getInt(s[2]));
												}
												else
												{
													se.setNum(0);
												}
												ses2.add(se);
											}
										}
									}
								}
								rrj.view = ses2;
							}
							// 转动n次奖励领取的标识
							List<ShopElement> ses3 = new ArrayList<ShopElement>();
							String lottoReward = pi.player.getLottoReward();
							if (lottoReward != null && lottoReward.length() > 0)
							{
								String[] str = lottoReward.split("&");
								for (int i = 0; i < str.length; i++)
								{
									if (str[i] != null && str[i].length() > 0)
									{
										String[] temp = str[i].split("-");
										ShopElement se = new ShopElement();
										se.setId(StringUtil.getInt(temp[0]));
										se.setNum(StringUtil.getInt(temp[1]));
										ses3.add(se);
									}
								}
							}
							rrj.list = ses3;
						}
						else
						{
							rrj.errorCode = errorcode;
						}
						msg = JSON.toJSONString(rrj);
					}
					else if (uj.ui == 60)// 领取转动奖励
					{
						playerlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|查看ui" + uj.ui + "|领取转动奖励");
						ej = new RunRewardResultJson();
						RunRewardResultJson rrj = (RunRewardResultJson) ej;
						RunData rData = RunData.getRunData(uj.i);
						HashMap<String, Integer> turnMap = pi.player.lottoTurnTimeMap();
						if (rData != null)
						{
							int times = turnMap.get(rData.runtype + "-" + rData.run);
							if (times < rData.truntime)
							{
								rrj.errorCode = 128;
							}
							if (rrj.errorCode == 0)
							{
								String[] str = pi.player.getLottoReward().split("&");
								for (int i = 0; i < str.length; i++)
								{
									String[] temp = str[i].split("-");
									if (StringUtil.getInt(temp[0]) == uj.i)
									{
										if (StringUtil.getInt(temp[1]) == 1)
										{
											rrj.errorCode = 91;
										}
										break;
									}
								}
							}
							if (rrj.errorCode == 0)
							{
								Statics.getReward(rData.rewardtype, rData.timereward, pi);
								String reward = "";
								String[] str = pi.player.getLottoReward().split("&");
								for (int i = 0; i < str.length; i++)
								{
									String[] temp = str[i].split("-");
									if (StringUtil.getInt(temp[0]) == uj.i)
									{
										str[i] = temp[0] + "-" + 1;
									}
									if (reward != null && reward.length() > 0)
									{
										reward = reward + "&" + str[i];
									}
									else
									{
										reward = str[i];
									}
								}
								pi.player.setLottoReward(reward);
							}
						}
						else
						{
							errorlogger.info("rundata is null");
						}
						msg = JSON.toJSONString(rrj);
					}
					
				}
				else
				{
					ej = new ErrorJson();
					ej.errorCode = -3;
					msg = JSON.toJSONString(ej);
				}
			}
			else
			{
				ej = new ErrorJson();
				ej.errorCode = -1;
				msg = JSON.toJSONString(ej);
			}
			Cache.recordRequestNum(ej);
			logger.info("ui-msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			logger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 判断装备是否是有空位或者提升 **/
	public static Integer checkEquipStar(List<PackElement> equips, List<Equip> eList, HashMap<Integer, Equip> equipMap, int eType)
	{
		int type = 0;
		if (equipMap.containsKey(eType))
		{
			Equip e = equipMap.get(eType);
			for (PackElement pe : equips)
			{
				if (pe != null)
				{
					EquipData eData = EquipData.getData(pe.getDataId());
					EquipData eData0 = EquipData.getData(e.getEquipId());
					if (eData != null && eData0 != null)
					{
						if (eData.star > eData0.star && eData.type == eData0.type)
						{
							type = 2;
							break;
						}
					}
				}
			}
		}
		else
		{
			for (PackElement pe : equips)
			{
				if (pe != null)
				{
					EquipData eData = EquipData.getData(pe.getDataId());
					if (eData != null)
					{
						if (eData.type == eType)
						{
							type = 1;
							break;
						}
					}
				}
			}
		}
		return type;
	}
	
	/**
	 * 背包数据
	 * 
	 * @param pi
	 * @param type
	 *            1：card 2:skill 3:equip 4:item 8:passiveSkill
	 * @return
	 */
	public static List<PackElement> packElements(PlayerInfo pi, int type)
	{
		List<PackElement> pes = new ArrayList<PackElement>();
		switch (type)
		{
			case 1:// 角色卡card
				pes.addAll(getElements(pi, 1));
				break;
			case 2:// skill
				pes.addAll(getElements(pi, 2));
				break;
			case 3:// equip
				pes.addAll(getElements(pi, 4));
				break;
			case 4:// item
				pes.addAll(getElements(pi, 5));
				break;
			case 8:// passiveSkill
				pes.addAll(getElements(pi, 3));
				break;
		}
		return pes;
	}
	
	/**
	 * 获取pe
	 * 
	 * @param pi
	 * @param type
	 *            1card ,2skill , 3passiveskill , 4equip , 5item
	 * @param i
	 *            绝对索引
	 * @return
	 */
	public static PackElement getPe(PlayerInfo pi, int type, int i)
	{
		List<PackElement> pes = getElements(pi, type);
		for (PackElement pe : pes)
		{
			if (pe.getI() == i)
			{
				return pe;
			}
		}
		return null;
	}
	
	/** 1,2,3,4,5 **/
	public static List<PackElement> getElements(PlayerInfo pi, int eType)
	{
		List<PackElement> pes = new ArrayList<PackElement>();
		switch (eType)
		{
			case 1:
				List<Card> cards = pi.getCards();
				for (int i = 0; i < cards.size(); i++)
				{
					pes.add(Statics.createPackElement(i, cards.get(i), pi));
				}
				break;
			case 2:
				List<Skill> skills = pi.getSkills();
				for (int i = 0; i < skills.size(); i++)
				{
					pes.add(Statics.createPackElement(i, skills.get(i), pi));
				}
				break;
			case 3:
				List<PassiveSkill> passiveSkills = pi.getPassiveSkillls();
				for (int i = 0; i < passiveSkills.size(); i++)
				{
					pes.add(Statics.createPackElement(i, passiveSkills.get(i), pi));
				}
				break;
			case 4:
				List<Equip> equips = pi.getEquips();
				for (int i = 0; i < equips.size(); i++)
				{
					pes.add(Statics.createPackElement(i, equips.get(i), pi));
				}
				break;
			case 5:
				List<Item> items = pi.getItems();
				for (int i = 0; i < items.size(); i++)
				{
					pes.add(Statics.createPackElement(i, items.get(i), pi));
				}
				break;
		}
		return pes;
	}
	
	/**
	 * 背包中可出售的物品
	 * 
	 * @param pi
	 * @param type
	 *            1：card 2:skill 3:equip 4:item 8:passiveSkill
	 * @return
	 */
	public static List<PackElement> packSales(PlayerInfo pi, int type)
	{
		List<PackElement> pes = new ArrayList<PackElement>();
		switch (type)
		{
			case 1:// card
				List<Card> cards = pi.getCards();
				for (int i = 0; i < cards.size(); i++)
				{
					boolean b = true;
					Card c = cards.get(i);
					b = Statics.isInCardGroup(pi, 1, i);
					if (!b)
					{
						pes.add(Statics.createPackElement(i, c, pi));
					}
				}
				break;
			case 2:// skill
				List<Skill> skills = pi.getSkills();
				for (int i = 0; i < skills.size(); i++)
				{
					boolean b = true;
					Skill s = skills.get(i);
					b = Statics.isInCardGroup(pi, 2, i);
					if (!b)
					{
						pes.add(Statics.createPackElement(i, s, pi));
					}
				}
				break;
			case 3:// equip
				List<Equip> equips = pi.getEquips();
				for (int i = 0; i < equips.size(); i++)
				{
					boolean b = true;
					Equip e = equips.get(i);
					b = Statics.isInCardGroup(pi, 4, i);
					if (!b)
					{
						pes.add(Statics.createPackElement(i, e, pi));
					}
				}
				break;
			case 4:// item
				List<Item> items = pi.getItems();
				for (int i = 0; i < items.size(); i++)
				{
					Item item = items.get(i);
					pes.add(Statics.createPackElement(i, item, pi));
				}
				break;
			case 8:// passiveSkill
				List<PassiveSkill> passiveSkills = pi.getPassiveSkillls();
				for (int i = 0; i < passiveSkills.size(); i++)
				{
					boolean b = true;
					PassiveSkill ps = passiveSkills.get(i);
					b = Statics.isInCardGroup(pi, 3, i);
					if (!b)
					{
						pes.add(Statics.createPackElement(i, ps, pi));
					}
				}
				break;
		}
		return pes;
	}
	
	/**
	 * 主界面-当前战斗卡组的角色卡信息
	 * 
	 * @param pi
	 * @return
	 */
	public static List<String> getCardInfo(PlayerInfo pi)
	{
		List<String> list = new ArrayList<String>();
		Card[] cs = pi.getCardGroup().cards;
		for (int i = 0; i < cs.length; i++)
		{
			if (cs[i] != null)
			{
				list.add(cs[i].getCardId() + "");
			}
			else
			{
				list.add("-1");
			}
		}
		return list;
	}
	
	/**
	 * 获取所在卡组一个位置上的所有物品 lt@2014-1-13 下午07:47:11
	 * 
	 * @param pi
	 * @param eType
	 * @param index
	 *            绝对索引
	 * @return
	 */
	public static List<PackElement> getCardGroupPEs(PlayerInfo pi, int index)
	{
		CardGroup cg = Statics.getCardGroup(pi, 1, index);
		if (cg != null)
		{
			Object object = Statics.getObject(pi, 1, index);
			Card c = (Card) object;
			// 寻在此卡位置
			int mark = -1;
			for (int k = 0; k < cg.cards.length; k++)
			{
				if (cg.cards[k] == c)
				{
					mark = k;
					break;
				}
			}
			if (mark == -1)
			{
				return null;
			}
			// 把这个位置上的所有物品下发
			List<PackElement> list = new ArrayList<PackElement>();
			// card
			list.add(Statics.createPackElement(pi.getCards().indexOf(c), cg.cards[mark], pi));
			// skill
			Skill s = cg.skills[mark];
			if (s != null)
			{
				list.add(Statics.createPackElement(pi.getSkills().indexOf(s), s, pi));
			}
			// equips
			List<Equip> equipsTemp = cg.equips[mark];
			if (equipsTemp != null)
			{
				List<Equip> equips = pi.getEquips();
				for (Equip e : equipsTemp)
				{
					list.add(Statics.createPackElement(equips.indexOf(e), e, pi));
				}
			}
			return list;
		}
		return null;
	}
	
	// 获取所在卡组一个位置上的被动技能
	public static List<PackElement> getCardGroupPEs2(PlayerInfo pi, int index)
	{
		CardGroup cg = Statics.getCardGroup(pi, 1, index);
		if (cg != null)
		{
			Object object = Statics.getObject(pi, 1, index);
			Card c = (Card) object;
			// 寻在此卡位置
			int mark = -1;
			for (int k = 0; k < cg.cards.length; k++)
			{
				if (cg.cards[k] == c)
				{
					mark = k;
					break;
				}
			}
			if (mark == -1)
			{
				return null;
			}
			// 把这个位置上的所有物品下发
			List<PackElement> list = new ArrayList<PackElement>();
			// pSkill
			List<PassiveSkill> p = cg.pSkills[mark];
			if (p != null)
			{
				List<PassiveSkill> ps = pi.getPassiveSkillls();
				for (PassiveSkill e : p)
				{
					list.add(Statics.createPackElement(ps.indexOf(e), e, pi));
				}
			}
			return list;
		}
		return null;
	}
	
	/** 根据序号排序 **/
	public static List<PackElement> orderPackElement(List<PackElement> list, int type)
	{
		if (list != null && list.size() > 0)
		{
			for (int i = 0; i < list.size(); i++)
			{
				switch (type)
				{
					case 1:// card 根据number从小到大
						for (int j = list.size() - 1; j > 0; j--)
						{
							CardData cData1 = CardData.getData(list.get(j).getDataId());
							CardData cData2 = CardData.getData(list.get(j - 1).getDataId());
							if (cData1 != null && cData2 != null)
							{
								if (cData1.number < cData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("card为空");
							}
						}
						break;
					case 2:// skill 根据number从大到小
						for (int j = list.size() - 1; j > 0; j--)
						{
							SkillData sData1 = SkillData.getData(list.get(j).getDataId());
							SkillData sData2 = SkillData.getData(list.get(j - 1).getDataId());
							if (sData1 != null && sData2 != null)
							{
								if (sData1.number > sData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("skill为空");
							}
						}
						break;
					case 3:// passiveskill 根据number从大到小
						for (int j = list.size() - 1; j > 0; j--)
						{
							PassiveSkillData psData1 = PassiveSkillData.getData(list.get(j).getDataId());
							PassiveSkillData psData2 = PassiveSkillData.getData(list.get(j - 1).getDataId());
							if (psData1 != null && psData2 != null)
							{
								if (psData1.number > psData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("passiveskill为空");
							}
						}
						break;
					case 4:// equip 根据number从大到小
						for (int j = list.size() - 1; j > 0; j--)
						{
							EquipData eData1 = EquipData.getData(list.get(j).getDataId());
							EquipData eData2 = EquipData.getData(list.get(j - 1).getDataId());
							if (eData1 != null && eData2 != null)
							{
								if (eData1.number > eData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("equip为空");
							}
						}
						break;
					case 5:// item 根据number从小到大
						for (int j = list.size() - 1; j > 0; j--)
						{
							ItemsData iData1 = ItemsData.getItemsData(list.get(j).getDataId());
							ItemsData iData2 = ItemsData.getItemsData(list.get(j - 1).getDataId());
							if (iData1 != null && iData2 != null)
							{
								if (iData1.number < iData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
								else if (iData1.number == iData2.number)
								{
									PackElement pe1 = list.get(j);
									PackElement pe2 = list.get(j - 1);
									if (pe1.getPile() < pe2.getPile())
									{
										list.set(j, pe2);
										list.set(j - 1, pe1);
									}
								}
							}
							else
							{
								errorlogger.info("card为空");
							}
						}
						
						break;
				}
			}
		}
		return list;
	}
	
	/** 星级序号排序--星级从高到低，相同星级的序号小的在前 **/
	public static List<PackElement> orderPackElement2(List<PackElement> pes, int type)
	{
		if (pes != null && pes.size() > 0)
		{
			for (int i = 0; i < pes.size(); i++)
			{
				switch (type)
				{
					case 1:// card
						for (int j = pes.size() - 1; j > 0; j--)
						{
							CardData cData1 = CardData.getData(pes.get(j).getDataId());
							CardData cData2 = CardData.getData(pes.get(j - 1).getDataId());
							if (cData1 != null && cData2 != null)
							{
								if (cData1.star > cData2.star)// 星级高的在前
								{
									PackElement pe = new PackElement();
									pe = pes.get(j);
									pes.set(j, pes.get(j - 1));
									pes.set(j - 1, pe);
								}
								else if (cData1.star == cData2.star)// 同星级时,序号小的在前
								{
									if (cData1.number < cData2.number)
									{
										PackElement pe = new PackElement();
										pe = pes.get(j);
										pes.set(j, pes.get(j - 1));
										pes.set(j - 1, pe);
									}
								}
								
							}
							else
							{
								errorlogger.info("card为空");
							}
						}
						break;
					case 2:// skill
						for (int j = pes.size() - 1; j > 0; j--)
						{
							SkillData sData1 = SkillData.getData(pes.get(j).getDataId());
							SkillData sData2 = SkillData.getData(pes.get(j - 1).getDataId());
							if (sData1 != null && sData2 != null)
							{
								if (sData1.star > sData2.star)// 星级高的在前
								{
									PackElement pe = new PackElement();
									pe = pes.get(j);
									pes.set(j, pes.get(j - 1));
									pes.set(j - 1, pe);
								}
								else if (sData1.star == sData2.star)// 同星级时,序号小的在前
								{
									if (sData1.number < sData2.number)
									{
										PackElement pe = new PackElement();
										pe = pes.get(j);
										pes.set(j, pes.get(j - 1));
										pes.set(j - 1, pe);
									}
								}
							}
							else
							{
								errorlogger.info("skill为空");
							}
						}
						break;
					case 3:// passiveskill
						for (int j = pes.size() - 1; j > 0; j--)
						{
							PassiveSkillData psData1 = PassiveSkillData.getData(pes.get(j).getDataId());
							PassiveSkillData psData2 = PassiveSkillData.getData(pes.get(j - 1).getDataId());
							if (psData1 != null && psData2 != null)
							{
								if (psData1.star > psData2.star)
								{
									PackElement pe = new PackElement();
									pe = pes.get(j);
									pes.set(j, pes.get(j - 1));
									pes.set(j - 1, pe);
								}
								else if (psData1.star == psData2.star)
								{
									if (psData1.number < psData2.number)
									{
										PackElement pe = new PackElement();
										pe = pes.get(j);
										pes.set(j, pes.get(j - 1));
										pes.set(j - 1, pe);
									}
								}
							}
							else
							{
								errorlogger.info("passiveskill 为空");
							}
						}
						break;
					case 4:// equip
						for (int j = pes.size() - 1; j > 0; j--)
						{
							EquipData eData1 = EquipData.getData(pes.get(j).getDataId());
							EquipData eData2 = EquipData.getData(pes.get(j - 1).getDataId());
							if (eData1 != null && eData2 != null)
							{
								if (eData1.star > eData2.star)
								{
									PackElement pe = new PackElement();
									pe = pes.get(j);
									pes.set(j, pes.get(j - 1));
									pes.set(j - 1, pe);
								}
								else if (eData1.star == eData2.star)
								{
									if (eData1.number < eData2.number)
									{
										PackElement pe = new PackElement();
										pe = pes.get(j);
										pes.set(j, pes.get(j - 1));
										pes.set(j - 1, pe);
									}
								}
							}
							else
							{
								errorlogger.info("equip 为空");
							}
						}
						break;
				}
			}
			
		}
		return pes;
	}
	
	/** 根据number排序**和排序1相反 **/
	public static List<PackElement> orderPackElement3(int type, List<PackElement> list, PlayerInfo pi)
	{
		if (list != null && list.size() > 0)
		{
			for (int i = 0; i < list.size(); i++)
			{
				switch (type)
				{
					case 1:// card 根据number从大到小
						for (int j = list.size() - 1; j > 0; j--)
						{
							CardData cData1 = CardData.getData(list.get(j).getDataId());
							CardData cData2 = CardData.getData(list.get(j - 1).getDataId());
							if (cData1 != null && cData2 != null)
							{
								if (cData1.number > cData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("card为空");
							}
						}
						break;
					case 2:// skill 根据number从小到大
						for (int j = list.size() - 1; j > 0; j--)
						{
							SkillData sData1 = SkillData.getData(list.get(j).getDataId());
							SkillData sData2 = SkillData.getData(list.get(j - 1).getDataId());
							if (sData1 != null && sData2 != null)
							{
								if (sData1.number < sData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("skill为空");
							}
						}
						break;
					case 3:// equip 根据number从小到大
						for (int j = list.size() - 1; j > 0; j--)
						{
							EquipData eData1 = EquipData.getData(list.get(j).getDataId());
							EquipData eData2 = EquipData.getData(list.get(j - 1).getDataId());
							if (eData1 != null && eData2 != null)
							{
								if (eData1.number < eData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("equip为空");
							}
						}
						break;
					case 4:// item 根据number从小到大
						for (int j = list.size() - 1; j > 0; j--)
						{
							ItemsData iData1 = ItemsData.getItemsData(list.get(j).getDataId());
							ItemsData iData2 = ItemsData.getItemsData(list.get(j - 1).getDataId());
							if (iData1 != null && iData2 != null)
							{
								if (iData1.number < iData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("equip为空");
							}
						}
						break;
					case 8:// passiveskill 根据number从小到大
						for (int j = list.size() - 1; j > 0; j--)
						{
							PassiveSkillData psData1 = PassiveSkillData.getData(list.get(j).getDataId());
							PassiveSkillData psData2 = PassiveSkillData.getData(list.get(j - 1).getDataId());
							if (psData1 != null && psData2 != null)
							{
								if (psData1.number < psData2.number)
								{
									PackElement pe = new PackElement();
									pe = list.get(j);
									list.set(j, list.get(j - 1));
									list.set(j - 1, pe);
								}
							}
							else
							{
								errorlogger.info("passiveskill为空");
							}
						}
						break;
				}
			}
		}
		return list;
	}
	
	/**
	 * 吞噬者排序--强化
	 * 
	 * @param pi
	 * @param list
	 *            筛选过后的强化数据
	 * @param type
	 *            1card 2skill 3passiveskill 4equip
	 * @return
	 */
	public static List<PackElement> orderIntensify(PlayerInfo pi, List<PackElement> list, int type)
	{
		List<PackElement> pes = new ArrayList<PackElement>();
		orderPackElement(list, type);
		
		pes = cgOrder(pi, type);// 按位置信息排序好的卡组
		for (int i = 0; i < list.size(); i++)
		{
			pes.add(list.get(i));
		}
		return pes;
	}
	
	/**
	 * 按照位置顺序排序的卡组数据
	 * 
	 * @param pi
	 * @param type
	 *            1card 2skill 3passiveskill 4equip
	 * @return
	 */
	public static List<PackElement> cgOrder(PlayerInfo pi, int type)
	{
		List<PackElement> pes = new ArrayList<PackElement>();
		CardGroup cg = pi.getCardGroup();
		List<Card> cardList = pi.getCards();
		List<Skill> skillList = pi.getSkills();
		List<PassiveSkill> pskillList = pi.getPassiveSkillls();
		List<Equip> equipList = pi.getEquips();
		HashMap<Integer, Card> cMap = new HashMap<Integer, Card>();
		switch (type)
		{
			case 1:// card
				for (int j = 0; j < cg.cards.length; j++)
				{
					if (cg.cards[j] != null)
					{
						Card c = cg.cards[j];
						if (c != null && !c.equals("n"))
						{
							CardData cData = CardData.getData(c.getCardId());
							if (cData != null)
							{
								if (cData.element != 5 && cData.race != 6 && cData.race != 7)
								{
									if (cData.element != 5 && cData.race != 6 && cData.race != 7)
									{
										if (!cMap.containsKey(c.getCardId()))
										{
											cMap.put(c.getCardId(), c);
											pes.add(Statics.createPackElement(cardList.indexOf(c), c, pi));
										}
									}
								}
							}
						}
					}
				}
				break;
			case 2:// skill
				for (int j = 0; j < cg.skills.length; j++)
				{
					if (cg.skills[j] != null)
					{
						Skill s = cg.skills[j];
						if (s != null && !s.equals("n"))
						{
							SkillData sData = SkillData.getData(s.getSkillId());
							if (sData != null)
							{
								if (sData.level != 5)
								{
									pes.add(Statics.createPackElement(skillList.indexOf(s), s, pi));
								}
							}
						}
					}
				}
				break;
			case 3:// passiveskill
				List<PassiveSkill>[] PassiveSkills = cg.pSkills;
				if (PassiveSkills.length > 0 && PassiveSkills != null)
				{
					for (int j = 0; j < PassiveSkills.length; j++)
					{
						if (PassiveSkills[j] != null)
						{
							List<PassiveSkill> ps = PassiveSkills[j];
							if (ps != null && ps.size() > 0)
							{
								for (int i = 0; i < ps.size(); i++)
								{
									PassiveSkill p = ps.get(i);
									PassiveSkillData pData = PassiveSkillData.getData(p.getPassiveSkillId());
									if (pData != null)
									{
										if (pData.level != 5)
										{
											pes.add(Statics.createPackElement(pskillList.indexOf(p), p, pi));
										}
									}
								}
							}
						}
					}
				}
				break;
			case 4:// equip
				List<Equip>[] equips = cg.equips;
				if (equips.length > 0 && equips != null)
				{
					for (int j = 0; j < equips.length; j++)
					{
						if (equips[j] != null)
						{
							List<Equip> es = equips[j];
							if (es != null && es.size() > 0)
							{
								for (int k = 0; k < es.size(); k++)
								{
									Equip e = es.get(k);
									if (e.getLevel() < pi.player.getLevel() * 3)
									{
										pes.add(Statics.createPackElement(equipList.indexOf(e), e, pi));
									}
								}
							}
						}
					}
				}
				break;
		}
		return pes;
	}
	
	/**
	 * 被吞噬者排序--强化
	 * 
	 * @param pi
	 * @param index
	 *            吞噬者索引
	 * @param list
	 *            筛选过后的被强化数据
	 * @param show
	 *            0为全部显示 1为本族显示
	 * @param type
	 * @return
	 */
	public static List<PackElement> intensifyOrder(PlayerInfo pi, List<PackElement> list, int index, int show, int type)
	{
		switch (type)
		{
			case 1:// card 根据number从大到小
				for (int j = 0; j < list.size(); j++)
				{
					for (int i = list.size() - 1; i > 0; i--)
					{
						CardData cData1 = CardData.getData(list.get(i - 1).getDataId());
						CardData cData2 = CardData.getData(list.get(i).getDataId());
						if (cData1 != null && cData2 != null)
						{
							if (cData1 != null && cData2 != null)
							{
								// new 强化消耗卡牌的排序是编号由高至低排序，取消相同卡牌和神格卡牌的优先级。
								if (cData1.number < cData2.number)
								{
									PackElement pe = list.get(i - 1);
									list.set(i - 1, list.get(i));
									list.set(i, pe);
								}
							}
						}
						else
						{
							errorlogger.info("被强化card为空");
						}
					}
				}
				break;
			case 2:// skill 根据number从小到大
				for (int j = 0; j < list.size(); j++)
				{
					for (int i = list.size() - 1; i > 0; i--)
					{
						SkillData sData1 = SkillData.getData(list.get(i).getDataId());
						SkillData sData2 = SkillData.getData(list.get(i - 1).getDataId());
						if (sData1 != null && sData2 != null)
						{
							if (sData1.number > sData2.number)
							{
								PackElement pe = list.get(i);
								list.set(i, list.get(i - 1));
								list.set(i - 1, pe);
							}
						}
						else
						{
							errorlogger.info("被强化skill为空");
						}
					}
				}
				break;
			case 3:// passiveskill
				for (int j = 0; j < list.size(); j++)
				{
					for (int i = list.size() - 1; i > 0; i--)
					{
						PassiveSkillData psData1 = PassiveSkillData.getData(list.get(i).getDataId());
						PassiveSkillData psData2 = PassiveSkillData.getData(list.get(i - 1).getDataId());
						if (psData1 != null && psData2 != null)
						{
							if (psData1.number > psData2.number)
							{
								PackElement pe = list.get(i);
								list.set(i, list.get(i - 1));
								list.set(i - 1, pe);
							}
						}
						else
						{
							errorlogger.info("被强化的passiveskill为空");
						}
					}
				}
		}
		return list;
	}
	
	/** 判断该合成物品是否可以制作 **/
	public static boolean checkCompose(ComposeData data, PlayerInfo pi)
	{
		boolean c = true;// 可以制作为true
		if (data != null)
		{
			if (data.material_num != null && data.material_num.size() > 0)
			{
				for (int i = 0; i < data.material_num.size(); i++)
				{
					c = true;
					String[] str = data.material_num.get(i).split("-");
					List<Item> items = pi.getTargetItems(StringUtil.getInt(str[0]));
					if (items != null && items.size() > 0)
					{
						int pile = pi.getTargetItemsNum(StringUtil.getInt(str[0]));
						if (pile < StringUtil.getInt(str[1]))
						{
							c = false;
							break;
						}
					}
					else
					{
						c = false;
						break;
					}
				}
			}
			else
			{
				c = false;
			}
		}
		else
		{
			errorlogger.info("该合成物品为空");
		}
		return c;
	}
	
	/**
	 * 可合成物品 排序
	 * 
	 * @param pi
	 * @param type
	 *            1card 2equip 3item
	 * @return
	 */
	public static List<String> compose(List<String> list, int type)
	{
		// 筛选
		if (list != null && list.size() > 0)
		{
			// 排序
			// 解锁的(星级高到低，同星级按照序号小到大)，未解锁（星级低到高，同星级序号大到小）(装备--解锁：星级从低到高。同星级序号小到大)
			for (int i = 0; i < list.size(); i++)
			{
				for (int j = list.size() - 1; j > 0; j--)
				{
					String[] str1 = list.get(j).split("-");
					String[] str2 = list.get(j - 1).split("-");
					// 均解锁
					if (str1[2].equals("1") && str2[2].equals("1"))
					{
						switch (type)
						{
							case 1:// card
								CardData cData1 = CardData.getData(StringUtil.getInt(str1[0]));
								CardData cData2 = CardData.getData(StringUtil.getInt(str2[0]));
								if (cData1 != null && cData2 != null)
								{
									if (cData1.star > cData2.star)
									{
										String s = list.get(j);
										list.set(j, list.get(j - 1));
										list.set(j - 1, s);
									}
									else if (cData1.star == cData2.star)
									{
										if (cData1.number < cData2.number)
										{
											String s = list.get(j);
											list.set(j, list.get(j - 1));
											list.set(j - 1, s);
										}
									}
								}
								else
								{
									errorlogger.info("card is null");
								}
								break;
							case 2:// equip
								EquipData eData1 = EquipData.getData(StringUtil.getInt(str1[0]));
								EquipData eData2 = EquipData.getData(StringUtil.getInt(str2[0]));
								if (eData1 != null && eData2 != null)
								{
									if (eData1.star < eData2.star)
									{
										String s = list.get(j);
										list.set(j, list.get(j - 1));
										list.set(j - 1, s);
									}
									else if (eData1.star == eData2.star)
									{
										if (eData1.number < eData2.number)
										{
											String s = list.get(j);
											list.set(j, list.get(j - 1));
											list.set(j - 1, s);
										}
										
									}
								}
								else
								{
									errorlogger.info("equip is null");
								}
								break;
							case 3:// item
								ItemsData iData1 = ItemsData.getItemsData(StringUtil.getInt(str1[0]));
								ItemsData iData2 = ItemsData.getItemsData(StringUtil.getInt(str2[0]));
								if (iData1 != null && iData2 != null)
								{
									if (iData1.star > iData2.star)
									{
										String s = list.get(j);
										list.set(j, list.get(j - 1));
										list.set(j - 1, s);
									}
									else if (iData1.star == iData2.star)
									{
										if (iData1.number < iData2.number)
										{
											String s = list.get(j);
											list.set(j, list.get(j - 1));
											list.set(j - 1, s);
										}
									}
								}
								else
								{
									errorlogger.info("items is null");
								}
								break;
						}
					}
					// j解锁，j-1未解锁
					else if (str1[2].equals("1") && str2[2].equals("0"))
					{
						String s = list.get(j);
						list.set(j, list.get(j - 1));
						list.set(j - 1, s);
					}
					// 均为解锁
					else if (str1[2].equals("0") && str2[2].equals("0"))
					{
						switch (type)
						{
							case 1:// card
								CardData cData1 = CardData.getData(StringUtil.getInt(str1[0]));
								CardData cData2 = CardData.getData(StringUtil.getInt(str2[0]));
								if (cData1 != null && cData2 != null)
								{
									if (cData1.star < cData2.star)
									{
										String s = list.get(j);
										list.set(j, list.get(j - 1));
										list.set(j - 1, s);
									}
									else if (cData1.star == cData2.star)
									{
										if (cData1.number > cData2.number)
										{
											String s = list.get(j);
											list.set(j, list.get(j - 1));
											list.set(j - 1, s);
										}
									}
								}
								else
								{
									errorlogger.info("card is null");
								}
								break;
							case 2:// equip
								EquipData eData1 = EquipData.getData(StringUtil.getInt(str1[0]));
								EquipData eData2 = EquipData.getData(StringUtil.getInt(str2[0]));
								if (eData1 != null && eData2 != null)
								{
									if (eData1.star < eData2.star)
									{
										String s = list.get(j);
										list.set(j, list.get(j - 1));
										list.set(j - 1, s);
									}
									else if (eData1.star == eData2.star)
									{
										if (eData1.number > eData2.number)
										{
											String s = list.get(j);
											list.set(j, list.get(j - 1));
											list.set(j - 1, s);
										}
										
									}
								}
								else
								{
									errorlogger.info("equip is null");
								}
								break;
							case 3:// item
								ItemsData iData1 = ItemsData.getItemsData(StringUtil.getInt(str1[0]));
								ItemsData iData2 = ItemsData.getItemsData(StringUtil.getInt(str2[0]));
								if (iData1 != null && iData2 != null)
								{
									if (iData1.star < iData2.star)
									{
										String s = list.get(j);
										list.set(j, list.get(j - 1));
										list.set(j - 1, s);
									}
									else if (iData1.star == iData2.star)
									{
										if (iData1.number > iData2.number)
										{
											String s = list.get(j);
											list.set(j, list.get(j - 1));
											list.set(j - 1, s);
										}
									}
								}
								else
								{
									errorlogger.info("items is null");
								}
								break;
						}
					}
				}
			}
		}
		return list;
	}
	
	/**
	 * 合成材料
	 * 
	 * @param pi
	 * @param i
	 *            要合成物品的id
	 * @return
	 */
	
	public static List<PackElement> composeMaterial(int i, int type, PlayerInfo pi)
	{
		List<PackElement> list = new ArrayList<PackElement>();
		ComposeData comData = ComposeData.getComDataByComposite(i, type);
		if (comData != null)
		{
			if (comData.material_num != null && comData.material_num.size() > 0)
			{
				for (int j = 0; j < comData.material_num.size(); j++)
				{
					String[] str = comData.material_num.get(j).split("-");
					List<Item> its = pi.getTargetItems(StringUtil.getInt(str[0]));
					if (its != null && its.size() > 0)
					{
						int pile = pi.getTargetItemsNum(StringUtil.getInt(str[0]));
						PackElement pe = new PackElement();
						pe.setDataId(StringUtil.getInt(str[0]));
						pe.setPile(pile);
						list.add(pe);
					}
				}
			}
		}
		return list;
	}
	
	/**
	 * 
	 * lt@2014-7-23 上午11:19:23
	 * 
	 * @param helpIds
	 * @param refreshRobot
	 * @return
	 */
	private static List<FriendElement> createFriendElements(List<String> helpIds, boolean fixRobot)
	{
		List<FriendElement> list = new ArrayList<FriendElement>();
		if (helpIds == null || helpIds.size() == 0)
		{
			list.addAll(FriendElement.getInitFriendElements());
		}
		else
		{
			for (int k = 0; k < helpIds.size(); k++)
			{
				String[] ss = helpIds.get(k).split("-");
				int helpId = StringUtil.getInt(ss[0]);
				int state = StringUtil.getInt(ss[1]);
				int unitId = StringUtil.getInt(ss[2]);
				/** 0已经是好友,1未申请,2已申请,3陌生人 **/
				FriendElement fe = new FriendElement(helpId, state, unitId);
				list.add(fe);
			}
			// 排序(好友战力降序,陌生人战力降序)
			List<FriendElement> temp1 = new ArrayList<FriendElement>();
			List<FriendElement> temp2 = new ArrayList<FriendElement>();
			for (int i = 0; i < list.size(); i++)
			{
				FriendElement fe = list.get(i);
				if (fe.t == 0)
				{
					for (int k = 0; k < temp1.size(); k++)
					{
						if (fe.bp > temp1.get(k).bp)
						{
							temp1.add(k, fe);
							break;
						}
					}
					if (!temp1.contains(fe))
					{
						temp1.add(fe);
					}
				}
				else
				{
					for (int k = 0; k < temp2.size(); k++)
					{
						if (fe.bp > temp2.get(k).bp)
						{
							temp2.add(k, fe);
							break;
						}
					}
					if (!temp2.contains(fe))
					{
						temp2.add(fe);
					}
				}
			}
			list.clear();
			list.addAll(temp1);
			list.addAll(temp2);
		}
		// 如果第一位要固定机器人好友
		if (fixRobot)
		{
			int fixId = 50;
			int index = -1;
			for (int k = 0; k < list.size(); k++)
			{
				FriendElement fe = list.get(k);
				if (fe.pid == fixId)
				{
					index = k;
					break;
				}
			}
			// 包含的话调整到第一位
			if (index >= 0)
			{
				if (index > 0)
				{
					FriendElement fe = list.remove(index);
					list.add(0, fe);
				}
			}
			// 不包含的话加入到第一位
			else
			{
				list.add(0, FriendElement.getOrl());
			}
		}
		return list;
	}
	
	/** 校验模块是否解锁 **/
	public static boolean checkMode(int mode, PlayerInfo pi)
	{
		UnLockData uData = UnLockData.getUnlockByMode(mode);
		if (uData != null)
		{
			if (uData.type == 1)
			{
				if (uData.method > pi.player.getMissionId())
				{
					return false;
				}
			}
			else if (uData.type == 2)
			{
				if (uData.method > pi.player.getLevel())
				{
					return false;
				}
			}
		}
		return true;
	}
	
	// 突破排序
	public static List<PackElement> breakOrder(List<PackElement> pes, Player p)
	{
		HashMap<Integer, PackElement> map = new HashMap<Integer, PackElement>();
		HashMap<Integer, PackElement> map1 = new HashMap<Integer, PackElement>();
		List<PackElement> pes1 = new ArrayList<PackElement>();
		for (int i = pes.size() - 1; i >= 0; i--)
		{
			pes1.add(pes.get(i));
		}
		
		for (int i = pes1.size() - 1; i >= 0; i--)
		{
			CardData cData = CardData.getData(pes1.get(i).getDataId());
			if (cData != null && cData.race == 6)// 神格卡
			{
				if (!map1.containsKey(cData.star))
				{
					map1.put(cData.star, pes1.get(i));
				}
				pes1.remove(i);
			}
			else if (cData != null && cData.race == 7)// 金币卡
			{
				pes1.remove(i);
			}
			else if (cData != null && cData.element == 5)// 经验卡
			{
				pes1.remove(i);
			}
			else if (cData != null && pes1.get(i).getBn() == 7)// 突破达上限的卡
			{
				if (!map.containsKey(pes1.get(i).getDataId()))
				{
					map.put(pes1.get(i).getDataId(), pes1.get(i));
				}
				pes1.remove(i);
			}
			else if (cData != null && cData.star <= 2)
			{
				pes1.remove(i);
			}
		}
		
		for (int i = pes1.size() - 1; i >= 0; i--)
		{
			if (pes1.get(i) != null)
			{
				CardData cData = CardData.getData(pes1.get(i).getDataId());
				int bn = pes1.get(i).getBn();
				EvolutionData evD = EvolutionData.getData(cData.star, bn + 1);
				// 判断突破1-3需要金罡心
				if (bn >= 0 && bn < 3)
				{
					if (p.getDiamond() >= evD.moneypercard)
					{
						pes1.get(i).setBnType(1);
					}
				}
				if (bn >= 3 && bn < 7)
				{
					// 玩家拥有此卡牌
					int pCardN = 0;
					// 玩家拥有万能突破卡牌数量
					int multCard = 0;
					for (int n = pes.size() - 1; n >= 0; n--)
					{
						if (pes.get(i) != null)
						{
							CardData cData2 = CardData.getData(pes.get(i).getDataId());
							if (pes.get(i).getUse() == 0)
							{// 不在卡组中的卡
								if (cData2 != null && cData2.race == 6 && cData2.star == cData.star)// 同星级神格卡
								{
									multCard++;
								}
								else if (cData2 != null && cData2.id == cData.id)// 同卡
								{
									pCardN++;
								}
							}
						}
					}
					if (p.getDiamond() >= evD.moneypercard && (pCardN + multCard) > evD.cards)
					{
						pes1.get(i).setBnType(1);
					}
					
				}
				// 判断重复的卡
				if (map.containsKey(pes1.get(i).getDataId()))
				{
					PackElement pe = new PackElement();
					pe = map.get(pes1.get(i).getDataId());
					if (pe.getBnType() == 0)
					{
						pe.setBnType(1);
					}
					pes1.remove(i);
				}
				else
				{
					map.put(pes1.get(i).getDataId(), pes1.get(i));
				}
			}
		}
		
		for (int i = 0; i < pes1.size(); i++)
		{
			for (int j = pes1.size() - 1; j > 0; j--)
			{
				if (pes1.get(j) != null && pes1.get(j - 1) != null)
				{
					if (pes1.get(j).getBnType() > pes1.get(j - 1).getBnType())
					{
						PackElement pe = pes1.get(j);
						pes1.set(j, pes1.get(j - 1));
						pes1.set(j - 1, pe);
					}
					CardData c1 = CardData.getData(pes1.get(j).getDataId());
					CardData c2 = CardData.getData(pes1.get(j - 1).getDataId());
					if (c1 != null && c2 != null)
					{
						if (pes1.get(j).getBnType() == pes1.get(j - 1).getBnType())
						{
							if (c1.number < c2.number)// number大的放在前边
							{
								PackElement pe = pes1.get(j);
								pes1.set(j, pes1.get(j - 1));
								pes1.set(j - 1, pe);
							}
						}
					}
					else
					{
						errorlogger.info("cdata is null");
					}
				}
				else
				{
					errorlogger.info("pes1.get(index) is null--breaknum");
				}
			}
		}
		return pes1;
	}
	
	/** 背包是否有新加的物品 **/
	public static Integer packageIfNew(PlayerInfo pi)
	{
		int i = 0;
		List<Card> cardList = pi.getCards();
		for (int c = 0; c < cardList.size(); c++)
		{
			if (cardList.get(c).getNewType() == 0)
			{
				i = 1;
				break;
			}
		}
		if (i == 0)
		{
			List<Equip> equipList = pi.getEquips();
			for (int e = 0; e < equipList.size(); e++)
			{
				if (equipList.get(e).getNewType() == 0)
				{
					i = 1;
					break;
				}
			}
		}
		if (i == 0)
		{
			List<Item> itemList = pi.getItems();
			for (int item = 0; item < itemList.size(); item++)
			{
				if (itemList.get(item).getNewType() == 0)
				{
					i = 1;
					break;
				}
			}
		}
		if (i == 0)
		{
			List<PassiveSkill> psList = pi.getPassiveSkillls();
			for (int p = 0; p < psList.size(); p++)
			{
				if (psList.get(p).getNewType() == 0)
				{
					i = 1;
					break;
				}
				
			}
		}
		if (i == 0)
		{
			List<Skill> sList = pi.getSkills();
			for (int s = 0; s < sList.size(); s++)
			{
				if (sList.get(s).getNewType() == 0)
				{
					i = 1;
					break;
				}
			}
		}
		return i;
	}
	
	/** 是否有未读邮件 **/
	public Integer getNewMail(PlayerInfo pi)
	{
		int i = 0;
		List<Mail> mails = pi.getMails();
		for (int m = 0; m < mails.size(); m++)
		{
			if (mails.get(m).getState() == 2)
			{
				i = 1;
				break;
			}
		}
		return i;
	}
	
	/** 是否有可以突破的卡牌 **/
	public Integer getBreakCard(PlayerInfo pi)
	{
		int k = 0;
		List<PackElement> pes = getElements(pi, 1);
		pes = breakOrder(pes, pi.player);
		for (PackElement pe : pes)
		{
			if (pe.getBnType() == 1)
			{
				k = 1;
				break;
			}
		}
		return k;
	}
	
	/**
	 * 获取掉落指引列表 lt@2014-5-10 下午08:10:14
	 * 
	 * @param pi
	 * @param itemId
	 * @return
	 */
	private static List<String> getDropGuides(PlayerInfo pi, int itemId)
	{
		List<String> result = new ArrayList<String>();
		// 解锁关卡
		List<Integer> unlocks = new ArrayList<Integer>();
		// 最近解锁三个普通关卡三个精英关卡(如果已解锁的普通关卡不足3个，自动用精英关卡填补，保持总数为6个，反之亦然，如果都不足3个那么列出所有已解锁关卡)
		List<Integer> ids1 = MissionData.getRecentUnlockMissions(pi.player.getMissionId(), 6, itemId);
		List<Integer> ids2 = MissionData.getRecentUnlockMissions(pi.player.getMissionId2(), 6, itemId);
		int num1 = ids1.size();
		int num2 = ids2.size();
		// 各取前3个
		if (num1 >= 3 && num2 >= 3)
		{
			for (int k = 0; k < 3; k++)
			{
				unlocks.add(ids1.get(k));
			}
			for (int k = 0; k < 3; k++)
			{
				unlocks.add(ids2.get(k));
			}
		}
		// 普通小于3,精英多取
		else if (num1 < 3)
		{
			unlocks.addAll(ids1);
			for (int k = 0; unlocks.size() < 6 && k < num2; k++)
			{
				unlocks.add(ids2.get(k));
			}
		}
		// 精英小于3,普通多取
		else if (num2 < 3)
		{
			unlocks.addAll(ids2);
			for (int k = 0; unlocks.size() < 6 && k < num1; k++)
			{
				unlocks.add(ids1.get(k));
			}
		}
		// 普通小于3,精英小于3,全取
		else
		{
			unlocks.addAll(ids1);
			unlocks.addAll(ids2);
		}
		// 即将解锁
		List<Integer> locks = new ArrayList<Integer>();
		// 即将解锁一个普通关卡
		locks.addAll(MissionData.getRecentLockMissions(pi.player.getMissionId(), 1, itemId));
		// 即将解锁一个精英关卡
		locks.addAll(MissionData.getRecentLockMissions(pi.player.getMissionId2(), 1, itemId));
		// 组建结果
		for (int mdId : unlocks)
		{
			int times = pi.getFightTimes(mdId);
			// ==关卡id-是否解锁(1解锁)-已完成次数==//
			String s = mdId + "-1-" + times;
			result.add(s);
		}
		for (int mdId : locks)
		{
			// ==关卡id-是否解锁(1解锁)-已完成次数==//
			String s = mdId + "-0-0";
			result.add(s);
		}
		return result;
	}
	
	/** 获取一个类型的数据,相同的卡牌只显示最早得到的一张 **/
	public List<PackElement> getElements2(PlayerInfo pi, int type)
	{
		List<PackElement> pes = new ArrayList<PackElement>();
		switch (type)
		{
			case 1:// card
			
				HashMap<Integer, Card> cMap = new HashMap<Integer, Card>();
				List<Card> cards = pi.getCards();
				for (int i = 0; i < cards.size(); i++)
				{
					Card c = cards.get(i);
					if (cMap.containsKey(c.getCardId()))
					{
						continue;
					}
					else
					{
						cMap.put(c.getCardId(), c);
						pes.add(Statics.createPackElement(i, c, pi));
					}
				}
				break;
			case 2:// skill
				List<Skill> skills = pi.getSkills();
				HashMap<Integer, Skill> sMap = new HashMap<Integer, Skill>();
				for (int i = 0; i < skills.size(); i++)
				{
					Skill s = skills.get(i);
					if (sMap.containsKey(s.getSkillId()))
					{
						continue;
					}
					else
					{
						sMap.put(s.getSkillId(), s);
						pes.add(Statics.createPackElement(i, s, pi));
					}
				}
				break;
			case 3:// passiveskill
				break;
			case 4:// equip
				List<Equip> equips = pi.getEquips();
				HashMap<Integer, Equip> eMap = new HashMap<Integer, Equip>();
				for (int i = 0; i < equips.size(); i++)
				{
					Equip e = equips.get(i);
					if (eMap.containsKey(e.getEquipId()))
					{
						continue;
					}
					else
					{
						eMap.put(e.getEquipId(), e);
						pes.add(Statics.createPackElement(i, e, pi));
					}
				}
				break;
			case 5:// item
				break;
			
		}
		return pes;
	}
	
	/** 判断主动节能或者装备卡是否在卡组中 显示与装备XXX **/
	public List<PackElementJson> getPackElementJsons(List<PackElement> pes, PlayerInfo pi, int type)
	{
		List<PackElementJson> pejs = new ArrayList<PackElementJson>();
		CardGroup cardGroup = pi.getCardGroup();
		List<Equip>[] equipList = cardGroup.getEquips();
		List<Equip> es = pi.getEquips();
		Skill[] skillList = cardGroup.getSkills();
		List<Skill> ss = pi.getSkills();
		List<PassiveSkill>[] psList = cardGroup.getpSkills();
		List<PassiveSkill> pss = pi.getPassiveSkillls();
		boolean position = true;
		for (int i = 0; i < pes.size(); i++)
		{
			PackElement pe = pes.get(i);
			PackElementJson pej = new PackElementJson();
			pej.pe = pe;
			if (type == 3)// equip
			{
				if (pe.getUse() == 1)// 装备在卡组中
				{
					for (int j = 0; j < equipList.length; j++)// 判断装备的位置
					{
						position = true;
						if (equipList[j] != null && equipList[j].size() > 0)
						{
							List<Equip> eList = equipList[j];
							for (Equip e : eList)
							{
								if (es.indexOf(e) == pe.getI())
								{
									position = false;
									Card c = cardGroup.getCard(j);// 根据位置得到目标卡的id
									pej.type = c.getCardId();
									break;
								}
							}
						}
						if (!position)
						{
							break;
						}
					}
					if (position)
					{
						errorlogger.error("卡组数据错误-equip");
					}
				}
				else
				// 装备不在卡组
				{
					pej.type = 0;
				}
			}
			else if (type == 2)// skill
			{
				if (pe.getUse() == 1)// 主动技能在卡组
				{
					for (int j = 0; j < skillList.length; j++)// 判断技能的位置
					{
						if (skillList[j] != null)
						{
							if (ss.indexOf(skillList[j]) == pe.getI())
							{
								Card c = cardGroup.getCard(j);// 根据位置获取目标卡
								pej.type = c.getCardId();
								position = false;
								break;
							}
						}
					}
					if (position)
					{
						errorlogger.info("卡组数据错误-skill");
					}
				}
				else
				// 不在卡组
				{
					pej.type = 0;
				}
			}
			else if (type == 8)// passiveskil
			{
				if (pe.getUse() == 1)// 技能在卡组
				{
					for (int j = 0; j < psList.length; j++)// 判断技能的位置
					{
						if (psList[j] != null)
						{
							for (PassiveSkill ps : psList[j])
							{
								if (pss.indexOf(ps) == pe.getI())
								{
									Card c = cardGroup.getCard(j);// 根据位置获取目标卡
									pej.type = c.getCardId();
									position = false;
									break;
								}
							}
						}
					}
					if (position)
					{
						errorlogger.info("卡组数据错误-pskill");
					}
				}
				else
				// 不在卡组
				{
					pej.type = 0;
				}
				
			}
			else
			{
				pej.type = 0;
			}
			pejs.add(pej);
		}
		return pejs;
	}
	
	/** 阵容N标识 */
	public Integer getNewTypeFormation(PlayerInfo pi)
	{
		// 空余的装备位置
		List<PackElement> cardList = new ArrayList<PackElement>();
		List<PackElement> equiplist = new ArrayList<PackElement>();
		List<PackElement> pskills = new ArrayList<PackElement>();
		List<PackElement> pes1 = getElements(pi, 4);// equip
		List<PackElement> pes2 = getElements(pi, 3);// pskill
		List<PackElement> pes3 = getElements(pi, 1);// card
		Card[] cards = pi.getCardGroup().getCards();
		HashMap<Integer, Card> cardMap = new HashMap<Integer, Card>();
		for (int i = 0; i < cards.length; i++)
		{
			if (cards[i] != null)
			{
				if (!cardMap.containsKey(cards[i].getCardId()))
				{
					cardMap.put(cards[i].getCardId(), cards[i]);
				}
			}
		}
		
		for (PackElement pe : pes1)
		{
			if (pe != null)
			{
				if (pe.getUse() == 0)
				{
					equiplist.add(pe);
				}
			}
		}
		for (PackElement pe : pes2)
		{
			if (pe != null)
			{
				if (pe.getUse() == 0)
				{
					pskills.add(pe);
				}
			}
		}
		for (PackElement pe : pes3)
		{
			if (pe != null)
			{
				if (pe.getUse() == 0)
				{
					if (!cardMap.containsKey(pe.getDataId()))// 过滤掉已经上阵的相同卡牌
					{
						cardList.add(pe);
					}
				}
			}
		}
		// 军团(有空的卡位或者卡牌身上有空的装备位置时提示)
		int cg = 0;
		// 解锁的卡位
		int cPosition = 0;
		for (int i = 27; i < 33; i++)
		{
			if (checkMode(i, pi))
			{
				cPosition++;
			}
		}
		CardGroup cardGroup = pi.getCardGroup();
		for (int j = 0; j < cardGroup.getCards().length; j++)
		{
			if (!(cardGroup.getCards()[j] == null || cardGroup.getCards()[j].equals("n")))
			{
				cg++;
			}
		}
		int team = 0;
		if (cPosition > cg)
		{
			if (cardList.size() > 0)
			{
				team = 1;
			}
		}
		else
		{
			team = 0;
		}
		if (team == 0)
		{
			for (int p = 0; p < 6; p++)
			{
				if (!(cardGroup.getCards()[p] == null || cardGroup.getCards()[p].equals("n")))
				{
					List<Equip> eList = cardGroup.getEquips(p);
					if (eList != null && eList.size() < 3)
					{
						if (equiplist != null && equiplist.size() > 0)
						{
							HashMap<Integer, Equip> equipMap = new HashMap<Integer, Equip>();
							for (Equip equip : eList)
							{
								EquipData equipData = EquipData.getData(equip.getEquipId());
								equipMap.put(equipData.type, equip);
							}
							for (int j = 0; j < equiplist.size(); j++)
							{
								if (equiplist.get(j) != null)
								{
									EquipData equipData2 = EquipData.getData(equiplist.get(j).getDataId());
									if (!equipMap.containsKey(equipData2.type))
									{
										team = 1;
										break;
									}
								}
							}
							
						}
					}
					else if (eList == null || eList.size() == 0)
					{
						if (equiplist != null && equiplist.size() > 0)
						{
							team = 1;
							break;
						}
					}
				}
			}
		}
		if (team == 0)// 提升
		{
			for (int p = 0; p < 6; p++)
			{
				if (!(cardGroup.getCards()[p] == null || cardGroup.getCards()[p].equals("n")))
				{
					List<Equip> eList = cardGroup.getEquips(p);
					if (eList != null)
					{
						for (int e = 0; e < eList.size(); e++)
						{
							EquipData eData = EquipData.getData(eList.get(e).getEquipId());
							for (PackElement pe : equiplist)
							{
								if (pe != null)
								{
									EquipData equipData = EquipData.getData(pe.getDataId());
									if (eData != null && equipData != null)
									{
										if (eData.type == equipData.type && equipData.star > eData.star)
										{
											team = 2;
											break;
										}
									}
									
								}
							}
						}
					}
				}
				
			}
		}
		// passiveskill
		if (team == 0)
		{
			for (int pn = 0; pn < 6; pn++)
			{
				if (!(cardGroup.getCards()[pn] == null || cardGroup.getCards()[pn].equals("n")))
				{
					List<PassiveSkill> ps = cardGroup.getPassiveSkills(pn);
					// 判断被动技能是否有新提醒
					if (ps != null)
					{
						for (int i = 0; i < 3; i++)
						{
							PassiveSkill p = ps.get(i);
							if (p == null && pskills != null && pskills.size() > 0)
							{
								boolean is = true;
								int num = 0;
								for (int j = 0; j < 3; j++)
								{
									PassiveSkill p2 = ps.get(j);
									if (p2 != null)
									{
										PassiveSkillData psd1 = PassiveSkillData.getData(p2.getPassiveSkillId());
										for (int k = 0; k < pskills.size(); k++)
										{
											PassiveSkillData psd2 = PassiveSkillData.getData(pskills.get(k).dataId);
											if (psd1.type == psd2.type)
											{
												num++;
												if (pskills.size() <= num)
												{
													is = false;
													break;
												}
											}
										}
									}
								}
								if (i == 0 && UiController.checkMode(34, pi) && is)
								{
									team = 1;
									break;
								}
								if (i == 1 && UiController.checkMode(35, pi) && is)
								{
									team = 1;
									break;
								}
								if (i == 2 && UiController.checkMode(36, pi) && is)
								{
									team = 1;
									break;
								}
							}
						}
					}
					else
					{
						for (int i = 0; i < 3; i++)
						{
							if (i == 0 && UiController.checkMode(34, pi) && pskills.size() > 0)
							{
								team = 1;
								break;
							}
							if (i == 1 && UiController.checkMode(35, pi) && pskills.size() > 0)
							{
								team = 1;
								break;
							}
							if (i == 2 && UiController.checkMode(36, pi) && pskills.size() > 0)
							{
								team = 1;
								break;
							}
						}
					}
				}
			}
		}
		return team;
	}
	
}
