package com.begamer.card.common.dao.finder.impl;

import org.springframework.aop.support.DefaultIntroductionAdvisor;

public class FinderIntroductionAdvisor extends DefaultIntroductionAdvisor {
	private static final long serialVersionUID = -8883446165517315062L;

	public FinderIntroductionAdvisor() {
		super(new FinderIntroductionInterceptor());
	}
};