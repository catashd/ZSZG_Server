package com.begamer.card.common.util.binRead;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

public class UnitSkillData implements PropertyReader
{
	public int index;
	public int number;
	public String name;
	public int type;
	public int cardtype;
	public int cardnum;
	public int cardid;
	public int card1;
	public int card2;
	public int card3;
	public int card4;
	public int card5;
	public int card6;
	public int card7;
	public int card8;
	public int icon;
	public int cost;
	public int aim;
	public int power;
	public String description;
	public int effect1;
	public int effect2;
	public int effect3;
	public int probability;
	public int lastRND;
	public String nameicon;
	public String nameeffect;
	public String backgroundColor;
	public String chargeCameraMove;
	public int chargePW;
	public String chargeEffect;
	//==蓄力音效==//
	public String chargeMusic;
	public float chargeEffectTime;
	public float chargePWSETime;
	public int attackType;
	public String castEffect;
	public int castPostion;
	public String castCameraMove;
	public int zoom;
	public float castPWSETime;
	public String actionCameraMove;
	public int actionPositionType;
	public float actionPrepareTime;
	public String actionSETime;
	//==攻击音效==//
	public String actionMusic;
	public String music;
	public float actionEffectTime;
	public float actionPWSETime;
	public int screenShake;
	public float screenShakeDelay;
	public float screenShakeLast;
	public String hurtEffect;
	//==受伤音效==//
	public String hurtMusic;
	public float hurtPWSETime;
	
	public int[] cards;

	private static HashMap<Integer, UnitSkillData> data=new HashMap<Integer, UnitSkillData>();
	
	public void addData()
	{
		cards=new int[8];
		cards[0]=card1;
		cards[1]=card2;
		cards[2]=card3;
		cards[3]=card4;
		cards[4]=card5;
		cards[5]=card6;
		cards[6]=card7;
		cards[7]=card8;
		data.put(index,this);
	}
	public void resetData()
	{
		data.clear();
	}
	public void parse(String[] ss)
	{
	}
	
	public static UnitSkillData getData(int index)
	{
		return data.get(index);
	}
	public static List<UnitSkillData> getAll()
	{
		List<UnitSkillData> list = new ArrayList<UnitSkillData>();
		for (UnitSkillData usd : data.values())
		{
			list.add(usd);
		}
		return list;
	}

	public static List<Integer> getUnitSkillIds(List<Integer> cardIds)
	{
		List<Integer> result=new ArrayList<Integer>();
		for(UnitSkillData us:data.values())
		{
			switch(us.cardtype)
			{
			case 1://人数组合//
				if(cardIds.size()>=us.cardnum)
				{
					result.add(us.index);
				}
				break;
			case 2://种族组合//
				if(cardIds.size()>=us.cardnum)
				{
					int num=0;
					for(int id:cardIds)
					{
						CardData cd=CardData.getData(id);
						switch(us.cardid)
						{
						case 0:
						{
							num++;
						}break;
						case 5:
						{
							if(cd.race==1)
							{
								num++;
							}
						}break;
						case 6:
						{
							if(cd.race==2)
							{
								num++;
							}
						}break;
						case 7:
						{
							if(cd.race==4)
							{
								num++;
							}
						}break;
						case 8:
						{
							if(cd.race==3)
							{
								num++;
							}
						}break;
						}
					}
					if(num>=us.cardnum)
					{
						result.add(us.index);
					}
				}
				break;
			case 3://属性组合//
				if(cardIds.size()>=us.cardnum)
				{
					int num=0;
					for(int id:cardIds)
					{
						CardData cd=CardData.getData(id);
						switch(us.cardid)
						{
						case 1:
							if(cd.element==1)
							{
								num++;
							}
							break;
						case 2:
							if(cd.element==2)
							{
								num++;
							}
							break;
						case 3:
							if(cd.element==3)
							{
								num++;
							}
							break;
						case 4:
							if(cd.element==4)
							{
								num++;
							}
							break;
						}
					}
					if(num>=us.cardnum)
					{
						result.add(us.index);
					}
				}
				break;
			case 4://特殊组合//
				if(cardIds.size()>=us.cardnum)
				{
					int num=0;
					for(int id:us.cards)
					{
						if(cardIds.contains(id))
						{
							num++;
						}
					}
					if(num>=us.cardnum)
					{
						result.add(us.index);
					}
				}
				break;
			case 5://性别组合//
				//TODO
				break;
			}
		}
		return result;
	}
	
	/**判断一个卡是否组成未解锁的合体技**/
	public static List<UnitSkillData> getUnitSkillDatasByCard(int cardId ,String unitskills)
	{
		List<UnitSkillData> list =new ArrayList<UnitSkillData>();
		for(UnitSkillData uData:data.values())
		{
			for(int k=0;k<uData.cards.length;k++)
			{
				if(uData.cards[k] !=0)
				{
					if(uData.cards[k]==cardId)
					{
						if(unitskills !=null && unitskills.length()>0 && !unitskills.contains(uData.index+""))
						{
							list.add(uData);
						}
						else if(unitskills ==null || unitskills.length()==0)
						{
							list.add(uData);
						}
						break;
					}
				}
			}
		}
		return list;
	}
}
