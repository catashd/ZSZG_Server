package com.begamer.card.common.util.binRead;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.model.pojo.Item;

public class ComposeData implements PropertyReader{

	public int id;
	/**合成类型**/
	public int type;
	/**合成物品**/
	public int composite;
	/**解锁类型**/
	public int style;
	/**解锁条件**/
	public int unlockmission;
	/**金币消耗**/
	public int cost;
	/**成功几率**/
	public int probability;
	/**材料-数量**/
	public List<String>  material_num;
	
	private static HashMap<Integer, ComposeData> data =new HashMap<Integer, ComposeData>();
	private static List<ComposeData> dataList =new ArrayList<ComposeData>();

	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location =0;
		id=StringUtil.getInt(ss[location]);
		type =StringUtil.getInt(ss[location+1]);
		composite =StringUtil.getInt(ss[location+2]);
		style =StringUtil.getInt(ss[location+3]);
		unlockmission =StringUtil.getInt(ss[location+4]);
		cost =StringUtil.getInt(ss[location+5]);
		probability =StringUtil.getInt(ss[location+6]);
		material_num =new ArrayList<String>();
		for(int i=0;i<4;i++)
		{
			location=7+i*2;
			int material =StringUtil.getInt(ss[location]);
			if(material==0)
			{
				continue;
			}
			int number =StringUtil.getInt(ss[location+1]);
			String material_numStr =material+"-"+number;
			material_num.add(material_numStr);
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
		dataList.clear();
	}
	
	public static ComposeData getComposeData(int id)
	{
		return data.get(id);
	}
	
	public static ComposeData getComDataByComposite(int composite ,int type)
	{
		for(ComposeData comData:data.values())
		{
			if(comData.composite==composite && comData.type ==type)
			{
				return comData;
			}
		}
		return null;
	}
	public static List<ComposeData> getComposeDatasBytype(int type)
	{
		List<ComposeData> list =new ArrayList<ComposeData>();
		for(ComposeData comdata:dataList)
		{
			if(comdata.type==type)
			{
				list.add(comdata);
			}
		}
		return list;
	}
	public static List<ComposeData> getComposeDatas()
	{
		return dataList;
	}
	
	/** 已解锁的合成物品data**/
	public static List<ComposeData> getComposeDataLocked(PlayerInfo pi,int type)
	{
		List<ComposeData> list =new ArrayList<ComposeData>();
		for(ComposeData comData : data.values())
		{
			if(comData.style==0)//自动开启
			{
				if(comData.type ==type)
				{
					list.add(comData);
				}
			}
			else if(comData.style==1)//等级解锁
			{
				if(comData.unlockmission<=pi.player.getLevel() && comData.type ==type)
				{
					list.add(comData);
				}
			}
			else if(comData.style==2)//关卡解锁
			{
				if(comData.unlockmission<=pi.player.getMissionId() && comData.type==type)
				{
					list.add(comData);
				}
			}
			else if(comData.style==3)
			{
				boolean a=true;
				List<String> needs =comData.material_num;
				for(int i=0;i<needs.size();i++)
				{
					String [] str =needs.get(i).split("-");
					List<Item> items =pi.getTargetItems(StringUtil.getInt(str[0]));
					if(items ==null || items.size()==0)
					{
						a =false;
						break;
					}
				}
				if(a)
				{
					list.add(comData);
				}
			}
		}
		return list;
	}
	
	/**所有已经解锁的物品。不包括自动解锁**/
	public static List<ComposeData> getComposeDataUnlocked(PlayerInfo pi)
	{
		List<ComposeData> list =new ArrayList<ComposeData>();
		for(ComposeData com:dataList)
		{
			if(com.style==1)
			{
				if(com.unlockmission<=pi.player.getLevel())
				{
					list.add(com);
				}
			}
			else if(com.style==2)
			{
				if(com.unlockmission<=pi.player.getMissionId())
				{
					list.add(com);
				}
			}
			else if(com.style==3)
			{
				boolean a=true;
				List<String> needs =com.material_num;
				for(int i=0;i<needs.size();i++)
				{
					String [] str =needs.get(i).split("-");
					List<Item> items =pi.getTargetItems(StringUtil.getInt(str[0]));
					if(items ==null || items.size()==0)
					{
						a =false;
						break;
					}
				}
				if(a)
				{
					list.add(com);
				}
			}
		}
		return list;
	}
	
	public static HashMap<Integer, ComposeData> getComposeDataMap(int type ,PlayerInfo pi)
	{
		HashMap<Integer, ComposeData> comMap =new HashMap<Integer, ComposeData>();
		for(ComposeData comData : data.values())
		{
			if(comData.style==0)//自动开启
			{
				if(comData.type ==type)
				{
					comMap.put(comData.composite, comData);
				}
			}
			else if(comData.style==1)//等级解锁
			{
				if(comData.unlockmission<=pi.player.getLevel() && comData.type ==type)
				{
					comMap.put(comData.composite, comData);
				}
			}
			else if(comData.style==2)//关卡解锁
			{
				if(comData.unlockmission<=pi.player.getMissionId() && comData.type==type)
				{
					comMap.put(comData.composite, comData);
				}
			}
			else if(comData.style==3)
			{
				boolean a=true;
				List<String> needs =comData.material_num;
				for(int i=0;i<needs.size();i++)
				{
					String [] str =needs.get(i).split("-");
					List<Item> list =pi.getTargetItems(StringUtil.getInt(str[0]));
					if(list ==null || list.size()==0)
					{
						a =false;
						break;
					}
				}
				if(a)
				{
					comMap.put(comData.composite, comData);
				}
			}
		}
		return comMap;
	}

}
