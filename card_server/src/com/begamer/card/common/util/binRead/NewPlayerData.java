package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NewPlayerData implements PropertyReader {
	public int id;
	public int type;
	public int goodId;
	public int position;

	private static HashMap<Integer, NewPlayerData> data = new HashMap<Integer, NewPlayerData>();
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static NewPlayerData getData(int id)
	{
		return data.get(id);
	}

	@Override
	public void parse(String[] ss)
	{
	}
	
	public static List<NewPlayerData> getAllNewPlayerData()
	{
		List<NewPlayerData> list = new ArrayList<NewPlayerData>();
		for(NewPlayerData nData : data.values())
		{
			list.add(nData);
		}
		return list;
	}
}
