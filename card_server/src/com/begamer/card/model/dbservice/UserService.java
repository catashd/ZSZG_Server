package com.begamer.card.model.dbservice;

import java.util.List;

import com.begamer.card.model.pojo.User;

/**
 * 
 * @ClassName: UserService
 * @Description: TODO 数据层接口user表
 * @author gs
 * @date Nov 10, 2011 4:04:49 PM
 * 
 */
public interface UserService {
	/**
	 * 
	 * @Title: getUserByNameAndPassword
	 * @Description: TODO 通过用户名，密码查找用户
	 */
	public User getUserByNameAndPassword(String name, String password);

	/**
	 * 
	 * @Title: read
	 * @Description: TODO
	 */
	public User read(Integer userid);

	/**
	 * 
	 * @Title: create
	 * @Description: TODO
	 */
	public void create(User user);

	/**
	 * 
	 * @Title: delete
	 * @Description: TODO
	 */
	public void delete(User user);

	/**
	 * 
	 * @Title: update
	 * @Description: TODO
	 */
	public void update(User user);

	/**
	 * 
	 * @Title: getUserByName
	 * @Description: TODO
	 */
	public User getUserByName(String userName);

	public List<User> list();
	

}
