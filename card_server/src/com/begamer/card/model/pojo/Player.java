package com.begamer.card.model.pojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.cache.Cache;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.Constant;
import com.begamer.card.common.util.GmSpecialMailUtil;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.AchievementData;
import com.begamer.card.common.util.binRead.DailyTaskData;
import com.begamer.card.common.util.binRead.MissionData;
import com.begamer.card.common.util.binRead.PlayerData;
import com.begamer.card.common.util.binRead.RunData;
import com.begamer.card.common.util.binRead.RuneData;
import com.begamer.card.common.util.binRead.RunnerData;
import com.begamer.card.common.util.binRead.VipData;
import com.begamer.card.common.util.binRead.VitalityData;
import com.begamer.card.log.PlayerLogger;

public class Player {
	private static final Logger playerLogger = PlayerLogger.logger;
	
	private int id;
	private int userId;
	private String platform;
	private String lastLogin;// "yyyy-MM-dd HH:mm:ss"
	private String name;
	private String head;
	private int level;
	private int curExp;
	private int extendCardNum;
	private int gold;
	private int crystal;
	private int crystalPay;// 充值所得钻石
	private int lotPort;// 水晶抽卡点数
	private int firstLot;// 1已抽,0未抽
	private int missionId;
	private String completeStar1;
	private String bouns1;// 领取过bonus为1，有bonus但未领取为2，没有bonus为0---（01012）
	private String times1;// 普通挑战次数
	private int missionId2;
	private String completeStar2;
	private String bouns2;// 领取过bonus为1，有bonus但未领取为2，没有bonus为0---（01012）
	private String times2;// 精英挑战次数
	private int iconId;// 援护合体技Id
	private int friend;// 友情值
	private long lastRestorePowerTime;// 上次自动回复体力时间
	// ===以下客户端不需要===//
	private int luckyNum;// 抽卡幸运值
	private int loginDayNum;// 登录天数
	/** 迷宫-位置-次数 **/
	private String maze;
	private int power;
	private String mazeFirstEntry;
	/** 上次0点刷新信息的日期:yyyy-MM-dd **/
	private String lastZeroDate;
	// 符文
	private int runeNum;// 符文值
	private String runeId;// 当前符文信息:x-xx-xx-xx-xx-xx-xx,遍数-第1图点亮个数-第2图点亮个数-第3图点亮个数-第4图点亮个数-第5图点亮个数-第6图点亮个数
	private String runeProAdd;// 符文成功率增加值:x-x-x-x-x-x
	/** 今日接收体力次数,0点刷新 **/
	private int powerTimes;
	
	// 符文临时数据,存库时会转换成string
	private int times;// 1,2,3,4
	private int[] pageNums;
	private int[] runeAdds;
	
	// 冥想
	private int imagination;
	
	// 活动副本
	private String FBnum1;
	private String FBnum2;
	
	// 成就及是否领取:10106%1-10201%2-10301%2-10334%2,0表示已完成但还未领取奖励,1表示已领取奖励,2表示还未完成
	private String achive;
	// 新手标志
	private int newPlayerType;
	// 总战斗力
	private int battlePower;
	// 本月签到次数
	private int signTimes;
	// 上次签到时间,yyyy-MM-dd HH:mm:ss
	private String lastSignTime;
	// 怒气上限
	private int maxEnergy;
	// vip等级
	private int vipLevel;
	private int vipCost;// 当前vip经验
	private int vipMonthType;// 是否月卡vip
	private int vipMonthDay;// 月卡vip剩余天数
	
	// 修改名字次数
	private int changeNameTime;
	// 解锁头像
	private int headIconUnlock;
	// 购买信息
	private int buyPowerTimes;// 购买体力次数
	private int buyGoldTimes;// 购买金币次数
	private int buyPkNumTimes;// 购买pvp进入次数
	private int buyPkCdTimes;// 购买pvp冷却时间
	private String buyPveEntryTimes1;// 购买普通关卡进入次数
	private String buyPveEntryTimes2;// 购买经营关卡进入次数
	private String buyVipGift;// 购买vip礼包记录 id-id
	// 商城黑市
	private String buyShopNum;// 购买商城物品每天次数 id&num-id&num
	private String buyShopNums;// 购买商城物品总次数 id&num-id&num
	private int buyRefreshTimes;// 购买黑市立即刷新次数
	private int buyShopBoxNum;// 购买黑市随机物品位置次数
	
	// 最后一次mission
	private int lastMissionId;
	private String logoutTime;
	/** 已经合成的物品 **/
	private String compose;
	private int composeEquipTimes;// 累计合成装备的次数
	
	private String lastResponseTime;
	/** 今日邀请好友战斗次数 **/
	private int inviteFriendTimes;
	
	// 每日任务完成度
	private String dailyTaskComplete;// 每日任务完成进度，任务id-完成次数num
	private String dailyTaskState;// 每个任务的状态，0未完成，1完成未领奖励，2完成并且已经领取奖励
	
	/** 活跃度礼包 **/
	private int active;// 活跃度
	private String activeState;// 每个活跃度段的领取情况0未达到领取条件1已达到未领取2已领取
	/** 竞技场 **/
	private int pvpHonor;// 竞技场荣誉点数
	private int pvpShopRefreshTimes;// 竞技场商城货币刷新次数
	private String pvpShop;// 竞技场商城道具
	private long lastPvpShopRefreshTime;// 上一次系统刷新时间
	private int diamond;// 金刚心
	private long lastFreeDrawTime;//上次免费抽卡时间
	
	private String lineGift;// x-0表示可以领取x礼包 x-1表示x礼包已经领取过
	/** 购买好友上限次数 **/
	private int buyFriendTimes;
	/** 购买背包上限次数 **/
	private int buyBagTimes;
	
	private int kopoint;// ko积分
	private String koExchange;// ko已经兑换过的id,id,id
	
	private String exchange;// 兑换活动所兑换的次数，格式:兑换ID-兑换次数,兑换ID-兑换次数
	private String rechargeInfo;// 记录首冲双倍信息
	
	private String allSelectId;// 背包出售全选设定 类型1&类型2 0金币卡 1-6:1-6星卡
	
	private String blackMarketRefreshTime;// 黑市刷新时间
	private String blackMarkets;// id-次数&id-次数
	private String lineGiftTime;
	private String loginDayAward; // 已经领取的七天有礼 id&id
	private int cornucopia;// 聚宝盆id,id
	private String owncards;// 得到过的卡牌id&id
	private String unitskills;// 解锁的合体技id&id
	private String levelgift;// 已经领取的等级奖励 id&id
	private int cornucopiaMail;// 聚宝盆结束两天后，反馈奖励邮件已经发送的数量
	private int sackFirst;// 首次开宝物袋
	private int cornucopiaCrystal;// 聚宝盆活动期间充值的钻石数
	private String battlepowerSpeciamail;// 战力达到n发邮件记录
	private String kopointSpeciamail;// ko积分达到n发邮件
	private int firstPayType;// 首冲标识
	private int bloodBuffNum;// 购买血瓶次数
	private String mazeBossDrop;// 迷宫boss掉落过的物品
	// 迷宫id-itemId-itemId...&迷宫id-itemid....
	private String mazeWish;// 迷宫许愿记录,迷宫id-itemid&迷宫id-itemid
	
	private String lottoTimes;//转盘抽奖次数  档位id-num&id-num
	private String lottoReward;//转盘抽奖n次奖励领取标识。id-type&id-type(0未领取，1领取过)
	private String lottoTurnTimes;//转盘转动次数     档位-转动次数&档位-次数
	private String LastLottoAward;//转盘上次获得的物品   档位id-类型-物品id,数量&档位id-类型-id,num
	
	private int mnum;//冥想领奖---冥想次数
	private int mid;//冥想领奖----奖励id
	private int mrandom;//冥想任务步数0随机其他标示第几步
	
	public void addExp(int exp)
	{
		exp = (int) (Cache.getExpMul() * exp);
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得经验|" + exp);
		while (exp > 0)
		{
			PlayerData pd = PlayerData.getData(level + 1);
			if (pd != null)
			{
				if (exp + curExp >= pd.exp)
				{
					exp = exp + curExp - pd.exp;
					level += 1;
					// 玩家等级达到多少发送奖励
					GmSpecialMailUtil.getInstance().playerLevelSpecialMail(getId(), getName(), getLevel());
					
					power += pd.recover;
					curExp = 0;
					updateAchieve(1, level + "");
					PkRank pr = Cache.getInstance().getRankById(id);
					if (pr != null)
					{
						pr.setPlayerLevel(level);
					}
				}
				else
				{
					curExp += exp;
					exp = 0;
				}
			}
			else
			{
				exp = 0;
			}
		}
	}
	
	public void addGold(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得金币|" + num);
		if (num > 0 && gold < Constant.MaxGold)
		{
			gold += num;
			if (gold > Constant.MaxGold || gold < 0)
			{
				gold = Constant.MaxGold;
			}
		}
	}
	
	public void removeGold(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|消耗金币|" + num);
		if (num > 0 && gold > 0 && gold >= num)
		{
			gold -= num;
		}
	}
	
	public void addCrystal(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得钻石|" + num);
		if (num > 0 && crystal < Constant.MaxCrystal)
		{
			crystal += num;
			if (crystal > Constant.MaxCrystal || crystal < 0)
			{
				crystal = Constant.MaxCrystal;
			}
		}
	}
	
	public void addCrystalPay(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得充值钻石|" + num);
		if (num > 0 && crystalPay < Constant.MaxCrystal)
		{
			crystalPay += num;
			if (crystalPay > Constant.MaxCrystal || crystalPay < 0)
			{
				crystalPay = Constant.MaxCrystal;
			}
		}
	}
	
	/** 删除钻石,先删除普通钻石,再删除充值钻石 **/
	public int[] removeCrystal(int num)
	{
		int[] result = { 0, 0 };
		if (num > 0 && getTotalCrystal() >= num)
		{
			if (crystal >= num)
			{
				crystal -= num;
				playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|消耗普通钻石|" + num);
				result[0] = num;
			}
			else
			{
				playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|消耗普通钻石|" + crystal);
				playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|消耗充值钻石|" + (num - crystal));
				result[0] = crystal;
				result[1] = num - crystal;
				crystalPay -= num - crystal;
				crystal = 0;
			}
			// 消耗钻石发送奖励
			GmSpecialMailUtil.getInstance().saveConsumeCrystalLog(getId(), getName(), getLevel(), num);
		}
		return result;
	}
	
	/** 增加友情值 **/
	public void addFriendValue(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得友情值|" + num);
		if (num > 0 && friend < Constant.MaxFriendValue)
		{
			friend += num;
			if (friend > Constant.MaxFriendValue || friend < 0)
			{
				friend = Constant.MaxFriendValue;
			}
		}
	}
	
	public void removeFriendValue(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|消耗友情值|" + num);
		if (num > 0 && friend > 0 && friend >= num)
		{
			friend -= num;
		}
	}
	
	/** 增加符文值 **/
	public void addRuneNum(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得符文|" + num);
		if (num > 0 && runeNum < Constant.MaxRuneNum)
		{
			runeNum += num;
			if (runeNum > Constant.MaxRuneNum || runeNum < 0)
			{
				runeNum = Constant.MaxRuneNum;
			}
		}
	}
	
	public void removeRuneNum(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|消耗符文值|" + num);
		if (num > 0 && runeNum > 0 && runeNum >= num)
		{
			runeNum -= num;
		}
	}
	
	public void addRunePro(int page, int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得符文成功率增加值|" + num);
		if (num > 0 && runeAdds[page - 1] < 100)
		{
			runeAdds[page - 1] += num;
			if (runeAdds[page - 1] > 100)
			{
				runeAdds[page - 1] = 100;
			}
		}
	}
	
	public void addPower(int num, boolean limitMax)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得体力|" + num);
		if (num > 0)
		{
			power += num;
			if (limitMax)
			{
				if (power > Constant.MaxPower || power < 0)
				{
					power = Constant.MaxPower;
				}
			}
		}
	}
	
	public void removePower(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|消耗体力|" + num);
		if (num > 0 && power >= num)
		{
			power -= num;
		}
	}
	
	public void addInviteTimes()
	{
		inviteFriendTimes++;
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|邀请好友战斗次数增加1");
	}
	
	/** 增加vip充值点 **/
	public void addVipCost(int cost)
	{
		vipCost = vipCost + cost;
		vipLevel = VipData.getVipLevel(vipCost, platform);
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得vip充值点|" + cost + "|总充值点|" + vipCost + "|vip等级|" + vipLevel + "|平台|" + platform);
	}
	
	public void updateVipLevel()
	{
		vipLevel = VipData.getVipLevel(vipCost, platform);
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public int getUserId()
	{
		return userId;
	}
	
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getHead()
	{
		return head;
	}
	
	public void setHead(String head)
	{
		this.head = head;
	}
	
	public int getLevel()
	{
		return level;
	}
	
	public void setLevel(int level)
	{
		this.level = level;
	}
	
	public int getCurExp()
	{
		return curExp;
	}
	
	public void setCurExp(int curExp)
	{
		this.curExp = curExp;
	}
	
	public int getExtendCardNum()
	{
		return extendCardNum;
	}
	
	public void setExtendCardNum(int extendCardNum)
	{
		this.extendCardNum = extendCardNum;
	}
	
	public int getGold()
	{
		return gold;
	}
	
	public void setGold(int gold)
	{
		this.gold = gold;
	}
	
	public int getTotalCrystal()
	{
		return crystal + crystalPay;
	}
	
	public int getCrystal()
	{
		return crystal;
	}
	
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	
	public int getMissionId()
	{
		return missionId;
	}
	
	public void setMissionId(int missionId)
	{
		this.missionId = missionId;
	}
	
	public int getMissionId2()
	{
		return missionId2;
	}
	
	public void setMissionId2(int missionId2)
	{
		this.missionId2 = missionId2;
	}
	
	public int getIconId()
	{
		return iconId;
	}
	
	public void setIconId(int iconId)
	{
		this.iconId = iconId;
	}
	
	public int getFriend()
	{
		return friend;
	}
	
	public void setFriend(int friend)
	{
		this.friend = friend;
	}
	
	public int getLuckyNum()
	{
		return luckyNum;
	}
	
	public void setLuckyNum(int luckyNum)
	{
		this.luckyNum = luckyNum;
	}
	
	public void addLuckyNum(int addValue)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得抽卡幸运值|" + addValue);
		if (addValue < 0)
		{
			return;
		}
		luckyNum += addValue;
		if (luckyNum > Constant.MaxLuckNum || luckyNum < 0)
		{
			luckyNum = Constant.MaxLuckNum;
		}
	}
	
	public int getLoginDayNum()
	{
		return loginDayNum;
	}
	
	public void setLoginDayNum(int loginDayNum)
	{
		this.loginDayNum = loginDayNum;
	}
	
	public void addLoginDayNum()
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|登陆天数增加");
		this.loginDayNum++;
		if (loginDayNum > Constant.MaxLoginDayNum)
		{
			loginDayNum = Constant.MaxLoginDayNum;
		}
		addLuckyNum(Constant.LoginLucky);
	}
	
	public String getMaze()
	{
		return maze;
	}
	
	public void setMaze(String maze)
	{
		this.maze = maze;
	}
	
	public int getPower()
	{
		return power;
	}
	
	public void setPower(int power)
	{
		this.power = power;
	}
	
	public int getRuneNum()
	{
		return runeNum;
	}
	
	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}
	
	public String getRuneId()
	{
		return runeId;
	}
	
	public void setRuneId(String runeId)
	{
		this.runeId = runeId;
	}
	
	public String getRuneProAdd()
	{
		return runeProAdd;
	}
	
	public void setRuneProAdd(String runeProAdd)
	{
		this.runeProAdd = runeProAdd;
	}
	
	public String getLastZeroDate()
	{
		return lastZeroDate;
	}
	
	public void setLastZeroDate(String lastZeroDate)
	{
		this.lastZeroDate = lastZeroDate;
	}
	
	public int getImagination()
	{
		return imagination;
	}
	
	public void setImagination(int imagination)
	{
		this.imagination = imagination;
	}
	
	public String getFBnum1()
	{
		return FBnum1;
	}
	
	public void setFBnum1(String bnum1)
	{
		FBnum1 = bnum1;
	}
	
	public int getNewPlayerType()
	{
		return newPlayerType;
	}
	
	public void setNewPlayerType(int newPlayerType)
	{
		this.newPlayerType = newPlayerType;
	}
	
	public int getChangeNameTime()
	{
		return changeNameTime;
	}
	
	public void setChangeNameTime(int changeNameTime)
	{
		this.changeNameTime = changeNameTime;
	}
	
	public int getHeadIconUnlock()
	{
		return headIconUnlock;
	}
	
	public void setHeadIconUnlock(int headIconUnlock)
	{
		this.headIconUnlock = headIconUnlock;
	}
	
	public void init()
	{
		// 初始化符文
		initRune();
	}
	
	private void initRune()
	{
		String[] ss = runeId.split("-");// 当前符文信息:x-xx-xx-xx-xx-xx-xx,遍数-第1图点亮个数-第2图点亮个数-第3图点亮个数-第4图点亮个数-第5图点亮个数-第6图点亮个数
		times = StringUtil.getInt(ss[0]);
		pageNums = new int[Constant.RunePageLength];
		for (int k = 0; k < pageNums.length; k++)
		{
			pageNums[k] = StringUtil.getInt(ss[k + 1]);
		}
		ss = runeProAdd.split("-");
		runeAdds = new int[Constant.RunePageLength];
		for (int k = 0; k < runeAdds.length; k++)
		{
			runeAdds[k] = StringUtil.getInt(ss[k]);
		}
	}
	
	public void build()
	{
		// 组建符文
		buildRune();
		buildRuneAdd();
	}
	
	private synchronized void buildRune()
	{
		// 符文
		runeId = "";
		for (int k = 0; k < pageNums.length; k++)
		{
			runeId += "-" + pageNums[k];
		}
		runeId = times + "" + runeId;
	}
	
	private synchronized void buildRuneAdd()
	{
		// 符文成功率增加值
		runeProAdd = "";
		for (int k = 0; k < runeAdds.length; k++)
		{
			runeProAdd += runeAdds[k] + "-";
		}
		runeProAdd = runeProAdd.substring(0, runeProAdd.length() - 1);
	}
	
	/**
	 * 获取本页下一个可点亮的符文 lt@2014-2-24 上午09:33:03
	 * 
	 * @param page
	 *            页数
	 * @return
	 */
	public RuneData getNextRune(int page)
	{
		RuneData rd = RuneData.getNextData(times, page, pageNums[page - 1]);
		return rd;
	}
	
	/**
	 * 设置符文 lt@2014-2-24 上午10:27:55
	 * 
	 * @param page
	 *            页数
	 * @param num
	 *            本页已经点亮的个数
	 */
	public void setRune(int page)
	{
		pageNums[page - 1]++;
	}
	
	/**
	 * 本次的符文是否全点亮 lt@2014-2-24 上午10:34:07
	 * 
	 * @return
	 */
	public boolean thisTimesFul()
	{
		for (int k = 0; k < pageNums.length; k++)
		{
			if (getNextRune(k + 1) != null)
			{
				return false;
			}
		}
		return true;
	}
	
	public void updateRune()
	{
		if (times < 4)
		{
			times++;
			
			for (int k = 0; k < pageNums.length; k++)
			{
				pageNums[k] = 0;
			}
		}
	}
	
	public String getRune()
	{
		buildRune();
		return runeId;
	}
	
	public int getRuneAdd(int page)
	{
		return runeAdds[page - 1];
	}
	
	public void resetRuneAdd(int page)
	{
		runeAdds[page - 1] = 0;
	}
	
	public String getRuneAdd()
	{
		buildRuneAdd();
		return runeProAdd;
	}
	
	public void setLastLogin(String lastLogin)
	{
		this.lastLogin = lastLogin;
	}
	
	public String getLastLogin()
	{
		return lastLogin;
	}
	
	public void setPowerTimes(int powerTimes)
	{
		this.powerTimes = powerTimes;
	}
	
	public int getPowerTimes()
	{
		return powerTimes;
	}
	
	public void addPowerTime()
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|接收体力次数增加一次");
		powerTimes++;
		if (powerTimes > Cache.maxPowerTimes)
		{
			powerTimes = Cache.maxPowerTimes;
		}
	}
	
	public void setLastRestorePowerTime(long lastRestorePowerTime)
	{
		this.lastRestorePowerTime = lastRestorePowerTime;
	}
	
	public long getLastRestorePowerTime()
	{
		return lastRestorePowerTime;
	}
	
	public int getTimes()
	{
		return times;
	}
	
	public void setTimes(int times)
	{
		this.times = times;
	}
	
	public int[] getPageNums()
	{
		return pageNums;
	}
	
	public void setPageNums(int[] pageNums)
	{
		this.pageNums = pageNums;
	}
	
	public int[] getRuneAdds()
	{
		return runeAdds;
	}
	
	public void setRuneAdds(int[] runeAdds)
	{
		this.runeAdds = runeAdds;
	}
	
	public String getCompleteStar1()
	{
		return completeStar1;
	}
	
	public void setCompleteStar1(String completeStar1)
	{
		this.completeStar1 = completeStar1;
	}
	
	public String getBouns1()
	{
		return bouns1;
	}
	
	public void setBouns1(String bouns1)
	{
		this.bouns1 = bouns1;
	}
	
	public String getCompleteStar2()
	{
		return completeStar2;
	}
	
	public void setCompleteStar2(String completeStar2)
	{
		this.completeStar2 = completeStar2;
	}
	
	public String getBouns2()
	{
		return bouns2;
	}
	
	public void setBouns2(String bouns2)
	{
		this.bouns2 = bouns2;
	}
	
	public String getFBnum2()
	{
		return FBnum2;
	}
	
	public void setFBnum2(String bnum2)
	{
		FBnum2 = bnum2;
	}
	
	/** 更新成就 **/
	public void updateAchieve()
	{
		// 更新成就
		updateAchieve(1, level + "");
		updateAchieve(2, missionId + "");
		List<String> conditions = MissionData.getPreMapZoneMissionType(missionId);
		for (String condition : conditions)
		{
			updateAchieve(3, condition);
		}
		if (missionId2 != 0)
		{
			updateAchieve(2, missionId2 + "");
			conditions = MissionData.getPreMapZoneMissionType(missionId2);
			for (String condition : conditions)
			{
				updateAchieve(3, condition);
			}
		}
		updateAchieve(4, null);
		PlayerInfo pi = Cache.getInstance().getPlayerInfo(id);
		List<Card> cards = pi.getCards();
		// 更新成就 突破card
		List<AchievementData> ads = AchievementData.getAchievementByType(11);
		for (int j = 0; j < ads.size(); j++)
		{
			AchievementData ad = ads.get(j);
			String req = ad.request;
			String[] result = req.split(",");
			int num = 0;
			int bnum = StringUtil.getInt(result[1]);
			for (int i = 0; i < cards.size(); i++)
			{
				if (cards.get(i).getBreakNum() == bnum)
				{
					num++;
				}
			}
			pi.player.updateAchieve(11, num + "," + bnum);
		}
		// 等级 card
		for (int j = 0; j < cards.size(); j++)
		{
			updateAchieve(13, cards.get(j).getLevel() + "");
		}
		
		// 更新成就 x星card卡x张
		HashMap<Integer, Integer> cmap = new HashMap<Integer, Integer>();
		cmap = pi.getCardNumMap();
		List<AchievementData> ads1 = AchievementData.getAchievementByType(6);
		for (AchievementData ad : ads1)
		{
			String[] temp = ad.request.split(",");
			if (temp[0] != null && temp[0].length() > 0)
			{
				if (cmap.get(StringUtil.getInt(temp[0])) == null)
				{
					updateAchieve(6, temp[0] + "," + 0);
				}
				else
				{
					updateAchieve(6, temp[0] + "," + cmap.get(StringUtil.getInt(temp[0])));
				}
			}
		}
		// 更新成就 xcard张n1卡n2卡
		List<AchievementData> ads2 = AchievementData.getAchievementByType(5);
		HashMap<Integer, Card> map1 = pi.getCardMap();
		for (AchievementData ad : ads2)
		{
			String[] temp = ad.request.split(",");
			if (cards.size() > (temp.length - 1) && temp.length > 1)
			{
				int n = 0;
				String con = "";
				for (int i = 1; i < temp.length; i++)
				{
					if (map1.containsKey(StringUtil.getInt(temp[i])))
					{
						
						n++;
						if (con == null || con.length() == 0)
						{
							con = temp[i];
						}
						else
						{
							con = con + "," + temp[i];
						}
					}
				}
				updateAchieve(5, n + "," + con);
			}
		}
		
		// 更新成就skill x星卡x张
		HashMap<Integer, Integer> smap = new HashMap<Integer, Integer>();
		smap = pi.getSkillNumMap();
		List<AchievementData> ads3 = AchievementData.getAchievementByType(7);
		for (AchievementData ad : ads3)
		{
			String[] temp = ad.request.split(",");
			if (temp[0] != null && temp[0].length() > 0)
			{
				if (smap.get(StringUtil.getInt(temp[0])) == null)
				{
					updateAchieve(7, temp[0] + "," + 0);
				}
				else
				{
					updateAchieve(7, temp[0] + "," + smap.get(StringUtil.getInt(temp[0])));
				}
			}
		}
		// 更新成就passiveskill x星卡x张
		HashMap<Integer, Integer> pmap = new HashMap<Integer, Integer>();
		pmap = pi.getPskillNumMap();
		List<AchievementData> ads4 = AchievementData.getAchievementByType(8);
		for (AchievementData ad : ads4)
		{
			String[] temp = ad.request.split(",");
			if (temp[0] != null && temp[0].length() > 0)
			{
				if (pmap.get(StringUtil.getInt(temp[0])) == null)
				{
					updateAchieve(8, temp[0] + "," + 0);
				}
				else
				{
					updateAchieve(8, temp[0] + "," + pmap.get(StringUtil.getInt(temp[0])));
				}
			}
		}
		
		// 更新成就equip x星卡x张
		HashMap<Integer, Integer> emap = new HashMap<Integer, Integer>();
		emap = pi.getEquipNumMap();
		List<AchievementData> ads5 = AchievementData.getAchievementByType(9);
		for (AchievementData ad : ads5)
		{
			String[] temp = ad.request.split(",");
			if (temp[0] != null && temp[0].length() > 0)
			{
				if (emap.get(StringUtil.getInt(temp[0])) == null)
				{
					updateAchieve(9, temp[0] + "," + 0);
				}
				else
				{
					updateAchieve(9, temp[0] + "," + emap.get(StringUtil.getInt(temp[0])));
				}
			}
		}
		// 更新成就 好友个数
		updateAchieve(10, pi.getFriends().size() + "");
		// 更新成就 符文
		String runeId = getRuneId();
		if (runeId != null && runeId.length() > 0)
		{
			String[] ss = runeId.split("-");
			int num = 0;
			for (int i = 1; i < ss.length; i++)
			{
				num = StringUtil.getInt(ss[i]) + num;
			}
			num = num + (StringUtil.getInt(ss[0]) - 1) * 72;
			updateAchieve(14, num + "");
		}
		// 更新成就 合成物品个数
		updateAchieve(15, composeEquipTimes + "");
		// 签到次数
		updateAchieve(16, signTimes + "");
	}
	
	/**
	 * 更新成就 lt@2014-3-25 下午02:10:34
	 * 
	 * @param type
	 *            1-人物等级,2-推图,3-3星关卡,4-BONUS
	 * @param condition
	 *            type1,type2使用
	 * @param condition2
	 *            type3使用
	 */
	public void updateAchieve(int type, String condition)
	{
		String[] ss = achive.split("-");
		String result = "";
		for (int i = 0; i < ss.length; i++)
		{
			String info = ss[i];
			// 已领取奖励成就可以往下更新
			if (ss[i].endsWith("1") || ss[i].endsWith("2"))
			{
				String[] infos = info.split("%");
				int achieve = StringUtil.getInt(infos[0]);
				String temp = updateAchieve(achieve, type, condition, StringUtil.getInt(infos[1]));
				result += temp + "-";
			}
			else
			{
				result += info + "-";
			}
		}
		if (!result.equals(""))
		{
			result = result.substring(0, result.length() - 1);
		}
		achive = result;
	}
	
	/** 更新成就 **/
	private String updateAchieve(int achieve, int type, String condition, int type2)
	{
		String result = "";
		List<AchievementData> ads = new ArrayList<AchievementData>();
		if (type2 == 1)
		{
			ads.addAll(AchievementData.getNextAchieveMents(achieve));
		}
		else
		{
			ads.add(AchievementData.getData(achieve));
		}
		if (ads.size() == 0)
		{
			return achieve + "%1";
		}
		for (AchievementData ad : ads)
		{
			if (ad.disable == 0)
			{
				String temp = updateAchieveMentSub(ad, achieve, type, condition);
				if (temp != null)
				{
					result += temp + "%0-";
				}
				else
				{
					result += ad.id + "%2-";
				}
			}
		}
		if (!result.equals(""))
		{
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}
	
	private String updateAchieveMentSub(AchievementData ad, int achieve, int type, String condition)
	{
		if (ad.disable == 0)
		{
			if (ad.type != type)
			{
				return null;
			}
			switch (ad.type)
			{
				case 1:
					if (StringUtil.getInt(condition) >= StringUtil.getInt(ad.request))
					{
						return ad.id + "";
					}
				case 2:
					int condi = StringUtil.getInt(condition);
					int requ = StringUtil.getInt(ad.request);
					MissionData missionD1 = MissionData.getMissionData(condi);
					MissionData missionD2 = MissionData.getMissionData(requ);
					if (missionD1 == null || missionD2 == null)
					{
						return null;
					}
					else
					{
						if (missionD1.missiontype == missionD2.missiontype)
						{
							if (condi >= requ)
							{
								return ad.id + "";
							}
						}
					}
//					if (condi / 10000 == requ / 10000 && condi >= requ)
//					{
//						return ad.id + "";
//					}
					break;
				case 3:// 此区域中所有小关都达到3星
					if (ad.request.equals(condition))
					{
						String[] ss = condition.split(",");
						int map = StringUtil.getInt(ss[0]);
						int zone = StringUtil.getInt(ss[1]);
						int missionType = StringUtil.getInt(ss[2]);
						int[] sequences = MissionData.getSequences(map, zone, missionType);
						if (missionType == 1)
						{
							if (completeStar1.length() < sequences[1])
							{
								return null;
							}
							for (int i = sequences[0]; i <= sequences[1]; i++)
							{
								if (completeStar1.charAt(i - 1) != '3')
								{
									return null;
								}
							}
							return ad.id + "";
						}
						else if (missionType == 2)
						{
							if (completeStar2.length() < sequences[1])
							{
								return null;
							}
							for (int i = sequences[0]; i <= sequences[1]; i++)
							{
								if (completeStar2.charAt(i - 1) != '3')
								{
									return null;
								}
							}
							return ad.id + "";
						}
					}
					break;
				case 4:// bonus总数达到一定数量
					int num = 0;
					for (int i = 0; i < bouns1.length(); i++)
					{
						if (bouns1.charAt(i) == '1')
						{
							num++;
						}
					}
					for (int i = 0; i < bouns2.length(); i++)
					{
						if (bouns2.charAt(i) == '1')
						{
							num++;
						}
					}
					if (num >= StringUtil.getInt(ad.request))
					{
						return ad.id + "";
					}
					break;
				case 5:// 孙悟空，猪八戒，沙僧
					String request = ad.request;
					String[] temp = request.split(",");
					String[] con = condition.split(",");
					HashMap<String, String> map1 = new HashMap<String, String>();
					for (int i = 1; i < con.length; i++)
					{
						if (!map1.containsKey(con[i]))
						{
							map1.put(con[i], con[i]);
						}
					}
					if (StringUtil.getInt(temp[0]) == StringUtil.getInt(con[0]))
					{
						for (int j = 1; j < temp.length; j++)
						{
							if (!map1.containsKey(temp[j]))
							{
								return null;
							}
						}
						return ad.id + "";
					}
					break;
				case 6:// 五星卡card
					String request1 = ad.request;
					String[] temp1 = request1.split(",");
					String[] con1 = condition.split(",");
					if (StringUtil.getInt(temp1[0]) == StringUtil.getInt(con1[0]) && StringUtil.getInt(temp1[1]) <= StringUtil.getInt(con1[1]))
					{
						return ad.id + "";
					}
					break;
				case 7:// skill
					String request2 = ad.request;
					String[] temp2 = request2.split(",");
					String[] con2 = condition.split(",");
					if (StringUtil.getInt(temp2[0]) == StringUtil.getInt(con2[0]) && StringUtil.getInt(temp2[1]) <= StringUtil.getInt(con2[1]))
					{
						return ad.id + "";
					}
					break;
				case 8:// passiveskill
					String request3 = ad.request;
					String[] temp3 = request3.split(",");
					String[] con3 = condition.split(",");
					if (StringUtil.getInt(temp3[0]) == StringUtil.getInt(con3[0]) && StringUtil.getInt(temp3[1]) <= StringUtil.getInt(con3[1]))
					{
						return ad.id + "";
					}
					break;
				case 9:// equip
					String request4 = ad.request;
					String[] temp4 = request4.split(",");
					String[] con4 = condition.split(",");
					if (StringUtil.getInt(temp4[0]) == StringUtil.getInt(con4[0]) && StringUtil.getInt(temp4[1]) <= StringUtil.getInt(con4[1]))
					{
						return ad.id + "";
					}
					break;
				case 10:// friend
					if (StringUtil.getInt(condition) >= StringUtil.getInt(ad.request))
					{
						return ad.id + "";
					}
					break;
				case 11:// 突破
					String request11 = ad.request;
					String[] temp11 = request11.split(",");
					String[] con11 = condition.split(",");
					if (StringUtil.getInt(con11[0]) >= StringUtil.getInt(temp11[0]))
					{
						if (StringUtil.getInt(con11[1]) == StringUtil.getInt(temp11[1]))
						{
							return ad.id + "";
						}
					}
					break;
				case 12:// pk
					if (StringUtil.getInt(condition) >= StringUtil.getInt(ad.request))
					{
						return ad.id + "";
					}
					break;
				case 13:// 卡牌升级
					if (StringUtil.getInt(condition) >= StringUtil.getInt(ad.request))
					{
						return ad.id + "";
					}
					break;
				case 14:// rune
					if (StringUtil.getInt(condition) >= StringUtil.getInt(ad.request))
					{
						return ad.id + "";
					}
					break;
				case 15:// 合成装备数量
					if (StringUtil.getInt(condition) >= StringUtil.getInt(ad.request))
					{
						return ad.id + "";
					}
					break;
				case 16:
					if (StringUtil.getInt(condition) >= StringUtil.getInt(ad.request))
					{
						return ad.id + "";
					}
					break;
			}
		}
		
		return null;
	}
	
	/** 领取成就奖励 **/
	public boolean getAchieveReward(int achieveId)
	{
		if (achive.indexOf(achieveId + "%0") < 0)
		{
			return false;
		}
		String replaceStr = "";
		List<AchievementData> list = AchievementData.getNextAchieveMents(achieveId);
		if (list.size() > 0)
		{
			for (AchievementData ad : list)
			{
				replaceStr += ad.id + "%2-";
			}
			if (!"".equals(replaceStr))
			{
				replaceStr = replaceStr.substring(0, replaceStr.length() - 1);
			}
		}
		else
		{
			replaceStr = achieveId + "%1";
		}
		achive = StringUtil.replaceStr(achive, achieveId + "%0", replaceStr);
		updateAchieve();
		return true;
	}
	
	public String getAchive()
	{
		return achive;
	}
	
	public void setAchive(String achive)
	{
		this.achive = achive;
	}
	
	public void addLotPort()
	{
		lotPort++;
	}
	
	public void setLotPort(int lotPort)
	{
		this.lotPort = lotPort;
	}
	
	public int getLotPort()
	{
		return lotPort;
	}
	
	public void setTimes1(String times1)
	{
		this.times1 = times1;
	}
	
	public String getTimes1()
	{
		return times1;
	}
	
	public void setTimes2(String times2)
	{
		this.times2 = times2;
	}
	
	public String getTimes2()
	{
		return times2;
	}
	
	public void clearTimes1()
	{
		String newTimes = "";
		for (int i = 0; i < times1.length(); i++)
		{
			newTimes += "0";
		}
		times1 = newTimes;
	}
	
	public void clearTimes2()
	{
		String newTimes = "";
		for (int i = 0; i < times2.length(); i++)
		{
			newTimes += "0";
		}
		times2 = newTimes;
	}
	
	public void clearBuyPveEntryTimes1()
	{
		String newTimes = "";
		for (int i = 0; i < buyPveEntryTimes1.length(); i++)
		{
			newTimes += "0";
		}
		buyPveEntryTimes1 = newTimes;
	}
	
	public void clearBuyPveEntryTimes2()
	{
		String newTimes = "";
		for (int i = 0; i < buyPveEntryTimes2.length(); i++)
		{
			newTimes += "0";
		}
		buyPveEntryTimes2 = newTimes;
	}
	
	public void setBattlePower(int battlePower)
	{
		this.battlePower = battlePower;
	}
	
	public int getBattlePower()
	{
		return battlePower;
	}
	
	public void setSignTimes(int signTimes)
	{
		this.signTimes = signTimes;
	}
	
	public int getSignTimes()
	{
		return signTimes;
	}
	
	public void addSignTimes()
	{
		signTimes++;
		updateAchieve(16, signTimes + "");
		
	}
	
	public void setLastSignTime(String lastSignTime)
	{
		this.lastSignTime = lastSignTime;
	}
	
	public String getLastSignTime()
	{
		return lastSignTime;
	}
	
	public void setMaxEnergy(int maxEnergy)
	{
		this.maxEnergy = maxEnergy;
	}
	
	public int getMaxEnergy()
	{
		return maxEnergy;
	}
	
	public void setVipLevel(int vipLevel)
	{
		this.vipLevel = vipLevel;
	}
	
	public int getVipLevel()
	{
		return vipLevel;
	}
	
	public int getBuyPowerTimes()
	{
		return buyPowerTimes;
	}
	
	public void setBuyPowerTimes(int buyPowerTimes)
	{
		this.buyPowerTimes = buyPowerTimes;
	}
	
	public int getBuyGoldTimes()
	{
		return buyGoldTimes;
	}
	
	public void setBuyGoldTimes(int buyGoldTimes)
	{
		this.buyGoldTimes = buyGoldTimes;
	}
	
	public int getBuyPkNumTimes()
	{
		return buyPkNumTimes;
	}
	
	public void setBuyPkNumTimes(int buyPkNumTimes)
	{
		this.buyPkNumTimes = buyPkNumTimes;
	}
	
	public int getBuyPkCdTimes()
	{
		return buyPkCdTimes;
	}
	
	public void setBuyPkCdTimes(int buyPkCdTimes)
	{
		this.buyPkCdTimes = buyPkCdTimes;
	}
	
	public int getLastMissionId()
	{
		return lastMissionId;
	}
	
	public void setLastMissionId(int lastMissionId)
	{
		this.lastMissionId = lastMissionId;
	}
	
	public void setLogoutTime(String logoutTime)
	{
		this.logoutTime = logoutTime;
	}
	
	public String getLogoutTime()
	{
		return logoutTime;
	}
	
	public String getCompose()
	{
		return compose;
	}
	
	public void setCompose(String compose)
	{
		this.compose = compose;
	}
	
	public String getBuyPveEntryTimes1()
	{
		return buyPveEntryTimes1;
	}
	
	public void setBuyPveEntryTimes1(String buyPveEntryTimes1)
	{
		this.buyPveEntryTimes1 = buyPveEntryTimes1;
	}
	
	public String getBuyPveEntryTimes2()
	{
		return buyPveEntryTimes2;
	}
	
	public void setBuyPveEntryTimes2(String buyPveEntryTimes2)
	{
		this.buyPveEntryTimes2 = buyPveEntryTimes2;
	}
	
	public String getMazeFirstEntry()
	{
		return mazeFirstEntry;
	}
	
	public void setMazeFirstEntry(String mazeFirstEntry)
	{
		this.mazeFirstEntry = mazeFirstEntry;
	}
	
	public String getLastResponseTime()
	{
		return lastResponseTime;
	}
	
	public void setLastResponseTime(String lastResponseTime)
	{
		this.lastResponseTime = lastResponseTime;
	}
	
	public void setInviteFriendTimes(int inviteFriendTimes)
	{
		this.inviteFriendTimes = inviteFriendTimes;
	}
	
	public int getInviteFriendTimes()
	{
		return inviteFriendTimes;
	}
	
	public String getDailyTaskComplete()
	{
		return dailyTaskComplete;
	}
	
	public void setDailyTaskComplete(String dailyTaskComplete)
	{
		this.dailyTaskComplete = dailyTaskComplete;
	}
	
	public String getDailyTaskState()
	{
		return dailyTaskState;
	}
	
	public void setDailyTaskState(String dailyTaskState)
	{
		this.dailyTaskState = dailyTaskState;
	}
	
	public int getVipCost()
	{
		return vipCost;
	}
	
	public void setVipCost(int vipCost)
	{
		this.vipCost = vipCost;
	}
	
	public int getVipMonthType()
	{
		return vipMonthType;
	}
	
	public void setVipMonthType(int vipMonthType)
	{
		this.vipMonthType = vipMonthType;
	}
	
	public int getVipMonthDay()
	{
		return vipMonthDay;
	}
	
	public void setVipMonthDay(int vipMonthDay)
	{
		this.vipMonthDay = vipMonthDay;
	}
	
	public int getComposeEquipTimes()
	{
		return composeEquipTimes;
	}
	
	public void setComposeEquipTimes(int composeEquipTimes)
	{
		this.composeEquipTimes = composeEquipTimes;
	}
	
	public String getLineGift()
	{
		return lineGift;
	}
	
	public void setLineGift(String lineGift)
	{
		this.lineGift = lineGift;
	}
	
	public void addVipMonthDay(int vipMonthDay)
	{
		if (vipMonthDay > 0)
		{
			this.vipMonthDay += vipMonthDay;
			playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得月卡vip天数|" + vipMonthDay + "|总天数|" + this.vipMonthDay);
		}
	}
	
	public void decreaseVipMonthDay()
	{
		if (vipMonthDay > 0)
		{
			vipMonthDay--;
			playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|减少月卡vip天数|" + vipMonthDay);
		}
	}
	
	public String getPlatform()
	{
		return platform;
	}
	
	public void setPlatform(String platform)
	{
		this.platform = platform;
	}
	
	public int getKopoint()
	{
		return kopoint;
	}
	
	public void setKopoint(int kopoint)
	{
		this.kopoint = kopoint;
	}
	
	public void setBuyFriendTimes(int buyFriendTimes)
	{
		this.buyFriendTimes = buyFriendTimes;
	}
	
	public int getBuyFriendTimes()
	{
		return buyFriendTimes;
	}
	
	public void addBuyFriendTimes()
	{
		buyFriendTimes++;
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|增加购买好友上限次数|" + buyFriendTimes);
	}
	
	public void setBuyBagTimes(int buyBagTimes)
	{
		this.buyBagTimes = buyBagTimes;
	}
	
	public int getBuyBagTimes()
	{
		return buyBagTimes;
	}
	
	public void addBuyBagTimes()
	{
		buyBagTimes++;
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|增加购买背包上限次数|" + buyBagTimes);
	}
	
	public String getKoExchange()
	{
		return koExchange;
	}
	
	public void setKoExchange(String koExchange)
	{
		this.koExchange = koExchange;
	}
	
	public String getExchange()
	{
		return exchange;
	}
	
	public void setExchange(String exchange)
	{
		this.exchange = exchange;
	}
	
	public void setRechargeInfo(String rechargeInfo)
	{
		this.rechargeInfo = rechargeInfo;
	}
	
	public String getRechargeInfo()
	{
		return rechargeInfo;
	}
	
	public void setFirstLot(int firstLot)
	{
		this.firstLot = firstLot;
	}
	
	public int getFirstLot()
	{
		return firstLot;
	}
	
	public String getBuyVipGift()
	{
		return buyVipGift;
	}
	
	public void setBuyVipGift(String buyVipGift)
	{
		this.buyVipGift = buyVipGift;
	}
	
	public String getBuyShopNum()
	{
		return buyShopNum;
	}
	
	public void setBuyShopNum(String buyShopNum)
	{
		this.buyShopNum = buyShopNum;
	}
	
	public String getBuyShopNums()
	{
		return buyShopNums;
	}
	
	public void setBuyShopNums(String buyShopNums)
	{
		this.buyShopNums = buyShopNums;
	}
	
	public int getBuyRefreshTimes()
	{
		return buyRefreshTimes;
	}
	
	public void setBuyRefreshTimes(int buyRefreshTimes)
	{
		this.buyRefreshTimes = buyRefreshTimes;
	}
	
	public int getBuyShopBoxNum()
	{
		return buyShopBoxNum;
	}
	
	public void setBuyShopBoxNum(int buyShopBoxNum)
	{
		this.buyShopBoxNum = buyShopBoxNum;
	}
	
	public String getAllSelectId()
	{
		return allSelectId;
	}
	
	public void setAllSelectId(String allSelectId)
	{
		this.allSelectId = allSelectId;
	}
	
	public String getBlackMarketRefreshTime()
	{
		return blackMarketRefreshTime;
	}
	
	public void setBlackMarketRefreshTime(String blackMarketRefreshTime)
	{
		this.blackMarketRefreshTime = blackMarketRefreshTime;
	}
	
	public String getBlackMarkets()
	{
		return blackMarkets;
	}
	
	public void setBlackMarkets(String blackMarkets)
	{
		this.blackMarkets = blackMarkets;
	}
	
	public String getLineGiftTime()
	{
		return lineGiftTime;
	}
	
	public void setLineGiftTime(String lineGiftTime)
	{
		this.lineGiftTime = lineGiftTime;
	}
	
	public String getLoginDayAward()
	{
		return loginDayAward;
	}
	
	public void setLoginDayAward(String loginDayAward)
	{
		this.loginDayAward = loginDayAward;
	}
	
	public Integer getCornucopia()
	{
		return cornucopia;
	}
	
	public void setCornucopia(int cornucopia)
	{
		this.cornucopia = cornucopia;
	}
	
	public String getOwncards()
	{
		return owncards;
	}
	
	public void setOwncards(String owncards)
	{
		this.owncards = owncards;
	}
	
	public String getUnitskills()
	{
		return unitskills;
	}
	
	public void setUnitskills(String unitskills)
	{
		this.unitskills = unitskills;
	}
	
	public void setCrystalPay(int crystalPay)
	{
		this.crystalPay = crystalPay;
	}
	
	public int getCrystalPay()
	{
		return crystalPay;
	}
	
	public String getLevelgift()
	{
		return levelgift;
	}
	
	public void setLevelgift(String levelgift)
	{
		this.levelgift = levelgift;
	}
	
	public int getCornucopiaMail()
	{
		return cornucopiaMail;
	}
	
	public void setCornucopiaMail(int cornucopiaMail)
	{
		this.cornucopiaMail = cornucopiaMail;
	}
	
	public int getSackFirst()
	{
		return sackFirst;
	}
	
	public void setSackFirst(int sackFirst)
	{
		this.sackFirst = sackFirst;
	}
	
	public int getCornucopiaCrystal()
	{
		return cornucopiaCrystal;
	}
	
	public void setCornucopiaCrystal(int cornucopiaCrystal)
	{
		this.cornucopiaCrystal = cornucopiaCrystal;
	}
	
	public String getBattlepowerSpeciamail()
	{
		return battlepowerSpeciamail;
	}
	
	public void setBattlepowerSpeciamail(String battlepowerSpeciamail)
	{
		this.battlepowerSpeciamail = battlepowerSpeciamail;
	}
	
	public String getKopointSpeciamail()
	{
		return kopointSpeciamail;
	}
	
	public void setKopointSpeciamail(String kopointSpeciamail)
	{
		this.kopointSpeciamail = kopointSpeciamail;
	}
	
	public int getFirstPayType()
	{
		return firstPayType;
	}
	
	public void setFirstPayType(int firstPayType)
	{
		this.firstPayType = firstPayType;
	}
	
	public int getBloodBuffNum()
	{
		return bloodBuffNum;
	}
	
	public void setBloodBuffNum(int bloodBuffNum)
	{
		this.bloodBuffNum = bloodBuffNum;
	}
	
	public int getActive()
	{
		return active;
	}
	
	public void setActive(int active)
	{
		this.active = active;
	}
	
	/**
	 * 任务类型 根据任务类型取到最大能完成的次数,如果小于等于最大次数则根据数据表增加加活跃度
	 * 
	 * @param type
	 */
	public void addActive(int type, int amount)
	{
		String[] ss = dailyTaskComplete.split(",");
		for (int i = 0; i < ss.length; i++)
		{
			String[] s = ss[i].split("-");
			int taskId = Integer.valueOf(s[0]);
			int num = Integer.valueOf(s[1]);
			if (taskId == type)
			{
				DailyTaskData dtd = DailyTaskData.getDailyTaskData(taskId);
				if (null != dtd)
				{
					if (dtd.request >= num)
					{
						active += dtd.num * amount;
						updateActiveState();
					}
					else
					{
						if (dtd.request > num - amount)
						{
							active += dtd.num * (dtd.request - (num - amount));
							updateActiveState();
						}
					}
				}
			}
		}
	}
	
	public String getActiveState()
	{
		if (null == activeState || "".equals(activeState))
		{
			initActiveState();
		}
		return activeState;
	}
	
	public void setActiveState(String activeState)
	{
		this.activeState = activeState;
	}
	
	/**
	 * 初始化活跃度礼包领取状态
	 */
	public void initActiveState()
	{
		activeState = "";
		HashMap<Integer, VitalityData> allData = VitalityData.getAllData();
		List<VitalityData> list = new ArrayList<VitalityData>();
		for (VitalityData vd : allData.values())
		{
			if (null != vd)
			{
				list.add(vd);
			}
		}
		Collections.sort(list, new ActiveCompare());
		if (null != list)
		{
			for (VitalityData vd : list)
			{
				activeState += vd.vitality + "-" + 0 + ",";
			}
		}
		activeState = activeState.substring(0, activeState.length() - 1);
	}
	
	public void updateActiveState()
	{
		if (null == activeState || "".equals(activeState))
		{
			initActiveState();
		}
		
		String[] ss = activeState.split(",");
		for (int i = 0; i < ss.length; i++)
		{
			String[] s = ss[i].split("-");
			int at = Integer.valueOf(s[0]);
			int state = Integer.valueOf(s[1]);
			if (active >= at && state == 0)
			{
				String re = at + "-1";
				String reas = "";
				for (int j = 0; j < ss.length; j++)
				{
					if (j != i)
					{
						reas += ss[j] + ",";
					}
					else
					{
						reas += re + ",";
					}
				}
				activeState = reas.substring(0, reas.length() - 1);
			}
		}
	}
	
	/**
	 * 更新活跃度礼包领取状态
	 * 
	 * @param active
	 * @param type
	 */
	public void rewardActive(int active)
	{
		String[] ss = activeState.split(",");
		for (int i = 0; i < ss.length; i++)
		{
			String[] s = ss[i].split("-");
			int at = Integer.valueOf(s[0]);
			int state = Integer.valueOf(s[1]);
			if (active == at && state == 1)
			{
				String re = at + "-2";
				String reas = "";
				for (int j = 0; j < ss.length; j++)
				{
					if (j != i)
					{
						reas += ss[j] + ",";
					}
					else
					{
						reas += re + ",";
					}
				}
				activeState = reas.substring(0, reas.length() - 1);
			}
		}
	}
	
	/**
	 * 获取活跃礼包状态
	 * 
	 * @param active
	 * @return
	 */
	public int getActiveState(int active)
	{
		String[] ss = activeState.split(",");
		for (int i = 0; i < ss.length; i++)
		{
			String[] s = ss[i].split("-");
			int at = Integer.valueOf(s[0]);
			int state = Integer.valueOf(s[1]);
			if (active == at)
			{
				return state;
			}
		}
		return -1;
	}
	
	class ActiveCompare implements Comparator<VitalityData> {
		@Override
		public int compare(VitalityData o1, VitalityData o2)
		{
			return o1.vitality - o2.vitality;
		}
	}
	
	public String getMazeBossDrop()
	{
		return mazeBossDrop;
	}
	
	public void setMazeBossDrop(String mazeBossDrop)
	{
		this.mazeBossDrop = mazeBossDrop;
	}
	
	public String getMazeWish()
	{
		return mazeWish;
	}
	
	public void setMazeWish(String mazeWish)
	{
		this.mazeWish = mazeWish;
	}

	public String getLottoTimes() {
		return lottoTimes;
	}

	public void setLottoTimes(String lottoTimes) {
		this.lottoTimes = lottoTimes;
	}

	public String getLottoReward() {
		return lottoReward;
	}

	public void setLottoReward(String lottoReward) {
		this.lottoReward = lottoReward;
	}
	
	/**根据档位类型和档位条件增加抽奖次数**/
	public void addLottoTimes(int t,int num)
	{
		RunnerData rData =RunnerData.getRunner(t, num);
		if(rData !=null)
		{
			if(lottoTimes !=null && lottoTimes.length()>0)
			{
				String [] lotto =lottoTimes.split("&");
				boolean in=false;
				for(int i=0;i<lotto.length;i++)//判断当前档位是否已经存在抽奖次数
				{
					if(lotto[i] !=null &&lotto[i].length()>0)
					{
						String [] temp =lotto[i].split("-");
						if(StringUtil.getInt(temp[0])==rData.id)
						{
							lotto[i] =temp[0]+"-"+(StringUtil.getInt(temp[1])+rData.time);
							in =true;
							break;
						}
					}
				}
				if(!in)//不存在
				{
					lottoTimes =lottoTimes+"&"+rData.id+"-"+rData.time;
				}
				else//存在
				{
					String str ="";
					for(int i=0;i<lotto.length;i++)
					{
						if(str !=null && str.length()>0)
						{
							str =str+"&"+lotto[i];
						}
						else
						{
							str =lotto[i];
						}
					}
					lottoTimes =str;
				}
			}
			else
			{
				lottoTimes =rData.id+"-"+rData.time;
			}
			playerLogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+getId()+"|"+getName()+"|等级："+getLevel()+"|增加抽奖次数|"+rData.time);
		}
		
	}
	public int getPvpHonor()
	{
		return pvpHonor;
	}
	
	/**减少抽奖次数***/
	public void removeLottoTimes(int k,int num)
	{
		String [] lotto =lottoTimes.split("&");
		String str="";
		boolean flag = true;
		for(int i=0;i<lotto.length;i++)
		{
			String [] temp =lotto[i].split("-");
			if(StringUtil.getInt(temp[0])==k)
			{
				str = lottoTimes.replace(lotto[i], temp[0]+"-"+(StringUtil.getInt(temp[1])-1));
				flag = false;
				break;
			}
//			if(str !=null && str.length()>0)
//			{
//				str =str+"&"+lotto[i];
//			}
//			else
//			{
//				str =lotto[i];
//			}
		}
		if(flag){
			//
		}
		lottoTimes =str;
		playerLogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+getId()+"|"+getName()+"|等级："+getLevel()+"|减少抽奖次数|"+num);
	}
	
	
	
	public String getLottoTurnTimes() {
		return lottoTurnTimes;
	}

	public void setLottoTurnTimes(String lottoTurnTimes) {
		this.lottoTurnTimes = lottoTurnTimes;
	}
	
	public void addLottoTurnTimes(int type,int run ,int times)
	{
		HashMap<String, Integer> map =lottoTurnTimeMap();
		if(lottoTurnTimes !=null && lottoTurnTimes.length()>0)
		{
			if(map.containsKey(type+"-"+run))
			{
				String [] lottoTimes =lottoTurnTimes.split("&");
				String str ="";
				boolean flag = true;
				for(int i=0;i<lottoTimes.length;i++)
				{
					String [] temp =lottoTimes[i].split("-");
					if(StringUtil.getInt(temp[0])==type && StringUtil.getInt(temp[1])==run)
					{
						//lottoTimes[i]=temp[0]+"-"+temp[1]+"-"+(StringUtil.getInt(temp[2])+times);
						str = lottoTurnTimes.replace(lottoTimes[i], temp[0]+"-"+temp[1]+"-"+(StringUtil.getInt(temp[2])+times));
						flag = false;
						break;
					}
//					if(str !=null && str.length()>0)
//					{
//						str =str+"&"+lottoTimes[i];
//					}
//					else
//					{
//						str =lottoTimes[i];
//					}
				}
				if(flag){
					str+=lottoTurnTimes+"&"+type+"-"+run+"-"+times;
				}
				lottoTurnTimes =str;
			}
			else
			{
				lottoTurnTimes =lottoTurnTimes+"&"+type+"-"+run+"-"+times;
			}
		}
		else
		{
			lottoTurnTimes =type+"-"+run+"-"+times;
		}
	}
	
	/**每个档位的转动次数map    key:type-档位 value:转动次数**/
	public HashMap<String, Integer> lottoTurnTimeMap()
	{
		HashMap<String, Integer> map =new HashMap<String, Integer>();
		if(lottoTurnTimes !=null && lottoTurnTimes.length()>0)
		{
			String [] lottoTimes =lottoTurnTimes.split("&");
			for(int i=0;i<lottoTimes.length;i++)
			{
				String [] temp =lottoTimes[i].split("-");
				if(!map.containsKey(temp[0]+"-"+temp[1]))
				{
					map.put(temp[0]+"-"+temp[1], StringUtil.getInt(temp[2]));
				}
			}
		}
		return map;
	}
	
	/**转动次数获得物品map    key-id,value-标识**/
	public HashMap<Integer, Integer> getLottoRewardMap()
	{
		HashMap<Integer,Integer> map =new HashMap<Integer, Integer>();
		if(lottoReward!=null && lottoReward.length()>0)
		{
			String [] str =lottoReward.split("&");
			for(int i=0;i<str.length;i++)
			{
				String [] temp =str[i].split("-");
				if(!map.containsKey(StringUtil.getInt(temp[0])))
				{
					map.put(StringUtil.getInt(temp[0]), StringUtil.getInt(temp[1]));
				}
			}
		}
		return map;
	}
	
	
	/**更新转动n次奖励状态**/
	/**类型     档位**/
	public void updateLottoReward(int type,int run)
	{
		HashMap<String, Integer> lottoTurnTimeMap =lottoTurnTimeMap();
		HashMap<Integer, Integer> rewardMap =getLottoRewardMap();
		if(lottoReward !=null && lottoReward.length()>0)//已经解锁了转动n次奖励的，判断是否可以解锁下个奖励
		{
			
			String [] lottoR =lottoReward.split("&");
			if(lottoR !=null && lottoR.length>0)
			{
				for(int i=1;i<=RunData.getDataMap(type).size();i++)
				{
					RunData rData =RunData.getRunData(i);
					if(rData !=null)
					{
						int times=0;
						if(!rewardMap.containsKey(rData.id))
						{
							if(lottoTurnTimeMap.containsKey(rData.runtype+"-"+rData.run))
							{
								times =lottoTurnTimeMap.get(rData.runtype+"-"+rData.run);
								if(times>=rData.truntime)
								{
									lottoReward =lottoReward+"&"+rData.id+"-"+0;
									rewardMap.put(rData.id, 0);
								}
							}
						}
						
					}
				}
			}
		}
		else//暂未解锁任何奖励的，从第一个奖励开始判断
		{
			for(RunData rData :RunData.getDatasByType(type))
			{
				int times=0;
				if(!rewardMap.containsKey(rData.id))
				{
					if(lottoTurnTimeMap.containsKey(rData.runtype+"-"+rData.run))
					{
						times =lottoTurnTimeMap.get(rData.runtype+"-"+rData.run);
						if(times>=rData.truntime)
						{
							if(lottoReward !=null && lottoReward.length()>0)
							{
								lottoReward =lottoReward+"&"+rData.id+"-"+0;
							}
							else
							{
								lottoReward =rData.id+"-"+0;
							}
							rewardMap.put(rData.id, 0);
						}
					}
				}
			}
		}
	}

	public String getLastLottoAward() {
		return LastLottoAward;
	}

	public void setLastLottoAward(String lastLottoAward) {
		LastLottoAward = lastLottoAward;
	}
	public void setPvpHonor(int pvpHonor)
	{
		this.pvpHonor = pvpHonor;
	}
	
	public void addPvpHonor(int num)
	{
		pvpHonor += num;
		if (pvpHonor < 0)
		{
			pvpHonor = 0;
		}
	}
	
	public int getPvpShopRefreshTimes()
	{
		return pvpShopRefreshTimes;
	}
	
	public void setPvpShopRefreshTimes(int pvpShopRefreshTimes)
	{
		this.pvpShopRefreshTimes = pvpShopRefreshTimes;
	}
	
	public void addPvpShopRefreshTimes(int num)
	{
		pvpShopRefreshTimes += num;
	}
	
	public long getLastPvpShopRefreshTime()
	{
		return lastPvpShopRefreshTime;
	}
	
	public void setLastPvpShopRefreshTime(long lastPvpShopRefreshTime)
	{
		this.lastPvpShopRefreshTime = lastPvpShopRefreshTime;
	}
	
	public String getPvpShop()
	{
		return pvpShop;
	}
	
	public void setPvpShop(String pvpShop)
	{
		this.pvpShop = pvpShop;
	}
	
	public int getDiamond()
	{
		return diamond;
	}
	
	public void setDiamond(int diamond)
	{
		this.diamond = diamond;
	}
	
	public void addDiamond(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|获得金罡心|" + num);
		diamond += num;
		if (diamond < 0)
		{
			diamond = 0;
		}
	}
	
	public void removeDiamond(int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + getId() + "|" + getName() + "|等级：" + getLevel() + "|消耗金罡心|" + num);
		if (num > 0 && diamond > 0 && diamond >= num)
		{
			diamond -= num;
		}
	}

	public long getLastFreeDrawTime() {
		return lastFreeDrawTime;
	}

	public void setLastFreeDrawTime(long lastFreeDrawTime) {
		this.lastFreeDrawTime = lastFreeDrawTime;
	}

	public int getMnum()
	{
		return mnum;
	}

	public void setMnum(int mnum)
	{
		this.mnum = mnum;
	}

	public int getMid()
	{
		return mid;
	}

	public void setMid(int mid)
	{
		this.mid = mid;
	}

	public int getMrandom()
	{
		return mrandom;
	}

	public void setMrandom(int mrandom)
	{
		this.mrandom = mrandom;
	}

}
