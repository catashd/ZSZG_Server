package com.begamer.card.model.pojo;

public class ActivityInfo {

	private int id;
	private int activityType;
	private int activityId;
	private String name;
	private String content;
	private int hot;//1显示,0不显示
	private int exchangeType;//兑换物品类型
	private int exchangeId;//兑换物品Id
	private int exchangeNum;//兑换物品数量
	private int needType;//玩家物品类型
	private int needId;//玩家物品Id
	private int needNum;//玩家兑换物品需要的物品数量
	private int needType2;//玩家物品类型
	private int needId2;//玩家物品Id
	private int needNum2;//玩家兑换物品需要的物品数量
	private int needType3;//玩家物品类型
	private int needId3;//玩家物品Id
	private int needNum3;//玩家兑换物品需要的物品数量
	private int sole;//兑换次数(0.无限制)
	private String exchangeContext;
	private int sell;//0启用，1未用
	private int weight;//权值
	
	/**公告型活动**/
	public static ActivityInfo createActivityInfo(int activityType,int activityId,String name,String content,int hot,int weight)
	{
		ActivityInfo activityInfo = new ActivityInfo();
		activityInfo.setActivityType(activityType);
		activityInfo.setActivityId(activityId);
		activityInfo.setName(name);
		activityInfo.setContent(content);
		activityInfo.setHot(hot);
		activityInfo.setSell(0);
		activityInfo.setWeight(weight);
		return activityInfo;
	}
	/**兑换型活动**/
	public static ActivityInfo createActivityInfo(int activityType,int activityId,String name,String content,int exchangeType,int exchangeId,int exchangeNum,int needType,int needId,int needNum,int needType2,int needId2,int needNum2,int needType3,int needId3,int needNum3,int sole,String exchangeContext)
	{
		ActivityInfo activityInfo = new ActivityInfo();
		activityInfo.setActivityType(activityType);
		activityInfo.setActivityId(activityId);
		activityInfo.setName(name);
		activityInfo.setContent(content);
		activityInfo.setExchangeType(exchangeType);
		activityInfo.setExchangeId(exchangeId);
		activityInfo.setExchangeNum(exchangeNum);
		activityInfo.setNeedType(needType);
		activityInfo.setNeedId(needId);
		activityInfo.setNeedNum(needNum);
		activityInfo.setNeedType2(needType2);
		activityInfo.setNeedId2(needId2);
		activityInfo.setNeedNum2(needNum2);
		activityInfo.setNeedType3(needType3);
		activityInfo.setNeedId3(needId3);
		activityInfo.setNeedNum3(needNum3);
		activityInfo.setSole(sole);
		activityInfo.setExchangeContext(exchangeContext);
		activityInfo.setSell(0);
		return activityInfo;
	}
	
	public int getHot()
	{
		return hot;
	}
	public void setHot(int hot)
	{
		this.hot = hot;
	}
	public int getActivityType()
	{
		return activityType;
	}
	public void setActivityType(int activityType)
	{
		this.activityType = activityType;
	}
	public String getExchangeContext()
	{
		return exchangeContext;
	}
	public void setExchangeContext(String exchangeContext)
	{
		this.exchangeContext = exchangeContext;
	}
	public int getNeedType()
	{
		return needType;
	}
	public void setNeedType(int needType)
	{
		this.needType = needType;
	}
	public int getNeedId()
	{
		return needId;
	}
	public void setNeedId(int needId)
	{
		this.needId = needId;
	}
	public int getExchangeType()
	{
		return exchangeType;
	}
	public void setExchangeType(int exchangeType)
	{
		this.exchangeType = exchangeType;
	}
	public int getExchangeId()
	{
		return exchangeId;
	}
	public void setExchangeId(int exchangeId)
	{
		this.exchangeId = exchangeId;
	}
	public int getSole()
	{
		return sole;
	}
	public void setSole(int sole)
	{
		this.sole = sole;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getActivityId()
	{
		return activityId;
	}
	public void setActivityId(int activityId)
	{
		this.activityId = activityId;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	public int getExchangeNum()
	{
		return exchangeNum;
	}
	public void setExchangeNum(int exchangeNum)
	{
		this.exchangeNum = exchangeNum;
	}
	public int getNeedNum()
	{
		return needNum;
	}
	public void setNeedNum(int needNum)
	{
		this.needNum = needNum;
	}
	public int getSell()
	{
		return sell;
	}
	public void setSell(int sell)
	{
		this.sell = sell;
	}
	public int getNeedType2()
	{
		return needType2;
	}
	public void setNeedType2(int needType2)
	{
		this.needType2 = needType2;
	}
	public int getNeedId2()
	{
		return needId2;
	}
	public void setNeedId2(int needId2)
	{
		this.needId2 = needId2;
	}
	public int getNeedNum2()
	{
		return needNum2;
	}
	public void setNeedNum2(int needNum2)
	{
		this.needNum2 = needNum2;
	}
	public int getNeedType3()
	{
		return needType3;
	}
	public void setNeedType3(int needType3)
	{
		this.needType3 = needType3;
	}
	public int getNeedId3()
	{
		return needId3;
	}
	public void setNeedId3(int needId3)
	{
		this.needId3 = needId3;
	}
	public int getNeedNum3()
	{
		return needNum3;
	}
	public void setNeedNum3(int needNum3)
	{
		this.needNum3 = needNum3;
	}
	public int getWeight()
	{
		return weight;
	}
	public void setWeight(int weight)
	{
		this.weight = weight;
	}
	
	
	
}
