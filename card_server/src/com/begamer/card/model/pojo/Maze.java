package com.begamer.card.model.pojo;

import java.util.List;

import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.util.Statics;
import com.begamer.card.common.util.binRead.CardData;

public class Maze {
	
	private int id;
	private int playerId;
	private int mazeId;// 迷宫id
	private String cardInfo;// 卡牌 格式：cardId-cardlevel
	private String breakInfo;// 突破(num-num-num)
	private String equipInfo;// 装备(格式：id-level,id-level)
	private String skillInfo;// 主动技能(格式：id-level,id-level)
	private String passiveskillInfo;// 被动技能(格式：id-level,id-level)
	private String talentInfo;// 天赋(格式:id-id-id,id-id-id-id)
	private String bloodInfo;// 血量
	private String maxbloodInfo;// 初始血量
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public int getPlayerId()
	{
		return playerId;
	}
	
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	
	public int getMazeId()
	{
		return mazeId;
	}
	
	public void setMazeId(int mazeId)
	{
		this.mazeId = mazeId;
	}
	
	public String getCardInfo()
	{
		return cardInfo;
	}
	
	public void setCardInfo(String cardInfo)
	{
		this.cardInfo = cardInfo;
	}
	
	public String getBreakInfo()
	{
		return breakInfo;
	}
	
	public void setBreakInfo(String breakInfo)
	{
		this.breakInfo = breakInfo;
	}
	
	public String getEquipInfo()
	{
		return equipInfo;
	}
	
	public void setEquipInfo(String equipInfo)
	{
		this.equipInfo = equipInfo;
	}
	
	public String getSkillInfo()
	{
		return skillInfo;
	}
	
	public void setSkillInfo(String skillInfo)
	{
		this.skillInfo = skillInfo;
	}
	
	public String getPassiveskillInfo()
	{
		return passiveskillInfo;
	}
	
	public void setPassiveskillInfo(String passiveskillInfo)
	{
		this.passiveskillInfo = passiveskillInfo;
	}
	
	public String getTalentInfo()
	{
		return talentInfo;
	}
	
	public void setTalentInfo(String talentInfo)
	{
		this.talentInfo = talentInfo;
	}
	
	public String getBloodInfo()
	{
		return bloodInfo;
	}
	
	public void setBloodInfo(String bloodInfo)
	{
		this.bloodInfo = bloodInfo;
	}
	
	public String getMaxbloodInfo()
	{
		return maxbloodInfo;
	}
	
	public void setMaxbloodInfo(String maxbloodInfo)
	{
		this.maxbloodInfo = maxbloodInfo;
	}
	
	public static Maze createMaze(PlayerInfo pi, int mid)
	{
		Maze maze = new Maze();
		maze.setPlayerId(pi.player.getId());
		maze.setMazeId(mid);
		CardGroup cg = pi.getCardGroup();
		Card[] cards = cg.getCards();
		Skill[] skills = cg.getSkills();
		List<PassiveSkill>[] pSkills = cg.getpSkills();
		List<Equip>[] equips = cg.getEquips();
		int unitId = cg.getUnitId();
		String cardInfo = "";// 卡牌 格式：cardId-cardlevel
		String breakInfo = "";// 突破(num-num-num)
		String equipInfo = "";// 装备(格式：id-level,id-level)
		String skillInfo = "";// 主动技能(格式：id-level,id-level)
		String passiveskillInfo = "";// 被动技能(格式：id-level,id-level)
		String talentInfo = "";// 天赋(格式:id-id-id,id-id-id-id)
		String bloodInfo = "";// 血量
		String maxbloodInfo = "";// 初始血量
		if (cards.length > 0)
		{
			for (int i = 0; i < cards.length; i++)
			{
				Card c = cards[i];
				if (c != null)
				{
					CardData cData = CardData.getData(c.getCardId());
					Skill skill = null;
					if (skills != null)
					{
						skill = skills[i];
						if (skill == null)
						{
							skill = new Skill();
							skill.setSkillId(cData.basicskill);
						}
					}
					int hp = Statics.totalCardBlood(cg, c, pi, equips[i], pSkills[i], skill, i, true);
					if ("".equals(cardInfo))
					{
						cardInfo = cards[i].getCardId() + "-" + cards[i].getLevel();
						breakInfo = breakInfo + cards[i].getBreakNum();
						talentInfo = cData.talent + "-" + cData.talent2 + "-" + cData.talent3;
						bloodInfo = bloodInfo + hp;
						maxbloodInfo = maxbloodInfo + hp;
					}
					else
					{
						cardInfo = cardInfo + "," + cards[i].getCardId() + "-" + cards[i].getLevel();
						breakInfo = breakInfo + "-" + cards[i].getBreakNum();
						talentInfo = talentInfo + "," + cData.talent + "-" + cData.talent2 + "-" + cData.talent3;
						bloodInfo = bloodInfo + "-" + hp;
						maxbloodInfo = bloodInfo + "-" + hp;
					}
					Skill s = skills[i];
					if (s != null)
					{
						if ("".equals(skillInfo))
						{
							skillInfo = skills[i].getSkillId() + "-" + skills[i].getLevel();
						}
						else
						{
							skillInfo = skillInfo + "," + skills[i].getSkillId() + "-" + skills[i].getLevel();
						}
					}
					else
					{
						if ("".equals(skillInfo))
						{
							skillInfo = cData.basicskill + "-" + 1;
						}
						else
						{
							skillInfo = skillInfo + "," + cData.basicskill + "-" + 1;
						}
					}
					
					if (equips[i] != null)
					{
						List<Equip> equips2 = equips[i];
						for (int j = 0; j < equips2.size(); j++)
						{
							Equip equip = equips2.get(j);
							if (equip != null)
							{
								if (j == 0)
								{
									equipInfo = equipInfo + equip.getEquipId() + "-" + equip.getLevel();
								}
								else
								{
									equipInfo = equipInfo + "-" + equip.getEquipId() + "-" + equip.getLevel();
								}
							}
							else
							{
								if (j == 0)
								{
									equipInfo = equipInfo + 0 + "-" + 0;
								}
								else
								{
									equipInfo = equipInfo + "-" + 0 + "-" + 0;
								}
							}
						}
						equipInfo = equipInfo + ",";
					}
					else
					{
						for (int j = 0; j < 3; j++)
						{
							if (j == 0)
							{
								equipInfo = equipInfo + 0 + "-" + 0;
							}
							else
							{
								equipInfo = equipInfo + "-" + 0 + "-" + 0;
							}
						}
						equipInfo = equipInfo + ",";
					}
					
					if (pSkills[i] != null)
					{
						List<PassiveSkill> passiveSkills = pSkills[i];
						for (int j = 0; j < passiveSkills.size(); j++)
						{
							PassiveSkill pa = passiveSkills.get(j);
							if (pa != null)
							{
								if (j == 0)
								{
									passiveskillInfo = passiveskillInfo + pa.getPassiveSkillId();
								}
								else
								{
									passiveskillInfo = passiveskillInfo + "-" + pa.getPassiveSkillId();
								}
							}
							else
							{
								if (j == 0)
								{
									passiveskillInfo = passiveskillInfo + 0;
								}
								else
								{
									passiveskillInfo = passiveskillInfo + "-" + 0;
								}
							}
						}
						passiveskillInfo = passiveskillInfo + ",";
					}
					else
					{
						for (int j = 0; j < 3; j++)
						{
							if (j == 0)
							{
								passiveskillInfo = passiveskillInfo + 0;
							}
							else
							{
								passiveskillInfo = passiveskillInfo + "-" + 0;
							}
						}
						passiveskillInfo = passiveskillInfo + ",";
					}
				}
			}
		}
		maze.setCardInfo(cardInfo);
		maze.setBreakInfo(breakInfo);
		maze.setSkillInfo(skillInfo);
		maze.setTalentInfo(talentInfo);
		maze.setBloodInfo(bloodInfo);
		maze.setEquipInfo(equipInfo);
		maze.setPassiveskillInfo(passiveskillInfo);
		maze.setMaxbloodInfo(maxbloodInfo);
		return maze;
	}
	
	// private static int getMaxHp(int cardId,int level,int breakNum,int
	// skillId,int skillLevel,List<String> psList,List<String> equipInfos,int
	// talent1,int talent2,int talent3,String runeId,int selfIndex,int[]
	// teamerCardIds,int[] talents1,int[] talents2,int[] talents3,String[]
	// raceAtts)
	// {
	// //maxHp=(card_hp*(1+b_hp+c_hp+f_hp)+a_hp+d_hp+e_hp+equip_hp+g_hp)//
	// //card_hp为卡牌本身的hp；//
	// //equip_hp为装备附加的hp；//
	// //a_hp为被动技能的加成系数；//
	// //b_hp为卡牌自身天赋的加成系数；//
	// //c_hp为队友卡牌天赋的加成系数（所有队友系数相乘后的结果）；//
	// //d_hp为卡牌当前装备的主动技能的加成系数；//
	// //e_hp为符文系统提供的hp加成系数；//
	// //f_hp为突破系统提供的hp加成系数；//
	// //g_hp为天赋系统提供的hp加成数值；//
	// float card_hp=getCardSelfMaxHp(cardId,level);
	// float equip_hp=0;
	// if(equipInfos!=null)
	// {
	// foreach(string equip in equipInfos)
	// {
	// string[] ss=equip.Split('-');
	// EquipData equipData=EquipData.getData(StringUtil.getInt(ss[0]));
	// if(equipData.type==3)
	// {
	// EquippropertyData
	// equippropertyData=EquippropertyData.getData(equipData.type,StringUtil.getInt(ss[1]));
	// equip_hp+=equippropertyData.starNumbers[equipData.star-1];
	// }
	// }
	// }
	// float a_hp=0;
	// for(int i = 0; i < psList.Count;++i)
	// {
	// PassiveSkillData
	// psd=PassiveSkillData.getData(StringUtil.getInt(psList[i]));
	// if(psd!=null && psd.type==3)
	// {
	// a_hp +=psd.numbers;
	// }
	// }
	//
	// float
	// b_hp=getTalentB_hp(talent1,skillId)+getTalentB_hp(talent2,skillId)+getTalentB_hp(talent3,skillId);
	// float
	// c_hp=getTalentC_hp(cardId,selfIndex,teamerCardIds,talents1)+getTalentC_hp(cardId,selfIndex,teamerCardIds,talents2)+getTalentC_hp(cardId,selfIndex,teamerCardIds,talents3);
	// float d_hp=0;
	//		
	// //符文属性//
	// float e_hp=getRuneValue(runeId, 3);
	// CardData cd=CardData.getData(cardId);
	// EvolutionData ed=EvolutionData.getData(cd.star,breakNum);
	// float f_hp=0;
	// if(ed!=null)
	// {
	// f_hp=ed.status;
	// f_hp/=100;
	// }
	// //种族加成属性//
	// float raceAttMul=getRaceAtt(6,raceAtts)/100;
	// float raceAttValue=getRaceAtt(3,raceAtts);
	// //==天赋加成固定数值==//
	// float
	// g_hp=getTalentG_hp(talent1)+getTalentG_hp(talent2)+getTalentG_hp(talent3);
	//		
	// return
	// (int)(card_hp*(1+b_hp+c_hp+f_hp+raceAttMul)+a_hp+d_hp+e_hp+equip_hp+raceAttValue+g_hp);
	// }
	
}
