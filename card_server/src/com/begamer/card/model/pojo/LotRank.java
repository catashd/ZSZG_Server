package com.begamer.card.model.pojo;

public class LotRank {
	
	private int id;
	private int playerId;
	private int lotNum;
	private int rank;
	private long lastLotTime;
	private int score;
	private int scoreLotNum;
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public int getPlayerId()
	{
		return playerId;
	}
	
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	
	public int getLotNum()
	{
		return lotNum;
	}
	
	public void setLotNum(int lotNum)
	{
		this.lotNum = lotNum;
	}
	
	public int getRank()
	{
		return rank;
	}
	
	public void setRank(int rank)
	{
		this.rank = rank;
	}
	
	public long getLastLotTime()
	{
		return lastLotTime;
	}
	
	public void setLastLotTime(long lastLotTime)
	{
		this.lastLotTime = lastLotTime;
	}
		
	public int getScore()
	{
		return score;
	}
	
	public void setScore(int score)
	{
		this.score = score;
	}

	public int getScoreLotNum()
	{
		return scoreLotNum;
	}

	public void setScoreLotNum(int scoreLotNum)
	{
		this.scoreLotNum = scoreLotNum;
	}
}
